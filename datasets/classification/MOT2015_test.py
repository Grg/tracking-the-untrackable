import os
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
from collections import namedtuple
import warnings

class MOT2015Dataset(Dataset):
    """2DMOT2015 dataset."""

    def __init__(
            self,
            transform=None,
            crops=True,
            velocities=True,
            occupancy_grids=True,
            balanced_batch=True,
            sequence_length=6,
            full_image_grid_dim = 15,
            occupancy_grid_dim = 7,
            input_w = 224,
            input_h = 224,
            dataset_path="/home/gkovac/Projekt/tracking-the-untrackable/data/"
    ):

        self.balanced_batch = balanced_batch
        self.crops = crops
        self.velocities = velocities
        self.occupancy_grids = occupancy_grids

        self.input_w, self.input_h = input_w, input_h
        self.margin = 0
        self.sequence_length = sequence_length
        self.max_sequence_length = sequence_length
        self.full_image_grid_dim = full_image_grid_dim
        assert full_image_grid_dim % 2 == 1

        self.occupancy_grid_dim = occupancy_grid_dim
        assert occupancy_grid_dim % 2 == 1

        self.dataset_path = dataset_path
        self.transform = transform

        video_names = os.listdir(os.path.join(dataset_path, "2DMOT2015/train"))
        # video_names = [video_names[0]]

        self.gt_boxes_dict = {}
        self.total_gt_boxes = []
        for video_name in video_names:
            gt_boxes = pd.read_csv(os.path.join(dataset_path,'2DMOT2015/train/', video_name, 'gt/gt.txt'), header=None).as_matrix()[:,:7]
            gt_boxes = parse_gt_boxes(gt_boxes, video_name)

            self.gt_boxes_dict[video_name] = create_sequence_gt_boxes_dict(gt_boxes)
            self.total_gt_boxes.extend(gt_boxes)

        print("gt boxes dict created")

        if self.occupancy_grids:
            self.occupancy_grids_dict = {gt_box: self.create_occupancy_grid(gt_box) for gt_box in self.total_gt_boxes}
        else:
            self.occupancy_grids_dict = None

        print("occupancy grids created")

        self.gt_boxes_sequences = self.create_sequences(self.total_gt_boxes)
        print("sequences created: ", len(self.gt_boxes_sequences))

        num_of_positive_examples = sum([b['label'] for b in self.gt_boxes_sequences])
        num_of_negative_examples = len(self.gt_boxes_sequences)-num_of_positive_examples
        self._class_weights = [num_of_negative_examples, num_of_positive_examples]

    @property
    def class_weights(self):
        return self._class_weights

    def get_coordinates_in_grid(self, box):
        # todo: do this in a smarter way
        img = cv2.imread(self.get_image_path(box))
        height, width, _ = img.shape

        cell_h = height // self.full_image_grid_dim
        cell_w = width  // self.full_image_grid_dim

        h_idx = int((box.bb_top  + (box.bb_height // 2)) // cell_h)
        w_idx = int((box.bb_left + (box.bb_width  // 2)) // cell_w)


        return h_idx, w_idx


    def create_occupancy_grid(self, box):
        occupancy_grid = np.zeros((self.occupancy_grid_dim, self.occupancy_grid_dim), dtype=np.float)
        grid_center_x = int(self.occupancy_grid_dim // 2)
        grid_center_y = int(self.occupancy_grid_dim // 2)
        box_y, box_x = self.get_coordinates_in_grid(box)

        for neighbour_id, neighbour in self.gt_boxes_dict[box.video_name][box.frame_number].items():

            # todo: razmisli sto je bolje, isprobaj?
            # if neighbour_id == box.obj_id: continue

            if type(neighbour) != Box:
                # todo: fix this bug, why does this happen
                print("Wierd list appears")
                exit()


            neighbour_y, neighbour_x = self.get_coordinates_in_grid(neighbour)

            delta_y = neighbour_y - box_y
            delta_x = neighbour_x - box_x

            if abs(delta_y) <= (self.occupancy_grid_dim // 2) and \
                            abs(delta_x) <= self.occupancy_grid_dim // 2:
                # neighbour is inside the occupancy grid

                grid_y = grid_center_y + delta_y
                grid_x = grid_center_x + delta_x



                occupancy_grid[grid_y][grid_x] = 1.0

        return occupancy_grid.flatten()


    def create_sequences(self, gt_boxes):
        sequences = []

        for gt_box in gt_boxes:

            # check if previous frame does not have the object so we can't calculate the velocities
            if self.velocities and len(self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number-1][gt_box.obj_id]) == 0: continue
            assert not (self.velocities and gt_box.obj_id not in self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number-1]) # replace with this

            if self.balanced_batch:
                sequence_boxes_balanced = [self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number+i][gt_box.obj_id] for i in range(self.sequence_length+1)]
                if any(len(sequence_box) == 0 for sequence_box in sequence_boxes_balanced): continue

            # check if at least one of the following frames does not have the wanted object
            sequence_boxes = [self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number+i][gt_box.obj_id] for i in range(self.sequence_length)]

            # todo: move this logic elswhere because it is redundant here
            if self.velocities:
                sequence_velocities=[np.array([
                    sequence_box.bb_left - self.gt_boxes_dict[sequence_box.video_name][sequence_box.frame_number - 1][sequence_box.obj_id].bb_left,
                    sequence_box.bb_top  - self.gt_boxes_dict[sequence_box.video_name][sequence_box.frame_number - 1][sequence_box.obj_id].bb_top
                ]) for sequence_box in sequence_boxes]

            if self.occupancy_grids:
                sequence_occupancy_grids = [self.occupancy_grids_dict[box] for box in sequence_boxes]


            if self.balanced_batch:
                sequence_boxes_balanced = [self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number+i][gt_box.obj_id] for i in range(self.sequence_length+1)]
                if any(len(sequence_box) == 0 for sequence_box in sequence_boxes_balanced): continue

                positive_candidate = self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number+self.sequence_length][gt_box.obj_id]
                positive_sample = self.create_sample(
                    candidate_box=positive_candidate,
                    candidate_label=1,
                    sequence_boxes=sequence_boxes,
                    sequence_velocities=sequence_velocities if self.velocities else None,
                    sequence_occupancy_grids=sequence_occupancy_grids if self.occupancy_grids else None
                )

                sequences.append(
                    positive_sample
                )

                for candidate_obj_id, candidate_box in self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number+self.sequence_length].items():

                    if gt_box.obj_id != candidate_obj_id and type(candidate_box) is not list:
                        negative_candiate = candidate_box
                        break
                else:
                    # do not add the negative candidate
                    continue



                negative_sample = self.create_sample(
                    candidate_box=negative_candiate,
                    candidate_label=0,
                    sequence_boxes=sequence_boxes,
                    sequence_velocities=sequence_velocities if self.velocities else None,
                    sequence_occupancy_grids=sequence_occupancy_grids if self.occupancy_grids else None
                )

                sequences.append(
                    negative_sample
                )

            else:

                for candidate_obj_id, candidate_box in self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number+self.sequence_length].items():

                    sample = {
                        "label": 1 if gt_box.obj_id == candidate_obj_id else 0
                    }

                    if self.crops:
                        sample["sequence_boxes"] = sequence_boxes
                        sample["candidate_box"]  = candidate_box

                    if self.velocities:
                        sample["sequence_velocities"] = sequence_velocities
                        sample["candidate_velocity"]  = np.array([
                            candidate_box.bb_left - sequence_boxes[-1].bb_left,
                            candidate_box.bb_top  - sequence_boxes[-1].bb_top,
                            ])

                    if self.occupancy_grids:
                        sample["sequence_occupancy_grids"] = sequence_occupancy_grids
                        sample["candidate_occupancy_grid"] = self.occupancy_grids_dict[candidate_box]

                    sequences.append(
                        sample
                    )

        return sequences


    def create_sample(self, candidate_box, candidate_label, sequence_boxes=None, sequence_velocities=None, sequence_occupancy_grids=None):
        if type(candidate_box) is not Box:
            raise ValueError("Candidate box must be of type Box")

        if candidate_label not in [0, 1]:
            raise ValueError("Candidate label must be 0 or 1")

        sample = {
            "label": candidate_label
        }

        if self.crops:
            if sequence_boxes is None:
                raise ValueError("sequcence_boxes can't be None if self.crops is True")

            sample["sequence_boxes"] = sequence_boxes
            sample["candidate_box"]  = candidate_box

        if self.velocities:
            if sequence_velocities is None:
                raise ValueError("sequcence_velocities can't be None if self.velocities is True")

            if sequence_boxes is None:
                raise ValueError("sequcence_boxes can't be None if self.velocities is True")


            sample["sequence_velocities"] = sequence_velocities
            sample["candidate_velocity"]  = np.array([
                candidate_box.bb_left - sequence_boxes[-1].bb_left,
                candidate_box.bb_top  - sequence_boxes[-1].bb_top,
                ])

        if self.occupancy_grids:
            if sequence_occupancy_grids is None:
                raise ValueError("sequence_occupancy_grids can't be None if self.occupancy_grids is True")

            if self.occupancy_grids_dict is None:
                raise ValueError("occupancy_grids_dict wasn't built")

            sample["sequence_occupancy_grids"] = sequence_occupancy_grids
            sample["candidate_occupancy_grid"] = self.occupancy_grids_dict[candidate_box]

        return sample


    def __len__(self):
        return len(self.gt_boxes_sequences)


    def  get_image_path(self, box):
        img_path = os.path.join(os.path.join(
            self.dataset_path,'2DMOT2015/train',box.video_name,'img1/',
            str(int(box.frame_number)).zfill(6) + ".jpg")
        )
        return img_path


    def crop_and_resize_box_from_frame(self, box):
        img_name = self.get_image_path(box)
        image = cv2.imread(img_name)

        assert image is not None
        # crop and add 4 for context
        image = image[
                int(np.floor(box.bb_top  - self.margin)): int(np.ceil(box.bb_top  + box.bb_height + 2*self.margin)),
                int(np.floor(box.bb_left - self.margin)): int(np.ceil(box.bb_left + box.bb_width  + 2*self.margin))
                ]
        # print(self.input_h)
        # print(self.input_w)
        # print(image.shape)
        return cv2.resize(image, dsize=(self.input_h, self.input_w), interpolation=cv2.INTER_CUBIC)

    def __getitem__(self, idx):
        # index of the last box to be fed into the LSTM


        sample = {
            "label" : self.gt_boxes_sequences[idx]['label']
        }

        if self.crops:
            # crop boxes from frames and reshape to fixes size
            boxes = self.gt_boxes_sequences[idx]["sequence_boxes"]

            preprocessed_boxes = []
            for i, box in enumerate(boxes):
                image = self.crop_and_resize_box_from_frame(box)
                preprocessed_boxes.append(image)

            # preprocessed_boxes = np.stack(preprocessed_boxes)
            candidate_crop = self.crop_and_resize_box_from_frame(self.gt_boxes_sequences[idx]["candidate_box"])

            sample["image_crops"] = preprocessed_boxes
            sample["candidate_crop"] = candidate_crop

        if self.velocities:
            sample["sequence_velocities"] = self.gt_boxes_sequences[idx]["sequence_velocities"]
            sample["candidate_velocity"] = self.gt_boxes_sequences[idx]["candidate_velocity"]

        if self.occupancy_grids:
            sample["sequence_occupancy_grids"] = self.gt_boxes_sequences[idx]["sequence_occupancy_grids"]
            sample["candidate_occupancy_grid"] = self.gt_boxes_sequences[idx]["candidate_occupancy_grid"]

        if self.transform:
            sample = self.transform(sample)

        return sample

class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __init__(self, crops=True, velocities=True, occupancy_grids=True):
        self.crops = crops
        self.velocities = velocities
        self.occupancy_grids = occupancy_grids
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


    def __call__(self, sample):
        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W

        # crops
        if self.crops:
            image_crops, candidate_crop = sample['image_crops'], sample['candidate_crop']

            image_crops = [
                torch.from_numpy(image_crop.transpose((2, 0, 1))).float()
                for i, image_crop in enumerate(image_crops)
            ]

            candidate_crop = torch.from_numpy(candidate_crop.transpose((2, 0, 1))).float()

            sample["image_crops"]    = image_crops
            sample["candidate_crop"] = candidate_crop

        if self.velocities:
            # velocities
            sequence_velocities,candidate_velocity = sample['sequence_velocities'], sample['candidate_velocity']
            sequence_velocities = [
                torch.from_numpy(velocities).float()
                for i, velocities in enumerate(sequence_velocities)
            ]
            candidate_velocity = torch.from_numpy(candidate_velocity).float()

            sample["sequence_velocities"] = sequence_velocities
            sample["candidate_velocity"]  = candidate_velocity

        if self.occupancy_grids:
            sequence_occupancy_grids, candidate_occupancy_grid = sample['sequence_occupancy_grids'], sample['candidate_occupancy_grid']
            sequence_occupancy_grids = [
                torch.from_numpy(occupancy_grid).float()
                for i, occupancy_grid in enumerate(sequence_occupancy_grids)
            ]
            candidate_velocity = torch.from_numpy(candidate_occupancy_grid).float()

            sample["sequence_occupancy_grids"] = sequence_occupancy_grids
            sample["candidate_occupancy_grid"] = candidate_velocity

        return sample
