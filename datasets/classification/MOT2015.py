import os
from itertools import chain
from PIL import Image
import random
import pickle
import glob
import cv2
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import torchvision.transforms.functional as tr_F
from collections import namedtuple, defaultdict
import warnings
import torch

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

import datasets.dataset_utils as dataset_utils

from datasets.dataset_utils import Box


def create_sequence_det_boxes_dict(det_boxes):
    det_boxes_dict = defaultdict(list)

    for det_box in det_boxes:
        assert type(det_box) == dataset_utils.Box
        det_boxes_dict[det_box.frame_number].append(det_box)

    det_boxes_dict = dict(det_boxes_dict)

    return det_boxes_dict

# shrinks bbox if it is outside of frame
def fit_bbox_to_frame(bbox, frame):
    x, y, w, h = bbox
    # box mora poceti unutar frame-a
    assert x >= 0
    assert y >= 0
    fh, fw, _ = frame.shape
    w = min(w, fw-x)
    h = min(h, fh-y)
    bbox = np.array([x, y, w, h])
    return bbox


class MOT2015(Dataset):
    """2DMOT2015 dataset."""

    def __init__(
            self,
            transform=None,
            crops=True,
            velocities=True,
            occupancy_grids=True,
            balanced_batch=True,
            hard_examples=True,
            sequence_length=6,
            future_seq_len=6,
            full_image_grid_dim=15,
            occupancy_grid_dim=7,
            input_w=224,
            input_h=224,
            dataset_path="/home/gkovac/Projekt/tracking-the-untrackable/data/",
            test_set=False,
    ):
        self.obj_id_counter = defaultdict(lambda: defaultdict(int))
        self.obj_id_frame_counter_neg = defaultdict(dict)
        self.obj_id_frame_counter_pos = defaultdict(dict)
        self.gap = 10
        self.limit = 200
        self.limit_ex_per_id = False

        self.only_pairs = True
        self.add_cuhk = False

        self.all_futures = False
        if self.all_futures:
            assert not self.only_pairs
            assert not self.limit_ex_per_id

        print("limit_ex {} limit {} gap {}".format(self.limit_ex_per_id, self.limit, self.gap))
        print("only pairs: {}".format(self.only_pairs))
        print("all futures: {}".format(self.all_futures))

        self.test_set = test_set
        self.crops = crops
        self.velocities = velocities
        self.occupancy_grids = occupancy_grids
        self.overlap_occ = 0.3  # original 0.7
        self.overlap_pos = 0.5

        self.future_seq_len = future_seq_len

        self.max_iou_detections_dict = dict()

        self.input_w, self.input_h = input_w, input_h
        self.margin = 0
        self.sequence_length = sequence_length
        self.hard_examples = hard_examples
        self.max_sequence_length = sequence_length
        self.full_image_grid_dim = full_image_grid_dim
        assert full_image_grid_dim % 2 == 1

        self.occupancy_grid_dim = occupancy_grid_dim
        assert occupancy_grid_dim % 2 == 1

        self.dataset_path = dataset_path
        self.transform = transform

        print("dataset_type: ", "test" if self.test_set else "train")
        # self.video_names = os.listdir(os.path.join(dataset_path, "2DMOT2015/train"))

        if self.test_set:
            # self.video_names = ["TUD-Campus", "ETH-Sunnyday", "ETH-Pedcross2", "ADL-Rundle-8", "Venice-2", "KITTI-17"]
            # ["TUD-Campus" : 70, "ETH-Sunnyday": 350, "ETH-Pedcross2": 837, "ADL-Rundle-8": 600, "Venice-2": 600, "KITTI-17": 145]

            self.video_names = ["TUD-Campus", "ETH-Sunnyday"]
            print("EXTENDED")


            # self.video_names = ["TUD-Campus"]

        else:
            # self.video_names = ["TUD-Stadtmitte", "ETH-Bahnhof", "ADL-Rundle-6", "KITTI-13"]
            #     'MOT16-10', < ADL-Rundle-8
            #     'MOT16-05', < Pedcross2
            #     'MOT16-02'

            # self.video_names = [
            #     'ADL-Rundle-6',
            #     'MOT16-13',
            #     'MOT16-04',
            #     'ETH-Bahnhof',
            #     'KITTI-13',
            #     'PETS09-S2L1',
            #     'MOT16-11',
            #     'TUD-Stadtmitte',
            #     # 'MOT16-09', < Rundle-6
            # ],

            self.video_names = [
               'ADL-Rundle-6',
               'MOT16-13',
               'MOT16-04',
               'ETH-Bahnhof',
               'KITTI-13',
               'PETS09-S2L1',
               'MOT16-11',
               'TUD-Stadtmitte',
                "ETH-Pedcross2",
                "ADL-Rundle-8",
                "Venice-2",
                "KITTI-17"
            ]
            print("EXTENDED")


        self.gt_boxes_dict = {}
        self.det_boxes_dict = {}
        self.total_gt_boxes = []
        for video_name in self.video_names:
            frame_h, frame_w, _ = cv2.imread(glob.glob(
                os.path.join(self.dataset_path, 'MOT/train/', video_name, 'img1/*.jpg'))[0]).shape

            yolo = True
            if yolo:
                print("yolo")
                det_path = os.path.join(dataset_path, 'MOT/train/', video_name, 'det/yolo_det.txt')
                det_boxes = pd.read_csv(det_path, header=None).values[:, :7]
                det_boxes = dataset_utils.parse_boxes(det_boxes, video_name, frame_h, frame_w)
                det_boxes = [b for b in det_boxes if b.conf > 50.0]

            # both = False
            # if both:
            #     assert yolo
            #     print("both")
            #     det_path_n = os.path.join(dataset_path, 'MOT/train/', video_name, 'det/det.txt')
            #     det_boxes_n = pd.read_csv(det_path_n, header=None).values[:, :7]
            #     det_boxes_n = dataset_utils.parse_boxes(det_boxes_n, video_name, frame_h, frame_w)
            #     det_boxes_n = [b for b in det_boxes_n if b.conf > 10.0]
            #
            #     det_boxes += det_boxes_n

            else:
                print("noisy")
                det_path = os.path.join(dataset_path, 'MOT/train/', video_name, 'det/det.txt')
                det_boxes = pd.read_csv(det_path, header=None).values[:, :7]
                det_boxes = dataset_utils.parse_boxes(det_boxes, video_name, frame_h, frame_w)
                det_boxes = [b for b in det_boxes if b.conf > 10.0]

            self.det_boxes_dict[video_name] = create_sequence_det_boxes_dict(det_boxes)

            gt_path = os.path.join(dataset_path, 'MOT/train/', video_name, 'gt/gt.txt')
            gt_boxes = pd.read_csv(gt_path, header=None).values[:, :7]
            gt_boxes = dataset_utils.parse_boxes(gt_boxes, video_name, frame_h, frame_w)
            gt_boxes = [b for b in gt_boxes if b.conf > 0.5]
            self.gt_boxes_dict[video_name] = dataset_utils.create_sequence_gt_boxes_dict(gt_boxes)
            self.total_gt_boxes.extend(gt_boxes)

        print("gt boxes dict created size: {}".format(len(self.total_gt_boxes)))

        if self.occupancy_grids:

            gt_boxes_hash = dataset_utils.calculate_hash([
                self.total_gt_boxes,
                self.occupancy_grid_dim,
                self.full_image_grid_dim,
                self.dataset_path,
                self.limit,
                self.limit_ex_per_id,
                self.gap
            ])

            cache_path = os.path.join("./cache", gt_boxes_hash+".pickle")

            if os.path.isfile(cache_path):
                with open(cache_path, "rb") as cache_file:
                    print("loading from cache: ", cache_path)
                    self.occupancy_grids_dict = pickle.load(cache_file)
                    print("loaded")

            else:
                print("creating occupancy grids")
                gt_box_det_box_pairs = [(gt_box, self.find_max_iou_detection(gt_box)[0]) for i, gt_box in enumerate(self.total_gt_boxes)]

                # det_box should not have id as it can be assigned to more objects
                # object looks for the most overlaping detection (with iou > 0.5)
                # can be true for more objects
                self.occupancy_grids_dict = {
                    det_box: self.create_occupancy_grid(
                        self.find_max_iou_detection(gt_box)[0],
                        self.occupancy_grid_dim,
                        self.full_image_grid_dim,
                        self.dataset_path,
                        i
                    ) for i, (gt_box, det_box) in enumerate(gt_box_det_box_pairs) if det_box is not None}

                print("occupancy grids created")

                print("caching")
                with open(cache_path, "wb") as cache_file:
                    pickle.dump(self.occupancy_grids_dict, cache_file)
                print("cached to: ", cache_path)

        else:
            self.occupancy_grids_dict = None

        total_gt_boxes_hash = dataset_utils.calculate_hash(
            self.total_gt_boxes+det_boxes+[
                self.velocities, self.crops, self.occupancy_grids, self.sequence_length,
                self.limit_ex_per_id, self.limit, self.gap, self.only_pairs
            ]
        )
        cache_path = os.path.join("./cache", total_gt_boxes_hash +".pickle")

        if os.path.isfile(cache_path):
            with open(cache_path, "rb") as cache_file:
                print("loading sequences from cache: ", cache_path)
                self.gt_boxes_sequences = pickle.load(cache_file)
                print("loaded")

        else:
            self.gt_boxes_sequences = self.create_sequences(self.total_gt_boxes)

            with open(cache_path, "wb") as cache_file:
                pickle.dump(self.gt_boxes_sequences, cache_file)

            if self.limit_ex_per_id and not self.test_set:
                assert len(self.gt_boxes_sequences) == sum(
                    chain(*[
                        obj_c.values() for obj_c in self.obj_id_counter.values()
                    ])
                )
            print("cached to: ", cache_path)

        if self.add_cuhk and not self.test_set:
            assert self.crops and not self.velocities
            cuhk_sequences = self.create_cuhk_sequences()

            self.gt_boxes_sequences.extend(cuhk_sequences)

        labels = [s['label'] for s in self.gt_boxes_sequences]

        print("sequences ", len(self.gt_boxes_sequences))
        pos = sum(labels)
        neg = len(labels) - pos
        print("positive samples: {} negative samples: {}".format(pos, neg))

        from collections import Counter
        c = Counter([s['sequence_boxes'].__repr__() for s in self.gt_boxes_sequences])
        print("duplicate sequence boxes:", sum([c == 2 for c in c.values()]))
        print("single sequence boxes:", sum([c == 1 for c in c.values()]))

        duplicate_labels = [s['label'] for s in self.gt_boxes_sequences if c[s['sequence_boxes'].__repr__()] == 2]
        print("duplicate labels:", Counter(duplicate_labels))

        duplicate_labels = [s['label'] for s in self.gt_boxes_sequences if c[s['sequence_boxes'].__repr__()] == 1]
        print("single labels:", Counter(duplicate_labels))


        if self.crops:
            num_diff_obj = len(set(
                [str(s['candidate_box'].obj_id)+str(s['candidate_box'].video_name) for s in self.gt_boxes_sequences]
            ))
            print("num diff obj: ", num_diff_obj)


        print("{} sequences created".format(len(self.gt_boxes_sequences)))

        num_of_positive_examples = sum([b['label'] for b in self.gt_boxes_sequences])
        num_of_negative_examples = len(self.gt_boxes_sequences)-num_of_positive_examples
        self._class_weights = [num_of_negative_examples, num_of_positive_examples]


    @property
    def class_weights(self):
        return self._class_weights

    @staticmethod
    def get_coordinates_in_grid(box, full_image_grid_dim, dataset_path):
        # todo: do this in a smarter way
        img = cv2.imread(MOT2015.get_image_path(box, dataset_path))
        height, width, _ = img.shape

        cell_h = height // full_image_grid_dim
        cell_w = width  // full_image_grid_dim

        h_idx = int((box.bb_top + (box.bb_height // 2)) // cell_h)
        w_idx = int((box.bb_left + (box.bb_width // 2)) // cell_w)

        return h_idx, w_idx

    def create_occupancy_grid(self, box, occupancy_grid_dim, full_image_grid_dim, dataset_path, i=None):
        print("creating occupancy grid ", i)
        occupancy_grid = np.zeros((occupancy_grid_dim, occupancy_grid_dim), dtype=np.float)
        grid_center_x = int(occupancy_grid_dim // 2)
        grid_center_y = int(occupancy_grid_dim // 2)
        box_y, box_x = MOT2015.get_coordinates_in_grid(box, full_image_grid_dim, dataset_path)

        for neighbour in self.det_boxes_dict[box.video_name][box.frame_number]:

            neighbour_y, neighbour_x = MOT2015.get_coordinates_in_grid(neighbour, full_image_grid_dim, dataset_path)

            delta_y = neighbour_y - box_y
            delta_x = neighbour_x - box_x

            if abs(delta_y) <= (occupancy_grid_dim // 2) and abs(delta_x) <= occupancy_grid_dim // 2:
                # neighbour is inside the occupancy grid

                grid_y = grid_center_y + delta_y
                grid_x = grid_center_x + delta_x

                occupancy_grid[grid_y][grid_x] = 1.0

        return occupancy_grid.flatten()

    def check_occluded(self, box):
        frame_detections = self.gt_boxes_dict[box.video_name][box.frame_number]

        # the only object on the frame
        if len(frame_detections) == 1: return False

        # occluded_overlap = max([
        #     dataset_utils.box_intersection_over_union(box, b)
        #     for b in frame_detections.values() if box.obj_id != b.obj_id
        # ])

        # assumption that the object is always occluded by one object, and that the bigger one is in from
        # with sum the problem is that the objet can be in the foreground
        occluded_pecentage = max([
            dataset_utils.box_intersection_over_area(box, b)
            for b in frame_detections.values() if box.obj_id != b.obj_id
        ])

        # return occluded_overlap > self.overlap_occ or occluded_pecentage > self.overlap_occ
        return occluded_pecentage > self.overlap_occ

    def find_max_iou_detection(self, gt_box):
        res = self.max_iou_detections_dict.get(gt_box)
        if res is None:
            # find max detections and insert to dict

            detections = self.det_boxes_dict[gt_box.video_name].get(gt_box.frame_number, [])
            if len(detections) > 0:
                iou_s = np.array([dataset_utils.box_intersection_over_union(
                    det_box,
                    gt_box
                ) for det_box in detections])

                max_detection_ind = np.argmax(iou_s)

                self.max_iou_detections_dict[gt_box] = detections[max_detection_ind], iou_s[max_detection_ind]

            else:
                self.max_iou_detections_dict[gt_box] = None, 0

        return self.max_iou_detections_dict.get(gt_box)

    def create_cuhk_sequences(self):
        self.cuhk_images_path = os.path.join(self.dataset_path, "cuhk03/images/detected")
        ids = set([n[:4] for n in os.listdir(self.cuhk_images_path)])
        assert len(ids) == 1467

        cuhk_sequences = []

        for id in ids:

            def create_samples_from_camera_boxes(camera_boxes):
                # returns 4 samples 2 positive and 2 negative
                # one positive sample is created by taking the last camera box as candidate and others as sequences, the
                # other positive sample is created by taking the first camera box as candidate and others as sequences
                # and then negative samples are created for each of the two positive samples

                samples = []

                def get_first_negative_sample(neg_id):
                    frame_number = 0
                    while True:

                        negative_candidate_box = Box(
                            video_name='cuhk', frame_number=frame_number, obj_id=neg_id,
                            bb_left=0, bb_top=0, bb_width=self.input_w, bb_height=self.input_h,
                            conf=1.0
                        )
                        assert frame_number < 10
                        if MOT2015.get_cuhk_image_path(negative_candidate_box, self.cuhk_images_path):
                            return negative_candidate_box
                        else:
                            frame_number += 1

                if len(camera_boxes) < 2:
                    return []

                other_ids = list(range(1, int(id))) + list(range(int(id) + 1, len(ids)))
                neg_ids = [i.zfill(4) for i in map(str, random.sample(other_ids, len(camera_boxes)))]

                for cand_ind in range(len(camera_boxes)):
                    # sample candidate first
                    sequence_boxes = camera_boxes[:cand_ind]+camera_boxes[cand_ind+1:]

                    if self.sequence_length >= len(sequence_boxes):
                        pad_size = self.sequence_length - len(sequence_boxes)
                        sequence_boxes = [sequence_boxes[0]]*pad_size + sequence_boxes

                    else:
                        sequence_boxes = sequence_boxes[:self.sequence_length]

                    positive_candidate_box = camera_boxes[cand_ind]
                    negative_candidate_box = get_first_negative_sample(neg_ids[cand_ind])

                    samples.append(self.create_sample(
                        candidate_box=positive_candidate_box,
                        candidate_label=1,
                        sequence_boxes=sequence_boxes,
                        sequence_velocities=None,
                        sequence_locations=None,
                        sequence_frame_numbers=None,
                        sequence_occupancy_grids=None))

                    samples.append(self.create_sample(
                        candidate_box=negative_candidate_box,
                        candidate_label=0,
                        sequence_boxes=sequence_boxes,
                        sequence_velocities=None,
                        sequence_locations=None,
                        sequence_frame_numbers=None,
                        sequence_occupancy_grids=None))


                # # sample candidate last
                # sequence_boxes = camera_boxes[:-1]
                # pad_size = self.sequence_length - len(sequence_boxes)
                # sequence_boxes = [sequence_boxes[0]]*pad_size + sequence_boxes
                #
                # positive_candidate_box = camera_boxes[-1]
                # negative_candidate_box = get_first_negative_sample(neg_ids[0])
                #
                # samples.append(self.create_sample(
                #     candidate_box=positive_candidate_box,
                #     candidate_label=1,
                #     sequence_boxes=sequence_boxes,
                #     sequence_velocities=None,
                #     sequence_locations=None,
                #     sequence_frame_numbers=None,
                #     sequence_occupancy_grids=None))
                #
                # samples.append(self.create_sample(
                #     candidate_box=negative_candidate_box,
                #     candidate_label=0,
                #     sequence_boxes=sequence_boxes,
                #     sequence_velocities=None,
                #     sequence_locations=None,
                #     sequence_frame_numbers=None,
                #     sequence_occupancy_grids=None))
                #
                # # sample candidate first
                # sequence_boxes = camera_boxes[1:]
                #
                # pad_size = self.sequence_length - len(sequence_boxes)
                # sequence_boxes = [sequence_boxes[0]]*pad_size + sequence_boxes
                # positive_candidate_box = camera_boxes[0]
                # negative_candidate_box = get_first_negative_sample(neg_ids[1])
                #
                # samples.append(self.create_sample(
                #     candidate_box=positive_candidate_box,
                #     candidate_label=1,
                #     sequence_boxes=sequence_boxes,
                #     sequence_velocities=None,
                #     sequence_locations=None,
                #     sequence_frame_numbers=None,
                #     sequence_occupancy_grids=None))
                #
                # samples.append(self.create_sample(
                #     candidate_box=negative_candidate_box,
                #     candidate_label=0,
                #     sequence_boxes=sequence_boxes,
                #     sequence_velocities=None,
                #     sequence_locations=None,
                #     sequence_frame_numbers=None,
                #     sequence_occupancy_grids=None))

                return samples

            first_camera_boxes = [
                Box(video_name='cuhk', frame_number=i, obj_id=id,
                    bb_left=0, bb_top=0, bb_width=self.input_w, bb_height=self.input_h,
                    conf=1.0
                    ) for i in range(5)
            ]
            first_camera_boxes = [
                b for b in first_camera_boxes if os.path.isfile(MOT2015.get_cuhk_image_path(b, self.cuhk_images_path))
            ]

            cuhk_sequences.extend(create_samples_from_camera_boxes(first_camera_boxes))

            second_camera_boxes = [
                Box(video_name='cuhk', frame_number=i, obj_id=id,
                    bb_left=0, bb_top=0, bb_width=self.input_w, bb_height=self.input_h,
                    conf=1.0
                    ) for i in range(5, 10)
            ]
            second_camera_boxes = [
               b for b in second_camera_boxes if os.path.isfile(MOT2015.get_cuhk_image_path(b, self.cuhk_images_path))
            ]
            cuhk_sequences.extend(create_samples_from_camera_boxes(second_camera_boxes))

        return cuhk_sequences

    def create_sequences(self, gt_boxes):
        sequences_ = []

        # kako oni rade
        # nadji id lika
        # nadji prvi box koji ima max(iou) sa detekcijama > overlap_pos
        # i max(ioa) sa drugim gt-ovima < overlap_occ (nije covered)
        # i da max iou detekcija ima ioa sa frameom > exit_thershold (unutar slike je)
        # od onda uzmi sve gt_detekcije tog lika do kraja

        # inicijaliziramo tracker sa detekcijom koja ima najveci overlap sa gt_om i damo joj id gt-a
        # ak je realocation vrtimo to, i uzmemo rezultat (detekciju s kojom je model spojio)
        # ako je overlap te detekcije sa gt-om > overlap_pos onda je okej
        # izvrsimo gt akciju i dodamo u dataset  sve primjere (featuri za sve detekcije i labele)
        # labele su -1 ali za onaj det s najvecim iou s gt je 1

        for i, gt_box in enumerate(gt_boxes):
            if i % 1000 == 0: print("create sequences step {}/{} video: {} ".format(i, len(gt_boxes), gt_box.video_name))

            # if the previous frame does not have the object we can't calculate the first velocity
            assert gt_box.video_name in self.gt_boxes_dict

            if self.velocities:

                if gt_box.frame_number-1 not in self.gt_boxes_dict[gt_box.video_name]: continue

                if gt_box.obj_id not in self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number-1]: continue

                # if the previous frame does not have the matching detection
                if self.find_max_iou_detection(
                        self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number-1][gt_box.obj_id])[1] < self.overlap_pos:
                    continue

            # if any of the following sequence_length frames does not have the object
            if any([gt_box.obj_id not in self.gt_boxes_dict[gt_box.video_name].get(gt_box.frame_number+i, []) for i in range(self.sequence_length)]):
                continue

            # if obj is occluded in the current frame
            if self.check_occluded(self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number][gt_box.obj_id]):
                continue

            # if obj has no detection in any of the following frames
            if any([
                self.find_max_iou_detection(
                    self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number+i][gt_box.obj_id]
                )[1] < self.overlap_pos for i in range(self.sequence_length)
            ]):
                continue

            sequence_boxes = [
                self.find_max_iou_detection(
                    self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number+i][gt_box.obj_id])[0]
                              for i in range(self.sequence_length)
            ]

            sequence_overlaps = np.array([
                self.find_max_iou_detection(
                    self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number+i][gt_box.obj_id])[1]
                for i in range(self.sequence_length)
            ])

            assert all(sequence_overlaps >= self.overlap_pos)

            pos_s = []
            neg_s = []

            if self.velocities:

                first_sequence_box = sequence_boxes[0]
                pre_sequence_box = self.find_max_iou_detection(
                    self.gt_boxes_dict[first_sequence_box.video_name][first_sequence_box.frame_number - 1][gt_box.obj_id]
                )[0]

                all_seq_boxes = [pre_sequence_box] + sequence_boxes
                all_seq_centers = dataset_utils.calc_centers(
                    np.array([dataset_utils.box_to_bbox(b) for b in all_seq_boxes]))

                # locations
                sequence_locations = [l for l in all_seq_centers]

                # frame_numbers
                sequence_frame_numbers = [b.frame_number for b in all_seq_boxes]

                # velocities
                sequence_velocities = all_seq_centers[1:] - all_seq_centers[:-1]
                sequence_velocities = [v for v in sequence_velocities]

            if self.occupancy_grids:
                sequence_occupancy_grids = [self.occupancy_grids_dict[box] for box in sequence_boxes]

            # find and add positive samples
            # we look at candidates from 6 frames in the future and take only the last
            following_sequence = [
                self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number + i] for i in
                                          range(self.sequence_length, self.sequence_length + self.future_seq_len)
                if gt_box.frame_number+i in self.gt_boxes_dict[gt_box.video_name]
            ]

            positive_candidate_boxes_gt = [fr[gt_box.obj_id] for fr in following_sequence if gt_box.obj_id in fr]
            positive_candidate_boxes_gt = [b for b in positive_candidate_boxes_gt if not self.check_occluded(b)]
            positive_candidate_boxes_gt = [b for b in positive_candidate_boxes_gt if self.find_max_iou_detection(b)[1] > self.overlap_pos]

            # random pick one
            if len(positive_candidate_boxes_gt) > 1:
                if not self.test_set:
                    # pick one if train
                    positive_candidate_boxes_gt = [random.choice(positive_candidate_boxes_gt)]

            else:
                positive_candidate_boxes_gt = []

            # gt_box.obj_id in fr and
            # not self.check_occluded(fr[gt_box.obj_id]) and
            # self.find_max_iou_detection(fr[gt_box.obj_id])[1] > self.overlap_pos)

            positive_candidate_boxes = [self.find_max_iou_detection(pos_cand_gt_box)[0] for pos_cand_gt_box in positive_candidate_boxes_gt]
            positive_candidate_ious = np.array([self.find_max_iou_detection(pos_cand_gt_box)[1] for pos_cand_gt_box in positive_candidate_boxes_gt])

            assert all(positive_candidate_ious > self.overlap_pos)
            assert all([c_b_gt.obj_id == gt_box.obj_id for c_b_gt in positive_candidate_boxes_gt])
            assert len(positive_candidate_boxes_gt) == len(positive_candidate_boxes)

            # if train,  one is already picked
            if not self.test_set and not self.all_futures:
                assert len(positive_candidate_boxes) <= 1

            for positive_candidate_box, positive_candidate_box_gt in zip(positive_candidate_boxes, positive_candidate_boxes_gt):
                assert type(positive_candidate_box) == Box

                positive_candidate_box = MOT2015.box_like_with_id(
                    positive_candidate_box, positive_candidate_box_gt.obj_id)

                sample = self.create_sample(candidate_box=positive_candidate_box, candidate_label=1,
                                            sequence_boxes=sequence_boxes,
                                            sequence_velocities=sequence_velocities if self.velocities else None,
                                            sequence_locations=sequence_locations if self.velocities else None,
                                            sequence_frame_numbers=sequence_frame_numbers if self.velocities else None,
                                            sequence_occupancy_grids=sequence_occupancy_grids if self.occupancy_grids else None)
                positive_sample = sample

                if self.test_set or not self.limit_ex_per_id:
                    # add if test set

                    # sequences.append(positive_sample)
                    pos_s.append(positive_sample)

                elif self.obj_id_counter[gt_box.video_name][gt_box.obj_id] < self.limit:

                    last_obj_frame = self.obj_id_frame_counter_pos[positive_candidate_box.video_name].get(positive_candidate_box.obj_id, None)
                    if last_obj_frame is None or sequence_boxes[0].frame_number-last_obj_frame > self.gap:

                        # sequences.append(positive_sample)
                        pos_s.append(positive_sample)
                        # self.obj_id_counter[positive_candidate_box.video_name][positive_candidate_box.obj_id]+=1
                        # self.obj_id_frame_counter_pos[positive_candidate_box.video_name][positive_candidate_box.obj_id]=positive_candidate_box.frame_number

            # find and add negative samples
            negative_sample_candidates = []
            for offset in range(self.future_seq_len):
                # we take candidates from future_seq_len frames in the future
                if gt_box.frame_number+self.sequence_length+offset not in self.gt_boxes_dict[gt_box.video_name]:
                    continue

                candidates = self.gt_boxes_dict[gt_box.video_name][gt_box.frame_number+self.sequence_length+offset].values()

                candidates = [cand for cand in candidates if not self.check_occluded(cand)]

                # leave only the candidates with corresponding detections
                candidates = [c for c in candidates if self.find_max_iou_detection(c)[1] > self.overlap_pos]

                if len(candidates) > 0:
                    # hard mining
                    if self.hard_examples:
                        negative_candidate_box_gt = MOT2015.find_hardest_negative_candidate_box(candidates, gt_box)

                    else:
                        negative_candidate_box_gt = MOT2015.find_random_negative_candidate_box(candidates, gt_box)

                    if negative_candidate_box_gt is not None:
                        negative_candidate_box, iou = self.find_max_iou_detection(negative_candidate_box_gt)
                        assert iou > self.overlap_pos

                        negative_candidate_box = MOT2015.box_like_with_id(
                            negative_candidate_box, negative_candidate_box_gt.obj_id)

                        if negative_candidate_box is not None:
                            negative_sample = self.create_sample(
                                candidate_box=negative_candidate_box,
                                candidate_label=0,
                                sequence_boxes=sequence_boxes,
                                sequence_velocities=sequence_velocities if self.velocities else None,
                                sequence_locations=sequence_locations if self.velocities else None,
                                sequence_frame_numbers=sequence_frame_numbers if self.velocities else None,
                                sequence_occupancy_grids=sequence_occupancy_grids if self.occupancy_grids else None
                            )

                            negative_sample_candidates.append(negative_sample)

            # random pick one
            if len(negative_sample_candidates) > 0:
                if self.test_set or self.all_futures:
                    # add all if test
                    for sam in negative_sample_candidates:
                        # sequences.append(sam)
                        neg_s.append(sam)

                else:
                    # pick one if train
                    neg_sample = random.choice(negative_sample_candidates)

                    if not self.limit_ex_per_id:
                        # add if not limit
                        # sequences.append(neg_sample)
                        neg_s.append(neg_sample)

                    elif self.obj_id_counter[gt_box.video_name][gt_box.obj_id] < self.limit:
                        # add if not above limit

                        last_obj_frame = self.obj_id_frame_counter_neg[gt_box.video_name].get(
                            gt_box.obj_id, None)

                        if last_obj_frame is None or neg_sample["sequence_boxes"][0].frame_number-last_obj_frame > self.gap:
                            # sequences.append(neg_sample)
                            neg_s.append(neg_sample)
                            # self.obj_id_counter[gt_box.video_name][gt_box.obj_id] += 1
                            # self.obj_id_frame_counter_neg[gt_box.video_name][gt_box.obj_id]=neg_sample["candidate_box"].frame_number

            if not self.test_set and not self.all_futures:
                if self.only_pairs:
                    if len(pos_s) != 1 or len(neg_s) != 1:
                        pos_s, neg_s = [], []

                assert len(pos_s) <= 1 and len(neg_s) <= 1

            if not self.test_set and self.limit_ex_per_id and not self.all_futures:
                if len(pos_s) > 0 and len(neg_s) > 0:
                    assert len(pos_s) == 1
                    assert len(neg_s) == 1
                    self.obj_id_counter[gt_box.video_name][gt_box.obj_id]+=1
                    self.obj_id_frame_counter_pos[gt_box.video_name][gt_box.obj_id]=pos_s[0]['candidate_box'].frame_number

                    self.obj_id_counter[gt_box.video_name][gt_box.obj_id] += 1
                    self.obj_id_frame_counter_neg[gt_box.video_name][gt_box.obj_id]=neg_s[0]["candidate_box"].frame_number

            sequences_.extend(pos_s)
            sequences_.extend(neg_s)

            if not self.test_set and self.limit_ex_per_id:
                assert len(sequences_) == sum(
                    chain(*[
                        obj_c.values() for obj_c in self.obj_id_counter.values()
                    ]))

        return sequences_

    @staticmethod
    def box_like_with_id(box, id):
        box_list = list(box)
        box_list[2] = id
        new_box = Box(*box_list)
        assert new_box.obj_id == id
        return new_box


    @staticmethod
    def find_random_negative_candidate_box(candidates, gt_box):
        for candidate_box in candidates:
            if gt_box.obj_id != candidate_box.obj_id and type(candidate_box) is not list:
                return candidate_box
        else:
            return None

    @staticmethod
    def find_hardest_negative_candidate_box(candidates, gt_box):
        assert gt_box.obj_id != -1
        neg_candidates_centers = {
            box: (
                box.bb_left + box.bb_width / 2,
                box.bb_top + box.bb_height / 2
            ) for box in candidates if box.obj_id != gt_box.obj_id and type(box) is not list}
        gt_center = (gt_box.bb_left + gt_box.bb_width / 2, gt_box.bb_top + gt_box.bb_height / 2)

        def man_dist(x, y):
            return abs(x[0] - y[0]) + abs(x[1] - y[1])

        if len(neg_candidates_centers) == 0:
            return None

        negative_candidate_box = min(neg_candidates_centers, key=lambda box: man_dist(neg_candidates_centers[box], gt_center))

        assert negative_candidate_box.obj_id != gt_box.obj_id
        assert type(negative_candidate_box) == Box

        return negative_candidate_box

    def create_sample(self, candidate_box, candidate_label, sequence_boxes=None, sequence_velocities=None, sequence_locations=None, sequence_frame_numbers=None, sequence_occupancy_grids=None):
        if sequence_boxes[-1].obj_id != -1:
            assert candidate_label == int(candidate_box.obj_id == sequence_boxes[-1].obj_id)

        if type(candidate_box) is not dataset_utils.Box:
            raise ValueError("Candidate box must be of type Box")

        if candidate_label not in [0, 1]:
            raise ValueError("Candidate label must be 0 or 1")

        sample = {
            "label": candidate_label
        }

        if sequence_boxes is None:
            raise ValueError("sequcence_boxes can't be None if self.crops is True")

        sample["sequence_boxes"] = sequence_boxes
        sample["candidate_box"] = candidate_box

        if self.velocities:
            if sequence_velocities is None:
                raise ValueError("sequcence_velocities can't be None if self.velocities is True")

            if sequence_boxes is None:
                raise ValueError("sequcence_boxes can't be None if self.velocities is True")

            cand_center = dataset_utils.box_center(candidate_box)
            # locations
            assert sequence_locations is not None
            sample["sequence_locations"] = sequence_locations  # sequence_len + 1
            sample["candidate_location"] = cand_center

            # velocities
            sample["sequence_velocities"] = sequence_velocities

            last_seq_center = dataset_utils.box_center(sequence_boxes[-1])
            sample["candidate_velocity"] = cand_center-last_seq_center

            # frame numbers
            sample["sequence_frame_numbers"] = sequence_frame_numbers  # sequence_len + 1
            sample["candidate_frame_number"] = candidate_box.frame_number

        if self.occupancy_grids:
            if sequence_occupancy_grids is None:
                raise ValueError("sequence_occupancy_grids can't be None if self.occupancy_grids is True")

            if self.occupancy_grids_dict is None:
                raise ValueError("occupancy_grids_dict wasn't built")

            sample["sequence_occupancy_grids"] = sequence_occupancy_grids
            sample["candidate_occupancy_grid"] = self.occupancy_grids_dict[self.box_like_with_id(candidate_box, -1)]

        return sample

    def __len__(self):
        return len(self.gt_boxes_sequences)

    @staticmethod
    def get_image_path(box, dataset_path):
        img_path = os.path.join(os.path.join(
            # dataset_path, '2DMOT2015/train', box.video_name, 'img1/',
            dataset_path, 'MOT/train', box.video_name, 'img1/',
            str(int(box.frame_number)).zfill(6) + ".jpg")
        )
        return img_path

    @staticmethod
    def get_cuhk_image_path(box, cuhk_images_path):
        image_path = os.path.join(
            cuhk_images_path,
            "{}_{}.jpg".format(box.obj_id, str(box.frame_number).zfill(2))
        )
        return image_path

    def crop_and_resize_box_from_frame(self, box):
        if box.video_name == "cuhk":
            image_path = MOT2015.get_cuhk_image_path(box, self.cuhk_images_path)
            image = cv2.imread(image_path)
            assert image is not None
            return image


        else:
            assert box.video_name in self.video_names
            img_name = self.get_image_path(box, self.dataset_path)
            image = cv2.imread(img_name)

            assert image is not None
            # crop and add 4 for context

            image = image[
                    int(np.floor(box.bb_top  - self.margin)): int(np.ceil(box.bb_top  + box.bb_height + 2*self.margin)),
                    int(np.floor(box.bb_left - self.margin)): int(np.ceil(box.bb_left + box.bb_width  + 2*self.margin))
                    ]

            if image.size == 0:
                assert all(np.array([box.bb_top, box.bb_left, box.bb_height, box.bb_width]) > 0)

            assert image.size > 0

            return cv2.resize(image, dsize=(self.input_h, self.input_w), interpolation=cv2.INTER_CUBIC)

    def __getitem__(self, idx):
        # index of the last box to be fed into the LSTM

        sample = {"label": self.gt_boxes_sequences[idx]['label']}

        boxes = self.gt_boxes_sequences[idx]["sequence_boxes"]
        # sample["sequence_boxes"] = boxes

        candidate_box = self.gt_boxes_sequences[idx]["candidate_box"]
        # sample["candidate_box"] = candidate_box

        if self.crops:
            # crop boxes from frames and reshape to fixes size

            preprocessed_boxes = []
            for i, box in enumerate(boxes):
                image = self.crop_and_resize_box_from_frame(box)
                preprocessed_boxes.append(image)

            # preprocessed_boxes = np.stack(preprocessed_boxes)
            candidate_crop = self.crop_and_resize_box_from_frame(candidate_box)

            sample["image_crops"] = preprocessed_boxes
            sample["candidate_crop"] = candidate_crop

        if self.velocities:
            sample["sequence_velocities"] = self.gt_boxes_sequences[idx]["sequence_velocities"]
            sample["candidate_velocity"] = self.gt_boxes_sequences[idx]["candidate_velocity"]

            sample["sequence_locations"] = self.gt_boxes_sequences[idx]["sequence_locations"]
            sample["candidate_location"] = self.gt_boxes_sequences[idx]["candidate_location"]

            sample["sequence_frame_numbers"] = self.gt_boxes_sequences[idx]["sequence_frame_numbers"]
            sample["candidate_frame_number"] = self.gt_boxes_sequences[idx]["candidate_frame_number"]

        if self.occupancy_grids:
            sample["sequence_occupancy_grids"] = self.gt_boxes_sequences[idx]["sequence_occupancy_grids"]
            sample["candidate_occupancy_grid"] = self.gt_boxes_sequences[idx]["candidate_occupancy_grid"]

        if self.transform:
            sample = self.transform(sample)

        return sample


class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __init__(self, crops=True, velocities=True, occupancy_grids=True, augment=False):
        self.crops = crops
        self.velocities = velocities
        self.occupancy_grids = occupancy_grids

        self.augment = augment
        self.toTen = transforms.ToTensor()
        self.toPil = transforms.ToPILImage()
        if self.augment:

            self.vertical_flip = lambda x: torch.index_select(x, 1, torch.tensor(range(x.shape[1]-1, -1, -1)))
            self.vertical_flip = transforms.RandomVerticalFlip(1.0)
            self.vertical_flip = tr_F.vflip

            self.horizontal_flip = lambda x: torch.index_select(x, 2, torch.tensor(range(x.shape[2]-1, -1, -1)))
            self.horizontal_flip = transforms.RandomHorizontalFlip(1.0)
            self.horizontal_flip = tr_F.hflip

    def __call__(self, sample):
        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W

        sample["label"] = torch.tensor(sample["label"]).float()
        # crops
        if self.crops:
            image_crops, candidate_crop = sample['image_crops'], sample['candidate_crop']

            image_crops = [self.toPil(image_crop) for image_crop in image_crops]
            candidate_crop = self.toPil(candidate_crop)

            if self.augment:

                if random.random() < 0.5:
                    image_crops = [self.horizontal_flip(image_crop) for image_crop in image_crops]
                    candidate_crop = self.horizontal_flip(candidate_crop)

                if random.random() < 0.2:
                    factor = np.random.uniform(0.6, 1.4)
                    image_crops = [tr_F.adjust_brightness(image_crop, factor) for image_crop in image_crops]
                    candidate_crop = tr_F.adjust_brightness(candidate_crop, factor)

                if random.random() < 0.2:
                    factor = np.random.uniform(0.6, 1.4)
                    image_crops = [tr_F.adjust_contrast(image_crop, factor) for image_crop in image_crops]
                    candidate_crop = tr_F.adjust_contrast(candidate_crop, factor)

                if random.random() < 0.2:
                    factor = np.random.uniform(0.6, 1.4)
                    image_crops = [tr_F.adjust_saturation(image_crop, factor) for image_crop in image_crops]
                    candidate_crop = tr_F.adjust_saturation(candidate_crop, factor)

                if random.random() < 0.2:
                    factor = np.random.uniform(-0.1, 0.1)
                    image_crops = [tr_F.adjust_hue(image_crop, factor) for image_crop in image_crops]
                    candidate_crop = tr_F.adjust_hue(candidate_crop, factor)

            image_crops = [
                (self.toTen(image_crop).float()*255)
                for i, image_crop in enumerate(image_crops)
            ]
            candidate_crop = self.toTen(candidate_crop).float()*255

            sample["image_crops"] = image_crops
            sample["candidate_crop"] = candidate_crop

        if self.velocities:
            # velocities
            sequence_velocities, candidate_velocity = sample['sequence_velocities'], sample['candidate_velocity']
            sequence_velocities = [
                torch.from_numpy(velocities).float()
                for i, velocities in enumerate(sequence_velocities)
            ]
            candidate_velocity = torch.from_numpy(candidate_velocity).float()

            sample["sequence_velocities"] = sequence_velocities
            sample["candidate_velocity"] = candidate_velocity

            # locations
            sequence_locations, candidate_location = sample['sequence_locations'], sample['candidate_location']
            sequence_locations = [
                torch.from_numpy(locations).float()
                for i, locations in enumerate(sequence_locations)
            ]
            candidate_location = torch.from_numpy(candidate_location).float()

            sample["sequence_locations"] = sequence_locations
            sample["candidate_location"] = candidate_location

            # frame num
            sequence_frame_numbers, candidate_frame_number = sample['sequence_frame_numbers'], sample['candidate_frame_number']
            # sequence_frame_numbers = [
            #     torch.from_numpy(frame_numbers).float()
            #     for i, frame_numbers in enumerate(sequence_frame_numbers)
            # ]
            sequence_frame_numbers = torch.tensor(sequence_frame_numbers)
            candidate_frame_number = torch.tensor(candidate_frame_number)

            sample["sequence_frame_numbers"] = sequence_frame_numbers
            sample["candidate_frame_number"] = candidate_frame_number

        if self.occupancy_grids:
            sequence_occupancy_grids, candidate_occupancy_grid = sample['sequence_occupancy_grids'], sample['candidate_occupancy_grid']
            sequence_occupancy_grids = [
                torch.from_numpy(occupancy_grid).float()
                for i, occupancy_grid in enumerate(sequence_occupancy_grids)
            ]
            candidate_velocity = torch.from_numpy(candidate_occupancy_grid).float()

            sample["sequence_occupancy_grids"] = sequence_occupancy_grids
            sample["candidate_occupancy_grid"] = candidate_velocity

        return sample


def get_loaders(
        batch_size,
        shuffle=False,
        crops=True,
        velocities=True,
        boxes=False,
        occupancy_grids=True,
        future_seq_len=6,
        num_workers=0,
        train=True,
        test=True,
        train_sequence_length=6,
        test_sequence_length=6,
):
    if train:
        print("no augment")
        composed = transforms.Compose([ToTensor(crops=crops, velocities=velocities, occupancy_grids=occupancy_grids, augment=True)])
        train_dataset = MOT2015(composed, crops=crops, velocities=velocities, occupancy_grids=occupancy_grids, test_set=False, sequence_length=train_sequence_length, future_seq_len=future_seq_len)
        train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers, drop_last=True)
    else:
        train_loader = None

    if test:
        composed = transforms.Compose([ToTensor(crops=crops, velocities=velocities, occupancy_grids=occupancy_grids)])
        test_dataset = MOT2015(composed, crops=crops, velocities=velocities, occupancy_grids=occupancy_grids, test_set=True, sequence_length=test_sequence_length)
        test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False, num_workers=num_workers)
    else:
        test_loader = None

    return train_loader, test_loader
