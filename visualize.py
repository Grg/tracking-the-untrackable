import os

import cv2
import matplotlib.pyplot as plt
import pandas as pd

from datasets.tracking.MOT2015_loader import create_sequence_gt_boxes_dict, parse_gt_boxes

gt_boxes = pd.read_csv(os.path.join("./results/AppearanceModel/TUD-Crossing.txt"), header=None).as_matrix()[:,:7]
gt_boxes = parse_gt_boxes(gt_boxes, "TUD-Crossing.txt")
gt_boxes_dict = create_sequence_gt_boxes_dict(gt_boxes)

def get_image_path(box):
    img_path = os.path.join(os.path.join(
        './data/2DMOT2015/test/TUD-Crossing/img1/',
        str(int(box.frame_number)).zfill(6) + ".jpg")
    )
    return img_path

for frame_num, boxes in gt_boxes_dict.items():
    im = cv2.imread(get_image_path(boxes[0]))
    plt.scatter(boxes[0].bb_left, boxes[0].bb_top)
    plt.imshow(im)


    plt.show(block=True)
