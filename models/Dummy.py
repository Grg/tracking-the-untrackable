import torch
import numpy as np
from collections import Counter
import os
import itertools
import random
from IPython import embed
import  matplotlib.pyplot as plt
import torch.nn as nn
from torchvision import transforms, utils, models
import subprocess

from .Model import Model
from .Appearance import AppearanceModel
from .Motion import MotionModel
from .Interaction import InteractionModel


class DummyModel(Model):
    def __init__(self):
        super(DummyModel, self).__init__()

    def set_dataset(self, dataset):
        self.dataset = dataset

    @staticmethod
    def bb_intersection_over_union(boxA, boxB):
        # determine the (x, y)-coordinates of the intersection rectangle
        xA = max(boxA[0], boxB[0])
        yA = max(boxA[1], boxB[1])
        xB = min(boxA[0] + boxA[2], boxB[0] + boxB[2])
        yB = min(boxA[1] + boxA[3], boxB[1] + boxB[3])

        # compute the area of intersection rectangle
        interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

        # compute the area of both the prediction and ground-truth
        # rectangles
        boxAArea = (boxA[0] + boxA[2] - boxA[0] + 1) * (boxA[1]+boxA[3] - boxA[1] + 1)
        boxBArea = (boxB[0] + boxB[2] - boxB[0] + 1) * (boxB[1]+boxB[3] - boxB[1] + 1)

        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        iou = interArea / float(boxAArea + boxBArea - interArea)

        # return the intersection over union value
        assert  iou >= 0
        assert  iou <= 1.0
        return iou


    def forward(self, sample, tracking=False, video_name=None, frame=None):
        # tracking mode
        # objects = [{ 'id': <id>, 'crops': [<tensor at step 1>, ..., <tensor at step 6>] }]
        objects, candidate_crops = sample['objects'], sample['candidates_crops']

        obj_true_ids = {}
        for id, o in objects.items():
            bboxes = o['bboxes']
            frames_numbers = o['frames_numbers']
            o_ids = []
            for bbox, frame_num in zip(bboxes, frames_numbers):
                detections = self.dataset.gt_videos_dict[video_name][frame_num]["detections"]
                gt_detections = [DummyModel.box_to_bbox(b) for b in detections]

                overlaps = [self.bb_intersection_over_union(det, bbox) for det in gt_detections]

                if len(overlaps) > 0:
                    ind = np.argmax(overlaps)
                    max_o = np.max(overlaps)
                    detection = detections[ind]

                    if max_o >= 0.5:
                        det_id = detection.obj_id
                        o_ids.append(det_id)

                    # if max_o < 0.5:
                    #     print("max_o:", max_o, "len:", len(frames_numbers))

            if len(o_ids) > 0:
                true_id = max(Counter(o_ids).items(), key=lambda kv:kv[1])[0]
            else:
                true_id = -200
            obj_true_ids[id] = true_id

        print("obj_true_ids:", obj_true_ids)

        candidates = sample['candidates_boxes']
        candidate_true_ids = {}
        detections = self.dataset.gt_videos_dict[video_name][frame]["detections"]
        gt_detections = [DummyModel.box_to_bbox(b) for b in detections]

        for cand in candidates:
            cand_bbox = DummyModel.box_to_bbox(cand)

            overlaps = [self.bb_intersection_over_union(det, cand_bbox) for det in gt_detections]
            if len(overlaps) > 0:
                max_o = np.max(overlaps)
                ind = np.argmax(overlaps)

                detection = detections[ind]
                id = detection.obj_id if max_o > 0.5 else -100

            else:
                id = -100

            candidate_true_ids[cand.obj_id] = id

        print("candi_true_ids:", candidate_true_ids)


        num_objects = len(objects)
        num_candidates = len(candidates)
        objects_ids, objects_ = zip(*objects.items())
        objects_ids = np.array(objects_ids)
        objects_ids_tiled = objects_ids.repeat([num_candidates]).reshape(num_objects*num_candidates, 1) # 1,1,2,2,3,3

        candidates_ids_tiled = torch.tensor(sample["candidates_ids"]).repeat([num_objects, 1]).reshape(num_objects*num_candidates, 1) # 1,2,3,1,2,3
        edges = np.concatenate((objects_ids_tiled, candidates_ids_tiled), axis=1)

        logits = []
        for edge in edges:
            true_id = obj_true_ids[edge[0]]
            cand_true_id = candidate_true_ids[edge[1]]
            logit = int(true_id == cand_true_id)*2-1
            logits.append(logit)

        print("dummy model")
        from IPython import embed
        embed()

        logits = torch.Tensor(np.array(logits))
        logs = np.array(logits > 0.)

        # print('num_obj:',num_objects)
        # print('num_cand:',num_candidates)
        # print("edges(o,c):", edges)
        # print("probs:", logits)
        # if num_objects > 20:
        #     no = len([v for v in obj_true_ids.values() if v < 0])
        #     f, axarr = plt.subplots(1+no, max(1, num_candidates), figsize=(15, 15))
        #
        #     objects, candidate_crops = sample['objects'], sample['candidates_crops']
        #     candidates_ids = sample["candidates_ids"]
        #     assert len(candidates_ids) == len(candidate_crops)
        #     for i, (c_id, crop) in enumerate(zip(candidates_ids, candidate_crops)):
        #         crop = np.array(crop[0]).swapaxes(0, -1).swapaxes(0, 1)
        #         axarr[0, i].imshow(crop/255)
        #         axarr[0, i].set_title('c_'+str(int(c_id)))
        #
        #     assert len(objects_ids) == len(objects)
        #     o_itesm =[(k,v) for k,v in objects.items() if obj_true_ids[k] < 0]
        #     for i, (o_id, obj) in enumerate(o_itesm):
        #         crops = obj['crops']
        #         for j, crop in enumerate(crops):
        #             crop = np.array(crop[0]).swapaxes(0, -1).swapaxes(0, 1)
        #             axarr[i+1, -1].imshow(crop/255)
        #             axarr[i+1, -1].set_title('o_'+str(int(o_id)))
        #
        #     plt.show(block=True)


        return edges, logits, None


    def load_state(self, paths, resume_train=False, optimizer=None):
        return
