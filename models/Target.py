import torch
import numpy as np
import os
import itertools
import random
from IPython import embed
import  matplotlib.pyplot as plt
import torch.nn as nn
from torchvision import transforms, utils, models
import subprocess

from .Model import Model
from .Appearance import AppearanceModel
from .Motion import MotionModel
from .Interaction import InteractionModel


class TargetModel(Model):
    def __init__(self, k=3, H=3, mode=["A", "M"], hidden=True, device=None,
                 appearance_model=None, motion_model=None, interaction_model=None, aux_heads=False, sequence_len=6):
        super(TargetModel, self).__init__()

        # todo: move this to apstract class constructor
        self.device = device if device else torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        # self.device = 'cpu'
        print("TargetModel device:"+str(self.device))
        self.mode = mode

        self.k = k
        self.H = H
        self.hidden = hidden

        self.A = "A" in self.mode
        self.M = "M" in self.mode
        self.I = "I" in self.mode

        self.sequence_len = sequence_len

        if appearance_model is not None: assert self.A
        if motion_model is not None: assert self.M
        if interaction_model is not None: assert self.I

        if self.A:
            if appearance_model is None:
                self.appearance_model = AppearanceModel(
                    k=100, H=128,
                    siamese=True, device=self.device, hidden=True,
                    use_bn=True, train_backbone=True, backbone=models.resnet18(pretrained=True)
                )
            else:
                self.appearance_model = appearance_model

        if self.M:
            if motion_model is None:
                self.motion_model = MotionModel(k=k, H=H, hidden=True, device=device)
            else:
                self.motion_model = motion_model

        if self.I:
            if interaction_model is None:
                self.interaction_model = InteractionModel(k=k, H=H, occupancy_grid_size=7**2, device=device)
            else:
                self.interaction_model = interaction_model

        # RNN
        self.lstm = nn.LSTM(500+self.k, self.H, batch_first=False)

        # FC
        self.fc = nn.Linear(self.H, self.k, bias=True)
        self.relu = nn.ReLU()
        self.bn = nn.BatchNorm1d(self.k)

        # hidden FC
        if self.hidden:
            self.hidden_fc = nn.Linear(self.k, self.k, bias=True)
            self.hidden_relu = nn.ReLU(inplace=True)
            self.hidden_fc_layer = lambda x: self.hidden_relu(self.hidden_fc(x))

        self.last_fc = nn.Linear(self.k, 1, bias=False)
        self.to(self.device)

        self.aux_heads = aux_heads
        if self.aux_heads:
            print("add heads")
            self.aux_fc1 = []
            self.aux_fc2 = []
            self.aux_fc = []

            for i in range(self.sequence_len - 1):
                self.aux_fc1.append(nn.Linear(self.H, self.H, bias=True).to(self.device))
                self.aux_fc2.append(nn.Linear(self.H, 1, bias=False).to(self.device))

                l = lambda x: self.aux_fc2[i](self.aux_fc1[i](x))
                self.aux_fc.append(l)

    def forward(self, sample, tracking=False):

        if not tracking:
            # classification mode
            features = []
            for i in range(self.sequence_len):
                current_sample = {}

                # assert all([n in sample.keys() for n in ["image_crops", "sequence_velocities", "sequence_occupancy_grids"]])
                # assert all([n in sample.keys() for n in ["sequence_velocities", "sequence_occupancy_grids"]])
                assert all([n in sample.keys() for n in ["image_crops", "sequence_velocities"]])
                for k, v in sample.items():
                    if k in ["image_crops", "sequence_velocities", "sequence_occupancy_grids", "sequence_frame_numbers", "sequence_locations"]:
                        if k == "sequence_frame_numbers":
                            current_sample[k] = sample[k][:, i:i+6]

                        else:
                            current_sample[k] = sample[k][i: i+6]
                    else:
                        current_sample[k] = v

                models_features = []
                if self.A:
                    _, appearance_features = self.appearance_model(current_sample)
                    models_features.append(appearance_features)

                if self.M:
                    _, motion_features = self.motion_model(current_sample)
                    models_features.append(motion_features)

                if self.I:
                    _, interaction_features = self.interaction_model(current_sample)
                    models_features.append(interaction_features)

                features.append(torch.cat(models_features, dim=1))

            features = torch.stack(features)  # (time_steps, batch_size, features_size)

            # Forward propagate LSTM
            trajectory_features_sequence, _ = self.lstm(features)
            trajectory_features = trajectory_features_sequence[-1, :, :]  # last state

            final_features = self.bn(self.relu(self.fc(trajectory_features)))

            if self.hidden:
                final_features = self.hidden_fc_layer(final_features)

            logits = self.last_fc(final_features).view(-1)

            if self.aux_heads and self.training:
                aux_logits = []

                # every step head
                for i, obj_step_features in enumerate(trajectory_features_sequence[:-1]):

                    step_logits = self.aux_fc[i](obj_step_features.to(self.device))

                    aux_logits.append(step_logits)

                logits = (logits, *aux_logits)

            # logs = np.array(logits > 0.)
            # labs = np.array(sample["label"])
            # display_objects_pos = [i for  i, (lo,la) in enumerate(zip(logs,labs)) if lo == la][:2]
            # display_objects_neg = [i for  i, (lo,la) in enumerate(zip(logs,labs)) if lo != la][:2]
            #
            # display_objects = display_objects_pos + display_objects_neg
            #
            # if len(display_objects_neg) == 0:
            #     return logits, final_features
            #
            # print('logs:', logs[display_objects])
            # print('labs:', labs[display_objects])
            #
            # f, axarr = plt.subplots(len(display_objects), 6+5+1, figsize=(50, 50))
            #
            # for y,o in enumerate(display_objects):
            #     for i, crop in enumerate(sample["image_crops"]):
            #         crop = np.array(crop[o]).swapaxes(0, -1).swapaxes(0, 1)
            #         axarr[y, i].imshow(crop/255)
            #         axarr[y, i].set_title('obj_t'+str(i))
            #
            #     crop = np.array(sample["candidate_crop"][o]).swapaxes(0, -1).swapaxes(0, 1)
            #     axarr[y, -1].imshow(crop/255)
            #     axarr[y, -1].set_title('cand')
            #
            # plt.show(block=True)

            return logits, final_features

        else:
            # tracking mode
            # objects = [{ 'id': <id>, 'crops': [<tensor at step 1>, ..., <tensor at step 6>] }]

            features = []
            # print("new steps")
            for i in range(6):
                current_sample = dict()
                for sample_k, sample_v in sample.items():

                    if sample_k == "objects":
                        current_objects = dict()
                        for obj_id, obj in sample_v.items():

                            current_obj = dict()
                            for obj_key, obj_val in obj.items():
                                if obj_key in ["crops", "locations", "velocities", 'occupancy_grids']:
                                    current_obj[obj_key] = obj_val[i: i+6]

                                elif obj_key in ["frames_numbers"]:
                                    total_len = len(obj['velocities'])
                                    assert len(obj_val) == total_len
                                    current_obj[obj_key] = obj_val[i: i+6]


                                else:
                                    current_obj[obj_key] = obj_val

                            current_objects[obj_id] = current_obj

                        current_sample[sample_k] = current_objects

                    else:
                        current_sample[sample_k] = sample_v

                models_features = []
                if self.A:
                    a_edges, _, appearance_features = self.appearance_model(current_sample, tracking=True)
                    edges = a_edges
                    models_features.append(appearance_features)

                if self.M:
                    m_edges, _, motion_features = self.motion_model(current_sample, tracking=True)
                    edges = m_edges
                    models_features.append(motion_features)

                if self.I:
                    i_edges, _, interaction_features = self.interaction_model(current_sample, tracking=True)
                    edges = i_edges
                    models_features.append(interaction_features)

                np.testing.assert_equal(a_edges, m_edges)  # todo: malo pametnije ovo

                features.append(torch.cat(models_features, dim=1))
                # features.append(torch.cat((appearance_features, motion_features, interaction_features), dim=1))

            # Forward propagate LSTM
            features = torch.stack(features)  # (time_steps, batch_size, features_size)
            trajectory_features_sequence, _ = self.lstm(features)
            trajectory_features = trajectory_features_sequence[-1, :, :] # last state

            final_features = self.bn(self.relu(self.fc(trajectory_features)))

            if self.hidden:
                final_features = self.hidden_fc_layer(final_features)

            logits = self.last_fc(final_features).view(-1)

            objects = sample["objects"]
            objects_ids, objects = zip(*objects.items())
            num_objects = len(objects)

            candidate_crops = sample["candidates_crops"]
            candidates_ids = sample["candidates_ids"]
            num_candidates = len(candidate_crops)

            # print('num_obj:',num_objects)
            # print('num_cand:',num_candidates)
            # print("edges(o,c):", edges)
            # print("probs:", logits)

            def sigmoid(x, derivative=False):
                return x * (1 - x) if derivative else 1 / (1 + np.exp(-x))

            # np.set_printoptions(suppress=True)
            # res = np.concatenate((edges, sigmoid(logits.cpu().detach().numpy().reshape(-1, 1))), axis=1)
            # print("edges, probs:", res)
            #
            # if np.max(res[:, -1]) > 0.5:
            #     seq_len = len(objects[0]['crops'])
            #     f, axarr = plt.subplots(1+num_objects, max(seq_len, num_candidates), figsize=(15, 15))
            #
            #     assert len(candidates_ids) == len(candidate_crops)
            #     for i, (c_id, crop) in enumerate(zip(candidates_ids, candidate_crops)):
            #         crop = np.array(crop[0]).swapaxes(0, -1).swapaxes(0, 1)[:,:,[2,1,0]]
            #
            #         axarr[0, i].imshow(crop/255)
            #         axarr[0, i].set_title('c_'+str(int(c_id)))
            #
            #     assert len(objects_ids) == len(objects)
            #     for i, (o_id, obj) in enumerate(zip(objects_ids, objects)):
            #         crops = obj['crops']
            #         for j, crop in enumerate(crops):
            #             crop = np.array(crop[0]).swapaxes(0, -1).swapaxes(0, 1)[:,:,[2,1,0]]
            #             axarr[i+1, j].imshow(crop/255)
            #             axarr[i+1, j].set_title('o_'+str(int(o_id)))
            #
            #     plt.show(block=True)

            return edges, logits, final_features


    def save_state(self, path, optimizer=None, epoch=None, loss=None):
        print("Saving state to ", path)
        # create subdirectories if they don't exits
        dirname = os.path.dirname(os.path.abspath(path))
        if not os.path.exists(dirname):
            os.makedirs(dirname)

        torch.save({
            k: v for k,v in [
            ('epoch', epoch),
            ('model_state_dict', self.state_dict()),
            ('optimizer_state_dict', optimizer.state_dict()),
            ('loss', loss)
        ] if v is not None
        }, path)

    def load_state(self, paths, resume_train=False, optimizer=None):
        if type(paths) == list:
            appearance_path = paths[0]
            motion_path = paths[1]
            # interaction_path = paths[2]

            self.appearance_model.load_state(
                appearance_path, resume_train=False)
            print("AppearanceModel loaded")

            self.motion_model.load_state(
                motion_path, resume_train=False)
            print("MotionModel loaded")

            # self.interaction_model.load_state(
            #     interaction_path, resume_train=False)
            # print("InteractionModel loaded")

        else:
            assert type(paths) == str
            super(TargetModel, self).load_state(paths, resume_train=resume_train, optimizer=optimizer)

        print("TargetModel loaded")
        epoch = 1
        return epoch
