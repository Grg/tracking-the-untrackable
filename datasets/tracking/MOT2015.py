from collections import defaultdict
import os
import cv2
import torch
from itertools import chain

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
from collections import namedtuple
from IPython import embed

# Ignore warnings
import warnings
import glob

import datasets.dataset_utils as dataset_utils

warnings.filterwarnings("ignore")


def create_frames_dict(boxes):
    frames_dict = defaultdict(lambda: defaultdict(list))

    for box in boxes:
        assert type(box) == dataset_utils.Box
        frames_dict[box.frame_number]["detections"].append(box)
        frames_dict[box.frame_number]["video_name"] = box.video_name
        frames_dict[box.frame_number]["frame_number"] = box.frame_number

    return dict(frames_dict)


class MOT2015(Dataset):
    """2DMOT2015 dataset."""

    def __init__(
            self,
            transform=None,
            crops=True,
            velocities=True,
            occupancy_grids=True,
            balanced_batch=True,
            boxes=True,
            full_image_grid_dim=15,
            occupancy_grid_dim=7,
            input_w=224,
            input_h=224,
            test_set=False,
            dataset_path="/home/gkovac/Projekt/tracking-the-untrackable/data/"
    ):

        self.balanced_batch = balanced_batch
        self.crops = crops
        self.velocities = velocities
        self.occupancy_grids = occupancy_grids
        self.boxes = boxes

        self.input_w, self.input_h = input_w, input_h
        self.margin = 0
        self.full_image_grid_dim = full_image_grid_dim
        assert full_image_grid_dim % 2 == 1

        self.occupancy_grid_dim = occupancy_grid_dim
        assert occupancy_grid_dim % 2 == 1

        self.dataset_path = dataset_path
        self.transform = transform

        print("dataset_type: ", "test" if test_set else "train")
        self.video_names = os.listdir(os.path.join(dataset_path, "2DMOT2015/train"))

        if test_set:
            # self.video_names = ["TUD-Campus", "ETH-Sunnyday", "ETH-Pedcross2", "ADL-Rundle-8", "Venice-2", "KITTI-17"]

            self.video_names = ["TUD-Campus", "ETH-Sunnyday"]

        else:
            self.video_names = ["TUD-Stadtmitte", "ETH-Bahnhof", "ADL-Rundle-6", "KITTI-13"]

        print("video_names:", self.video_names)

        self.det_videos_dict = {}
        self.gt_videos_dict = {}

        for video_name in self.video_names:
            frame_h, frame_w, _ = cv2.imread(glob.glob(os.path.join(self.dataset_path,'2DMOT2015/train/', video_name, 'img1/*.jpg'))[0]).shape

            # print("det")
            # det_path = os.path.join(dataset_path, '2DMOT2015/train/', video_name, 'det/det.txt')
            # boxes = pd.read_csv(det_path, header=None).as_matrix()[:, :7]
            # boxes = dataset_utils.parse_boxes(boxes, video_name, frame_h, frame_w)
            # boxes = [b for b in boxes if b.conf > 10.0]

            print("yolo")
            det_path = os.path.join(dataset_path, '2DMOT2015/train/', video_name, 'det/yolo_det.txt')
            boxes = pd.read_csv(det_path, header=None).as_matrix()[:, :7]
            boxes = dataset_utils.parse_boxes(boxes, video_name, frame_h, frame_w)
            boxes = [b for b in boxes if b.conf > 70.0]
            # print("gt")
            # gt_path = os.path.join(dataset_path, '2DMOT2015/train/', video_name, 'gt/gt.txt')
            # boxes = pd.read_csv(gt_path, header=None).as_matrix()[:, :7]

            gt_path = os.path.join(dataset_path, '2DMOT2015/train/', video_name, 'gt/gt.txt')
            gt_boxes = pd.read_csv(gt_path, header=None).as_matrix()[:, :7]
            gt_boxes = dataset_utils.parse_boxes(gt_boxes, video_name, frame_h, frame_w)
            gt_boxes = [b for b in gt_boxes if b.conf > 0.5]

            assert set([b.conf for b in gt_boxes]) == {1}

            self.det_videos_dict[video_name] = create_frames_dict(boxes)
            self.gt_videos_dict[video_name] = create_frames_dict(gt_boxes)

        self.frames = []
        for video_name in self.video_names:
            last_video_frame = int(max(self.gt_videos_dict[video_name].keys()))
            for frame_number in range(1, last_video_frame+1):

                frame = self.det_videos_dict[video_name].get(frame_number, {})

                if len(frame) > 0:
                    assert video_name == frame['video_name']
                    assert frame_number == frame['frame_number']

                else:
                    frame["video_name"] = video_name
                    frame["frame_number"] = frame_number
                    frame["detections"] = []

                self.frames.append(frame)

        print("dataset created")

    def get_coordinates_in_grid(self, box):
        # todo: do this in a smarter way
        img = cv2.imread(self.get_image_path(box.video_name, box.frame_number))
        height, width, _ = img.shape

        cell_h = height // self.full_image_grid_dim
        cell_w = width  // self.full_image_grid_dim

        h_idx = int((box.bb_top  + (box.bb_height // 2)) // cell_h)
        w_idx = int((box.bb_left + (box.bb_width  // 2)) // cell_w)

        return h_idx, w_idx

    def create_occupancy_grid(self, box, neighbours):
        occupancy_grid = np.zeros((self.occupancy_grid_dim, self.occupancy_grid_dim), dtype=np.float)
        grid_center_x = int(self.occupancy_grid_dim // 2)
        grid_center_y = int(self.occupancy_grid_dim // 2)
        box_y, box_x = self.get_coordinates_in_grid(box)

        # for neighbour_id, neighbour in self.gt_boxes_dict[box.video_name][box.frame_number].items():
        for neighbour in neighbours:

            if type(neighbour) != dataset_utils.Box:
                # todo: fix this bug, why does this happen
                print("type of neighbour must be box and it is ", type(neighbour) )
                print("Wierd list appears")
                exit()

            neighbour_y, neighbour_x = self.get_coordinates_in_grid(neighbour)

            delta_y = neighbour_y - box_y
            delta_x = neighbour_x - box_x

            if abs(delta_y) <= (self.occupancy_grid_dim // 2) and \
                            abs(delta_x) <= self.occupancy_grid_dim // 2:
                # neighbour is inside the occupancy grid

                grid_y = grid_center_y + delta_y
                grid_x = grid_center_x + delta_x

                occupancy_grid[grid_y][grid_x] = 1.0

        return occupancy_grid.flatten()

    def __len__(self):
        return len(self.frames)

    def get_image_path(self, video_name=None, frame_number=None):
        img_path = os.path.join(os.path.join(
            # self.dataset_path, '2DMOT2015/test', box.video_name,'img1/',
            self.dataset_path, '2DMOT2015/train', video_name, 'img1/',
            str(int(frame_number)).zfill(6) + ".jpg")
        )
        return img_path

    def crop_and_resize_box_from_frame(self, box):
        img_name = self.get_image_path(box.video_name, box.frame_number)
        image = cv2.imread(img_name)

        assert image is not None
        # crop and add 4 for context
        image = image[
            int(np.floor(box.bb_top - self.margin)): int(np.ceil(box.bb_top + box.bb_height + 2*self.margin)),
            int(np.floor(box.bb_left - self.margin)): int(np.ceil(box.bb_left + box.bb_width + 2*self.margin))
        ]
        return cv2.resize(image, dsize=(self.input_h, self.input_w), interpolation=cv2.INTER_CUBIC)

    def __getitem__(self, idx):

        sample = {
            "video_name": self.frames[idx]["video_name"],
            "frame_number": self.frames[idx]["frame_number"]
        }

        boxes = self.frames[idx]["detections"]
        sample["boxes"] = [np.array(list(box)[1:]) for box in boxes]

        video_name = self.frames[idx]['video_name']
        frame_number = self.frames[idx]['frame_number']

        gt_boxes = self.gt_videos_dict[video_name][frame_number]['detections']
        sample["gt_boxes"] = [np.array(list(box)[1:]) for box in gt_boxes]

        img_name = self.get_image_path(video_name, frame_number)
        frame = cv2.imread(img_name)
        sample["frame"] = frame

        if self.crops:
            # crop boxes from frames and reshape to fixes size
            preprocessed_boxes = []
            for box in boxes:
                image = self.crop_and_resize_box_from_frame(box)
                preprocessed_boxes.append(image)

            sample["image_crops"] = preprocessed_boxes

        if self.velocities:
            # return left top locations
            sample["locations"] = [dataset_utils.box_center(box) for box in boxes]

        if self.occupancy_grids:
            sample["occupancy_grids"] = [self.create_occupancy_grid(box, boxes) for box in boxes]

        if self.transform:
            sample = self.transform(sample)

        return sample


class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __init__(self, crops=True, velocities=True, occupancy_grids=True):
        self.crops = crops
        self.velocities = velocities
        self.occupancy_grids = occupancy_grids
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    def __call__(self, sample):

        # sample["frame"]=torch.from_numpy(sample["frame"].transpose((2, 0, 1))).float()
        if self.crops:
            image_crops = sample['image_crops']
            image_crops = [torch.from_numpy(image_crop.transpose((2, 0, 1))).float() for image_crop in image_crops]
            sample["image_crops"] = image_crops

        if self.velocities:
            # velocities
            locations = [
                torch.from_numpy(location).float() for location in sample["locations"]
            ]
            sample["locations"] = locations

        if self.occupancy_grids:
            occupancy_grids = sample['occupancy_grids']
            occupancy_grids = [torch.from_numpy(occupancy_grid).float() for occupancy_grid in occupancy_grids]

            sample["occupancy_grids"] = occupancy_grids

        return sample


def get_loaders(
        batch_size,
        shuffle=False,
        crops=True,
        velocities=True,
        boxes=True,
        occupancy_grids=True,
        num_workers=0,
        train=True,
        test=True,
):
    if test:
        composed = transforms.Compose([ToTensor(crops=crops, velocities=velocities, occupancy_grids=occupancy_grids)])
        test_dataset = MOT2015(composed, crops=crops, velocities=velocities, occupancy_grids=occupancy_grids, boxes=boxes, test_set=True)
        test_loader = DataLoader(test_dataset, batch_size=1, shuffle=False, num_workers=1)

    else:
        test_loader = None

    if train:
        composed = transforms.Compose([ToTensor(crops=crops, velocities=velocities, occupancy_grids=occupancy_grids)])
        train_dataset = MOT2015(composed, crops=crops, velocities=velocities, occupancy_grids=occupancy_grids, boxes=boxes, test_set=False)
        train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=False, num_workers=1)
    else:
        train_loader = None

    return train_loader, test_loader
