import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import datasets.dataset_utils as dataset_utils
from IPython import embed
import matplotlib.pyplot as plt

from .Model import Model

def calculate_objects_velocities(objects, normalized=False):
    # prvi je 0,0
    objects_velocities = torch.stack([torch.stack(list(obj["velocities"])) for obj in objects])
    objects_velocities = objects_velocities.permute(1, 0, 2)

    if normalized:
        # [n_fr, obj]
        num_steps = objects_velocities.shape[0]

        # mask first enterance of object (velocity would be location)
        num_objects = len(objects)
        objects_locations = torch.stack([torch.stack(list(obj["locations"])) for obj in objects]).permute(1, 0, 2)
        mask = (torch.sum(objects_locations[:-1, :, :], dim=-1) > 0) * (
            torch.sum(objects_locations[1:, :, :], dim=-1) > 0)

        # first step velocities are always zeros
        mask = torch.cat((torch.zeros(1, num_objects), mask.float()))
        objects_velocities = objects_velocities * mask[:, :, None].repeat(1, 1, 2)

        objects_frames_numbers = [torch.Tensor(obj["frames_numbers"]) for obj in objects]

        diffs = []
        for object_frames_numbers in objects_frames_numbers:
            diff = object_frames_numbers[1:]-object_frames_numbers[:-1]

            assert len(diff) <= num_steps
            assert len(diff) != num_steps  # uvijek paddamo jedan mozda je to problem

            diff = torch.cat([torch.ones(num_steps-len(diff)), diff]) # padd
            diffs.append(diff)

        diffs = torch.stack(diffs).permute(1, 0)
        assert diffs.shape == (num_steps, len(objects))

        # repeat for x and y axis
        diffs = diffs[:, :, None].repeat([1, 1, 2])

        assert objects_velocities.shape == diffs.shape
        objects_velocities = objects_velocities/diffs.float()


    return objects_velocities

def calculate_candidates_velocities(candidates_locations, objects, boxes, normalized=False):
    num_objects = len(objects)
    num_candidates = len(candidates_locations)

    candidates_locations_tiled = torch.stack(candidates_locations).repeat([num_objects, 1])
    if num_objects != 1: assert torch.var(torch.mean(candidates_locations_tiled[::num_candidates], dim=-1)) <= 1e-5  # 1,2,3,1,2,3,1,2,3

    objects_last_locations = torch.stack([obj["locations"][-1] for obj in objects], dim=0)  # (obj, 2)
    objects_last_locations_tiled = objects_last_locations.repeat([1, num_candidates]).view(num_objects*num_candidates, 2)

    if num_candidates != 1: assert torch.var(torch.mean(objects_last_locations_tiled[:num_candidates], dim=-1)) <= 1e-5  # 1,1,1,2,2,2,3,3,3

    candidates_velocities = candidates_locations_tiled - objects_last_locations_tiled

    if normalized:
        assert torch.all(torch.eq(
            torch.stack([torch.tensor(dataset_utils.box_center(b)) for b in boxes]).float(),
            torch.stack(candidates_locations).float()
        ))

        candidates_frame_numbers = [b.frame_number for b in boxes]
        candidates_frame_numbers_tiled = torch.tensor(candidates_frame_numbers).view(-1, 1).repeat([num_objects, 1])
        if num_objects != 1: assert torch.var(torch.mean(candidates_frame_numbers_tiled[::num_candidates], dim=-1)) <= 1e-5  # 1,2,3,1,2,3,1,2,3

        objects_last_frame_numbers = torch.tensor([obj["frames_numbers"][-1] for obj in objects]).view(-1, 1)  # (obj, 1)
        objects_last_frame_numbers_tiled = objects_last_frame_numbers.repeat([1, num_candidates]).view(num_objects*num_candidates, 1)
        if num_candidates != 1: assert torch.var(torch.mean(objects_last_locations_tiled[:num_candidates], dim=-1)) <= 1e-5  # 1,1,1,2,2,2,3,3,3

        candidates_diffs = candidates_frame_numbers_tiled.float() - objects_last_frame_numbers_tiled.float()
        candidates_diffs = candidates_diffs.repeat([1, 2])
        assert candidates_diffs.shape == candidates_velocities.shape

        candidates_velocities /= candidates_diffs.float()

    return candidates_velocities

class MotionModel(Model):
    def __init__(self, k=3, H=3, hidden=False, average_velocities=False, normalized_velocities=False, device=None):
        super(MotionModel, self).__init__()

        self.device = device if device else torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        print("MotionModel device:", self.device)

        self.k = k
        self.H = H
        self.hidden = hidden

        self.candiate_fc = nn.Linear(2, self.H)

        # RNN
        self.lstm = nn.LSTM(2, self.H, batch_first=False)

        # FC
        self.fc = nn.Linear(2*self.H, self.k)
        self.bn = nn.BatchNorm1d(self.k)
        self.relu = nn.ReLU()

        self.average_velocities = average_velocities
        self.normalized_velocities = normalized_velocities

        # hidden FC
        if self.hidden:
            self.hidden_fc = nn.Linear(self.k, self.k, bias=True)
            self.hidden_relu = nn.ReLU(inplace=True)
            self.hidden_fc_layer = lambda x: self.hidden_relu(self.hidden_fc(x))

        self.last_fc = nn.Linear(self.k, 1)
        self.to(self.device)

    def forward(self, sample, tracking=False):
        if not tracking:
            sequence_velocities, candidate_velocity, candidate_location, labels = (
                sample['sequence_velocities'],
                sample['candidate_velocity'],
                sample['candidate_location'],
                sample['label']
            )

            batch_size = candidate_velocity.shape[0]
            seq_len = len(sequence_velocities)

            # assert len(sequence_velocities) == len(sample["sequence_locations"]) == sample["sequence_frame_numbers"].shape[1]

            # for b in range(batch_size):
            #     ls = [l[b] for l in sample["sequence_locations"]]
            #     assert all(sample["candidate_location"][b]-ls[-1] == sample["candidate_velocity"][b])

            if self.average_velocities:
                sequence_velocities, candidate_velocity = MotionModel.calcualte_averaged_velocities(sample, batch_size)

            elif self.normalized_velocities:
                sequence_velocities, candidate_velocity = MotionModel.calcualte_normalized_velocities(sample, batch_size)

            else:
                sequence_velocities = torch.stack(sequence_velocities)

            # if False:
            #     assert False
            #     sequence_locations = sample["sequence_locations"]
            #     assert len(sequence_velocities) == len(sequence_locations)-1
            #
            #     sequence_velocities = sequence_locations[1:]
            #     candidate_velocity = sample["candidate_location"]

            # assert sequence_velocities.shape == (seq_len, batch_size, 2)

            # Forward propagate LSTM
            # (seq_length, batch_size, hidden_size)
            object_trajectory_features_sequence, _ = self.lstm(sequence_velocities.to(self.device))
            object_trajectory_features = object_trajectory_features_sequence[-1, :, :]  # last state

            candidate_velocity_features = self.candiate_fc(candidate_velocity.to(self.device))

            comparison_features = torch.cat((object_trajectory_features, candidate_velocity_features), dim=-1)

            final_features = self.bn(self.relu(self.fc(comparison_features)))

            if self.hidden:
                final_features = self.hidden_fc_layer(final_features)

            logits = self.last_fc(final_features).view(-1)

            # logs = np.array(logits > 0.)
            # labs = np.array(labels)
            # display_objects_pos = [i for  i,(lo,la) in enumerate(zip(logs,labs)) if lo == la][:2]
            # display_objects_neg = [i for  i,(lo,la) in enumerate(zip(logs,labs)) if lo != la][:5]
            #
            # display_objects = display_objects_pos + display_objects_neg
            #
            # if len(display_objects_neg) == 0:
            #     return logits
            #
            # print('logs:', logs[display_objects])
            # print('labs:', labs[display_objects])
            #
            # f, axarr = plt.subplots(len(display_objects), 7, figsize=(50, 50))
            #
            # for y,o in enumerate(display_objects):
            #     for i, crop in enumerate(sample["image_crops"]):
            #         print("vel of obj "+ str(i) + " :", sequence_velocities[i][o])
            #         crop = np.array(crop[o]).swapaxes(0, -1).swapaxes(0, 1)
            #         axarr[y, i].imshow(crop/255)
            #         axarr[y, i].set_title('obj_t'+str(i))
            #     crop = np.array(sample["candidate_crop"][o]).swapaxes(0, -1).swapaxes(0, 1)
            #     print("vel of candidate ", o, " :", candidate_velocity[o])
            #     axarr[y, -1].imshow(crop/255)
            #     axarr[y, -1].set_title('cand')
            #
            # plt.show(block=True)
            # plt.savefig("motion")
            # input("dumepd")

            return logits, final_features

        else:
            objects, candidates_locations = sample['objects'], sample['candidates_locations']

            if self.average_velocities:
                raise NotImplementedError("Averaged velocities trakcing not implemented")

            num_objects = len(objects)

            objects_ids, objects = zip(*objects.items())
            objects_ids = np.array(objects_ids)

            # prvi je 0,0
            objects_velocities = torch.stack([torch.stack(list(obj["velocities"])) for obj in objects])
            objects_velocities = objects_velocities.permute(1, 0, 2)


            if self.normalized_velocities:
                # [n_fr, obj]
                num_steps = objects_velocities.shape[0]

                objects_locations = torch.stack(
                    [torch.stack(list(obj["locations"])) for obj in objects]).permute(1, 0, 2)


                # mask first enterance of object (velocity would be location)
                mask = (torch.sum(objects_locations[:-1, :, :], dim=-1) > 0) * (torch.sum(objects_locations[1:, :, :], dim=-1) > 0)

                assert len(objects_locations) == 6

                if len(objects_locations) == 1:
                    from IPython import embed
                    embed()

                mask = torch.cat((torch.zeros(1, num_objects), mask.float()))
                objects_velocities = objects_velocities * mask[:, :, None].repeat(1, 1, 2)
                # first step velocities are always zeros

                objects_frames_numbers = [torch.Tensor(obj["frames_numbers"]) for obj in objects]

                diffs = []
                for object_frames_numbers in objects_frames_numbers:
                    diff = object_frames_numbers[1:]-object_frames_numbers[:-1]

                    assert len(diff) <= num_steps
                    assert len(diff) != num_steps  # uvijek paddamo jedan mozda je to problem

                    diff = torch.cat([torch.ones(num_steps-len(diff)), diff]) # padd
                    diffs.append(diff)

                diffs = torch.stack(diffs).permute(1, 0)
                assert diffs.shape == (num_steps, num_objects)

                # repeat for x and y axis
                diffs = diffs[:, :, None].repeat([1, 1, 2])

                assert objects_velocities.shape == diffs.shape
                objects_velocities = objects_velocities/diffs.float()

            assert objects_velocities.shape == (len(objects[0]['velocities']), num_objects, 2)


            # # bla
            # vs = calculate_objects_velocities(objects, normalized=self.normalized_velocities)


            # (seq, batch_size, feature)
            objects_trajectory_features_sequence, _ = self.lstm(objects_velocities.to(self.device))
            objects_trajectory_features = objects_trajectory_features_sequence[-1, :, :]  # last state

            num_candidates = len(candidates_locations)

            candidates_locations_tiled = torch.stack(candidates_locations).repeat([num_objects, 1])

            if num_objects != 1:
                assert torch.var(torch.mean(candidates_locations_tiled[::num_candidates], dim=-1)) <= 1e-5  # 1,2,3,1,2,3,1,2,3

            objects_last_locations = torch.stack([obj["locations"][-1] for obj in objects], dim=0)  # (obj, 2)
            objects_last_locations_tiled = objects_last_locations.repeat([1, num_candidates]).view(num_objects*num_candidates, 2)

            if num_candidates != 1:
                assert torch.var(torch.mean(objects_last_locations_tiled[:num_candidates], dim=-1)) <= 1e-5  # 1,1,1,2,2,2,3,3,3

            candidates_velocities = candidates_locations_tiled - objects_last_locations_tiled


            if self.normalized_velocities:
                    assert torch.all(torch.eq(
                        torch.stack([torch.tensor(dataset_utils.box_center(b)) for b in sample['candidates_boxes']]).float(),
                        torch.stack(candidates_locations).float()
                    ))

                    candidates_frame_numbers = [b.frame_number for b in sample["candidates_boxes"]]
                    candidates_frame_numbers_tiled = torch.tensor(candidates_frame_numbers).view(-1,1).repeat([num_objects, 1])
                    if num_objects != 1: assert torch.var(torch.mean(candidates_frame_numbers_tiled[::num_candidates], dim=-1)) <= 1e-5  # 1,2,3,1,2,3,1,2,3

                    objects_last_frame_numbers = torch.tensor([obj["frames_numbers"][-1] for obj in objects]).view(-1, 1)  # (obj, 1)
                    objects_last_frame_numbers_tiled = objects_last_frame_numbers.repeat([1, num_candidates]).view(num_objects*num_candidates, 1)
                    if num_candidates != 1: assert torch.var(torch.mean(objects_last_locations_tiled[:num_candidates], dim=-1)) <= 1e-5  # 1,1,1,2,2,2,3,3,3

                    candidates_diffs = candidates_frame_numbers_tiled.float() - objects_last_frame_numbers_tiled.float()
                    candidates_diffs = candidates_diffs.repeat([1, 2])
                    assert candidates_diffs.shape == candidates_velocities.shape

                    candidates_velocities /= candidates_diffs.float()

            candidates_velocities_features = self.candiate_fc(candidates_velocities.to(self.device))

            candidates_ids_tiled = torch.tensor(sample["candidates_ids"]).repeat([num_objects, 1]).reshape(num_objects*num_candidates, 1)
            if num_objects != 1:
                assert torch.var(torch.mean(candidates_locations_tiled[::num_candidates], dim=-1)) <= 1e-5  # 1,2,3,1,2,3

            objects_trajectory_features_tiled = objects_trajectory_features.repeat([1, num_candidates]).view(num_objects*num_candidates, self.H)

            if num_candidates != 1:
                assert torch.var(torch.mean(objects_trajectory_features_tiled[:num_candidates], dim=-1)) <= 1e-5  # 1,1,2,2

            comparison_features = torch.cat((
                objects_trajectory_features_tiled,
                candidates_velocities_features
            ), dim=-1)

            # if not len(set([c.__repr__() for c in comparison_features])) == comparison_features.shape[0]:
            #     from IPython import embed
            #     embed()

            objects_ids = objects_ids.repeat([num_candidates]).reshape(num_objects*num_candidates, 1)

            if num_candidates != 1:
                assert np.var(np.mean(objects_ids[:num_candidates], axis=-1)) <= 1e-5  # 1,1,2,2,3,3

            edges = np.concatenate((objects_ids, candidates_ids_tiled), axis=1)

            assert len(set([e.__repr__() for e in edges])) == edges.shape[0]

            final_features = self.bn(self.relu(self.fc(comparison_features)))

            if self.hidden:
                final_features = self.hidden_fc_layer(final_features)

            logits = self.last_fc(final_features).view(-1)

            return edges, logits, final_features


    @staticmethod
    def calcualte_averaged_velocities(sample, batch_size):
        # averaged_velocities
        # (seq_len, batch, input_size)
        sequence_locations = torch.stack(sample["sequence_locations"]+[sample["candidate_location"]])
        sequence_frame_numbers = torch.cat((
            sample["sequence_frame_numbers"],
            sample["candidate_frame_number"].view(batch_size, 1)
        ), dim=1).permute(1, 0)

        assert len(sequence_locations) == len(sequence_frame_numbers)

        s1, s2 = map(list, np.tril_indices(len(sequence_locations), -1))

        # steps where locations is not 0,0 is was not padded in
        valid_steps = torch.prod(sequence_locations[s1], dim=2) > 0
        vels = sequence_locations[s2] - sequence_locations[s1]
        diffs = sequence_frame_numbers[s2] - sequence_frame_numbers[s1]

        valid_steps_split = torch.split(valid_steps, list(range(1, 8)), dim=0)
        split_vels = torch.split(vels, list(range(1, 8)), dim=0)
        split_diffs = torch.split(diffs, list(range(1, 8)), dim=0)

        assert len(split_vels) == len(split_diffs) == len(valid_steps_split)

        mean_vels_list = []
        for step_vel, step_dif, valid_st in zip(split_vels, split_diffs, valid_steps_split):
            valid_mask = valid_st.unsqueeze(-1).repeat([1, 1, 2]).float()

            # normalize by frames passed
            normd_vels = step_vel / step_dif.unsqueeze(-1).repeat([1, 1, 2])

            # mask out blackouted steps
            masked_vels = normd_vels*valid_mask

            # num valid, repeat for both x and y
            valid_steps_number = torch.sum(valid_st, dim=0).unsqueeze(-1).repeat([1, 2]).float()

            # where all the steps are invalid fallback to 0
            mean_vels = torch.where(
                valid_steps_number > 0,
                torch.sum(masked_vels, dim=0) / valid_steps_number,
                torch.zeros_like(valid_steps_number)
            )

            mean_vels_list.append(mean_vels)

        mean_vels_list_ = list()
        for step_vel, step_dif in zip(split_vels, split_diffs):
            normd_vels = step_vel / step_dif.unsqueeze(-1).repeat([1, 1, 2])
            mean_vels = torch.mean(normd_vels, dim=0)
            mean_vels_list_.append(mean_vels)

        # samo za blackouts == False
        assert torch.all(torch.eq(torch.stack(mean_vels_list), torch.stack(mean_vels_list_)))

        sequence_velocities = mean_vels_list[:-1]
        candidate_velocity = mean_vels_list[-1]

        sequence_velocities = torch.stack(sequence_velocities).to(self.device)
        return sequence_velocities, candidate_velocity


    @staticmethod
    def calcualte_normalized_velocities(sample, batch_size):
        # averaged_velocities
        # (seq_len, batch, input_size)
        sequence_locations = torch.stack(sample["sequence_locations"]+[sample["candidate_location"]])
        sequence_frame_numbers = torch.cat((
            sample["sequence_frame_numbers"],
            sample["candidate_frame_number"].view(batch_size, 1)
        ), dim=1).permute(1, 0)

        assert len(sequence_locations) == len(sequence_frame_numbers)

        vels = sequence_locations[1:] - sequence_locations[:-1]
        diffs = sequence_frame_numbers[1:] - sequence_frame_numbers[:-1]

        # mask first enterance of object (velocity would be location)
        mask = (torch.sum(sequence_locations[1:], dim=-1) > 0) * (torch.sum(sequence_locations[:-1], dim=-1) > 0)
        mask = mask[:, :, None].repeat([1, 1, 2]).float()
        vels = vels*mask

        # repeat for both x and y axis
        diffs_tiled = diffs[:, :, None].repeat([1, 1, 2])

        normalized_vels = vels / diffs_tiled

        sequence_velocities = normalized_vels[:-1]
        candidate_velocity = normalized_vels[-1]

        # print("classification_mode")
        # from IPython import embed
        # embed()

        return sequence_velocities, candidate_velocity
