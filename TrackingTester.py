import numpy as np
from pathlib import Path
from IPython import embed
from termcolor import colored
from itertools import product

import os
import matplotlib
import json
import csv
matplotlib.use('Agg')
from collections import deque, defaultdict

import cv2
import numpy as np
import torch
import torch.nn as nn
from scipy.optimize import linear_sum_assignment

from torchvision import models
import datasets.dataset_utils as dataset_utils
from datasets.tracking.MOT2015 import get_loaders
from datasets.dataset_utils import Box, crop_bbox_to_frame
from models.Appearance import AppearanceModel
from models.Interaction import InteractionModel
from models.Motion import MotionModel
from models.Target import TargetModel
from models.Joint import JointModel
from models.Dummy import DummyModel

rprint = lambda *args: print(colored("".join(map(str, args)), "red"))
bprint = lambda *args: print(colored("".join(map(str, args)), "blue"))


class Tester:

    def __init__(
            self,
            model,
            model_checkpoint_path = None,
            crops=True,
            velocities=True,
            occupancy_grids=True,
            boxes=False,
            num_workers=20,
            device = None,
            threshold=0.5,
            sequence_len=6,
            dump_videos=True,
            vis_save_dir=None
    ):

        # Device configuration
        self.device = device if device is not None else torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        print("Tester device:", self.device)
        self.crops = crops
        self.velocities = velocities
        self.occupancy_grids = occupancy_grids

        if vis_save_dir is None:
            self.vis_save_dir = Path(model_checkpoint_path).parent / "vis"

        else:
            self.vis_save_dir = vis_save_dir

        self.dump_videos = dump_videos

        _, self.test_loader = get_loaders(
            batch_size=1,
            num_workers=num_workers,
            crops=self.crops,
            velocities=self.velocities,
            occupancy_grids=self.occupancy_grids,
            boxes=boxes,
            test=True,
            train=False
        )

        self.dataset = self.test_loader.dataset
        self.boxes = boxes
        self.model = model.to(self.device)
        self.model.eval()

        if type(self.model) == DummyModel:
            # Dummy
            self.model.set_dataset(self.dataset)

        # Loss and optimizer
        self.criterion = nn.CrossEntropyLoss(size_average=True).to(self.device)

        if model_checkpoint_path is not None:
            self.start_epoch = self.model.load_state(
                model_checkpoint_path,
                resume_train=False
            )
        self.threshold = threshold
        self.sequence_len = sequence_len

        self.sigmoid = torch.nn.Sigmoid()
        self.current_video_name = None
        self.exit_threshold = 0.95

    def pad_crops(self, crops):
        assert len(crops) <= self.sequence_len
        return [torch.zeros_like(crops[0])] * (self.sequence_len - len(crops)) + crops

    def pad_locations(self, locations):
        assert len(locations) <= self.sequence_len
        return [torch.zeros_like(locations[0])] * (self.sequence_len - len(locations)) + locations

    def pad_velocities(self, velocities):
        assert len(velocities) <= self.sequence_len
        velocities =  [torch.zeros_like(velocities[0])] * (self.sequence_len - len(velocities)) + velocities
        assert len(velocities) <= self.sequence_len
        return velocities

    def pad_frame_numbers(self, frame_numbers):
        assert len(frame_numbers) <= self.sequence_len
        padd_size = self.sequence_len - len(frame_numbers)
        # padd so that diff is 1
        frame_numbers = list(range(frame_numbers[0]-padd_size, frame_numbers[0])) + list(frame_numbers)

        assert len(frame_numbers) == self.sequence_len

        return frame_numbers

    def create_occupancy_grids(self, obj):
        frames_numbers = obj["frames_numbers"]
        video_dict = self.dataset.det_videos_dict[self.current_video_name]
        frames_object_bboxes = obj["bboxes"]
        frames_neighbours_boxes = [video_dict[frame_number]['detections'] if frame_number in video_dict else [] for frame_number in frames_numbers]

        occupancy_grids = [
            torch.from_numpy(
                self.dataset.create_occupancy_grid(
                    Box(self.current_video_name, frame_num, obj["id"], *frame_object_box, 1.0),
                    frame_neighbours_boxes
                )
            ).float() for frame_num, frame_object_box, frame_neighbours_boxes
            in zip(
                frames_numbers, frames_object_bboxes, frames_neighbours_boxes)
        ]

        return occupancy_grids

    def pad_occupancy_grids(self, occupancy_grids):
        assert len(occupancy_grids) <= self.sequence_len
        return [torch.zeros_like(occupancy_grids[0])] * (self.sequence_len - len(occupancy_grids)) + occupancy_grids

    def create_padded_crops(self, crop):
        return deque([torch.zeros_like(crop)] * (self.sequence_len - 1) + [crop], maxlen=self.sequence_len)

    def create_padded_grids(self, grid):
        return deque([torch.zeros_like(grid)] * (self.sequence_len - 1) + [grid], maxlen=self.sequence_len)

    def create_padded_locations_and_velocities(self, location):
        return deque([torch.tensor([0.0, 0.0])] * (self.sequence_len -1 ) + [location], maxlen=self.sequence_len),\
               deque([torch.tensor([0.0, 0.0])] * self.sequence_len, maxlen=self.sequence_len)

    def dump_bbox(self, bbox, id, frame, video_name):
        # < frame >, < id >, < bb_left >, < bb_top >, < bb_width >, < bb_height >, < conf >, < x >, < y >, < z >
        with open("./results/AppearanceModel/"+video_name+".txt", "a+") as file:
            file.write(
                ("{},"*9+"{}").format(
                    int(frame),
                    int(id),
                    float(bbox[0]),
                    float(bbox[1]),
                    float(bbox[2]),
                    float(bbox[3]),
                    float(0.99),
                    1, 1, 1)+"\n")

    def dump_box(self, box, id, video_name):
        # < frame >, < id >, < bb_left >, < bb_top >, < bb_width >, < bb_height >, < conf >, < x >, < y >, < z >
        with open("./results/AppearanceModel/"+video_name+".txt", "a+") as file:
            file.write(
                ("{},"*9+"{}").format(
                    int(box[0]),
                    int(id),
                    float(box[2]),
                    float(box[3]),
                    float(box[4]),
                    float(box[5]),
                    float(box[6]),
                    1, 1, 1)+"\n")

    def get_crop(self, bbox, video_name, frame_number, as_torch=True):
        img_path = os.path.join(os.path.join(
            self.dataset.dataset_path,'2DMOT2015/train', video_name,'img1/',
            str(int(frame_number)).zfill(6) + ".jpg")
        )

        image = cv2.imread(img_path)
        f_w, f_h, _ = image.shape

        bbox = crop_bbox_to_frame(bbox, f_w, f_h)

        assert image is not None

        image = image[
                int(np.floor(bbox[1])): int(np.ceil(bbox[1] + bbox[3])),
                int(np.floor(bbox[0])): int(np.ceil(bbox[0] + bbox[2]))
                ]

        assert self.dataset.input_h > 0
        assert self.dataset.input_w > 0
        assert image.size >= 0

        crop = cv2.resize(image, dsize=(self.dataset.input_h, self.dataset.input_w), interpolation=cv2.INTER_LINEAR)

        if as_torch:
            crop = torch.from_numpy(crop).float().permute(2, 0, 1).unsqueeze(0)

        return crop

    def check_predictions_detections_overlap(self, obj, current_bbox, current_frame_number, threshold=0.8, frame=None):
        # threshold = opt.overlap_box = 0.5
        # report kaze 0.8
        assert self.dataset.det_videos_dict is not None
        assert type(current_frame_number) == int

        frames_dict = self.dataset.det_videos_dict[self.current_video_name]
        max_overlaps = []

        for bbox, frame_number in zip(list(obj["bboxes"])+[current_bbox], list(obj["frames_numbers"])+[current_frame_number]):
            assert type(frame_number) == int

            if frame_number in frames_dict:
                detections = [
                    dataset_utils.box_to_bbox(box) for box in frames_dict[frame_number]["detections"]
                ]
                max_o = max([dataset_utils.bb_intersection_over_union(det, bbox) for det in detections])

            else:
                max_o = 0.0

            max_overlaps.append(max_o)

        mean_overlap = sum(max_overlaps)/len(max_overlaps)

        return  mean_overlap > threshold, mean_overlap

    # def bb_intersection_over_union(self, boxA, boxB):
    #     # determine the (x, y)-coordinates of the intersection rectangle
    #     xA = max(boxA[0], boxB[0])
    #     yA = max(boxA[1], boxB[1])
    #     xB = min(boxA[0] + boxA[2], boxB[0] + boxB[2])
    #     yB = min(boxA[1] + boxA[3], boxB[1] + boxB[3])
    #
    #     # compute the area of intersection rectangle
    #     interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    #
    #     # compute the area of both the prediction and ground-truth
    #     # rectangles
    #     boxAArea = (boxA[0] + boxA[2] - boxA[0] + 1) * (boxA[1]+boxA[3] - boxA[1] + 1)
    #     boxBArea = (boxB[0] + boxB[2] - boxB[0] + 1) * (boxB[1]+boxB[3] - boxB[1] + 1)
    #
    #     # compute the intersection over union by taking the intersection
    #     # area and dividing it by the sum of prediction + ground-truth
    #     # areas - the interesection area
    #     iou = interArea / float(boxAArea + boxBArea - interArea)
    #
    #     # return the intersection over union value
    #     assert iou >= 0
    #     assert iou <= 1.0
    #     return iou
    #
    # def bb_intersection_over_area(self, boxA, boxB):
    #     # retuns the percentage of the boxA being occluded by boxB
    #
    #     xA = max(boxA[0], boxB[0])
    #     yA = max(boxA[1], boxB[1])
    #     xB = min(boxA[0] + boxA[2], boxB[0] + boxB[2])
    #     yB = min(boxA[1] + boxA[3], boxB[1] + boxB[3])
    #
    #     interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    #     boxAArea = (boxA[0] + boxA[2] - boxA[0] + 1) * (boxA[1]+boxA[3] - boxA[1] + 1)
    #
    #     ioa = interArea / float(boxAArea)
    #
    #     # return the intersection over area value
    #     assert ioa >= 0
    #     assert ioa <= 1.0
    #     return ioa


    def get_unsuppressed_candidates(
        self,
        candidate_boxes,
        candidate_image_crops,
        candidate_occupancy_grids,
        candidate_locations,
        iou_detection_thresh=0.5,
        confidence_thresh=0.5
    ):
        unsupressed_indices = self.get_unsuppresed_indices(candidate_boxes, iou_detection_thresh, confidence_thresh)

        candidate_boxes = [ candidate_boxes[i] for i in unsupressed_indices] if candidate_boxes else []
        candidate_image_crops = [ candidate_image_crops[i] for i in unsupressed_indices] if candidate_image_crops else []
        candidate_occupancy_grids = [ candidate_occupancy_grids[i] for i in unsupressed_indices] if candidate_occupancy_grids else []
        candidate_locations = [ candidate_locations[i] for i in unsupressed_indices] if candidate_locations else []

        return candidate_boxes, candidate_image_crops, candidate_occupancy_grids, candidate_locations


    def get_unsuppresed_indices(self, candidate_boxes, iou_detection_thresh=0.5, confidence_thresh=0.5):
        # suppress detections
        tracked_bboxes = [to['bboxes'][-1] for to in self.tracked_objects.values()]
        candidate_bboxes = [dataset_utils.box_to_bbox(box) for box in candidate_boxes]

        unsupressed_indices_iou = [
            candidate_index for candidate_index, candidate_bbox in enumerate(candidate_bboxes)
            if max(
                [0.] + [dataset_utils.bb_intersection_over_union(candidate_bbox, tr_bbox) for tr_bbox in tracked_bboxes]
            ) < iou_detection_thresh
        ]


        # percentage of candidate_box being covered by tracked boxes
        # occluded is in the background    (max of int_over_area)
        # covered can be in the foreground (sum of int_over_area)

        # they use sum here
        unsupressed_indices_ioa = [
            candidate_index for candidate_index, candidate_bbox in enumerate(candidate_bboxes)
            if sum(
                [dataset_utils.bb_intersection_over_area(candidate_bbox, tr_bbox) for tr_bbox in tracked_bboxes]
            ) < iou_detection_thresh
        ]

        low_confidence_indices = [
            i for (i, box) in enumerate(candidate_boxes) if box.conf < confidence_thresh
        ]

        unsupressed_indices = set(unsupressed_indices_ioa) & set(unsupressed_indices_iou)
        unsupressed_indices = list(set(unsupressed_indices) - set(low_confidence_indices))

        return unsupressed_indices


    def prepare_lost_objects(self, lost_objects):
        for lost_id, lost_obj in lost_objects.items():
            bboxes = lost_obj["bboxes"]
            frames_numbers = lost_obj["frames_numbers"]

            # if not len(bboxes) == len(frames_numbers):
            #     from IPython import embed
            #     embed()

            crops = [
                self.get_crop(
                    bbox, self.current_video_name, frame_number
                ) for bbox, frame_number in zip(bboxes, list(frames_numbers)[-len(bboxes):])
            ]

            lost_obj["crops"] = self.pad_crops(crops)

            locations = [
                torch.tensor(dataset_utils.bbox_center(bbox)).float()
                for bbox, frame_number in zip(bboxes, frames_numbers)
            ]
            if self.occupancy_grids:
                occupancy_grids = self.create_occupancy_grids(lost_obj)

            velocities = [locations[0]] + [locations[i]-locations[i-1] for i in range(1, len(locations))]
            # velocities = velocities if len(locations) > 1 else [locations[0]]

            lost_obj["locations"] = self.pad_locations(locations)
            lost_obj["velocities"] = self.pad_velocities(velocities)
            lost_obj["frames_numbers"] = self.pad_frame_numbers(frames_numbers[-self.sequence_len:])

            if self.occupancy_grids:
                lost_obj["occupancy_grids"] = self.pad_occupancy_grids(occupancy_grids)

        return lost_objects


    def add_new_objects_to_tracked(self, candidate_boxes, frame, disable_a3):
        for candidate_box in candidate_boxes:
            # new_id = max_key+1+i
            bbox = dataset_utils.box_to_bbox(candidate_box)

            new_id = candidate_box.obj_id
            self.tracked_objects[new_id] = {"id": new_id}

            assert self.current_frame_number == candidate_box.frame_number
            self.tracked_objects[new_id]["bboxes"] = deque([bbox], maxlen=self.sequence_len)
            # self.tracked_objects[new_id]["frames_numbers"] = deque([self.current_frame_number], maxlen=self.sequence_len)
            self.tracked_objects[new_id]["frames_numbers"] = [self.current_frame_number]

            if not disable_a3:
                # tracker = cv2.TrackerKCF_create()
                tracker = cv2.TrackerTLD_create()
                # tracker = cv2.TrackerMIL_create()
                # tracker = cv2.TrackerKLT_create()
                # tracker = cv2.TrackerCSRT_create()

                bbox = fit_bbox_to_frame(bbox, frame)
                tracker.init(frame, tuple(bbox))
                self.tracked_objects[new_id]["tracker"] = tracker

        assert len(set([c.frame_number for c in candidate_boxes])) in [0, 1]

        # if len(candidate_boxes) > 0:
        #     video_name = candidate_boxes[0].video_name
        #     frame_num = candidate_boxes[0].frame_number
        #
        #     assert self.current_frame_number == candidate_boxes[0].frame_number
        #
        #     detections = self.dataset.gt_videos_dict[video_name][frame_num]["detections"]
        #     gt_detections = [DummyModel.box_to_bbox(b) for b in detections]
        #     for cand in candidate_boxes:
        #         cand_bbox = DummyModel.box_to_bbox(cand)
        #
        #         max_o = np.max([self.bb_intersection_over_union(det, cand_bbox) for det in gt_detections])
        #         assert max_o > 0.5

    def resolve_tracker_conflicts(self, overlap_sup=0.7):
        suppress_ids = []
        for to_id, to in self.tracked_objects.items():
            to_bb = to["bboxes"][-1]

            # overlaps with all the other not suppressed trackers [(id, o), (id, o), ...]
            overlaps = [
                (o_id, dataset_utils.bb_intersection_over_union(o['bboxes'][-1], to_bb))
                for o_id, o in self.tracked_objects.items() if o_id != to_id and o_id not in suppress_ids
            ]

            # overlaps bigger than threshold
            overlaps = [(o_id, o) for o_id, o in overlaps if o > overlap_sup]

            if len(overlaps) > 0:
                overlapping_object_id = max(overlaps, key=lambda o: o[1])[0]
                overlapping_object = self.tracked_objects[overlapping_object_id]

                to_streak_tracked = len(to['bboxes'])
                overl_o_streak_tracked = len(overlapping_object['bboxes'])

                # suppress tracked object with a smaller streak
                if to_streak_tracked > overl_o_streak_tracked:
                    suppress_ids.append(overlapping_object_id)

                elif overl_o_streak_tracked > to_streak_tracked:
                    suppress_ids.append(to_id)

                else:
                    # if the streaks are the same suppress the one with smaller overlap with detections
                    to_over = self.overlap_with_detections(to['bboxes'][-1], to['frames_numbers'][-1])
                    overl_o_over = self.overlap_with_detections(
                        overlapping_object['bboxes'][-1], overlapping_object['frames_numbers'][-1])

                    if to_over > overl_o_over:
                        suppress_ids.append(overlapping_object_id)
                    else:
                        suppress_ids.append(to_id)

        for suppress_id in set(suppress_ids):
            # rprint("suppressing id: ", suppress_id)
            del self.tracked_objects[suppress_id]




    def overlap_with_detections(self, bbox, frame_number):
        frames_dict = self.dataset.det_videos_dict[self.current_video_name]
        detections = [
            (
                box.bb_left,
                box.bb_top,
                box.bb_width,
                box.bb_height,
            ) for box in frames_dict[frame_number]["detections"]
        ]

        return max([dataset_utils.bb_intersection_over_union(det, bbox) for det in detections])

    @staticmethod
    def get_unsuppresed_edges_weights(edges, weights, lo_id_bboxes, c_id_bboxes, threshold_ratio=0.6, threshold_dis=3.0):

        def get_suppressed_edges_by_ratio(lo_id_bboxes, c_id_bboxes, threshold_ratio = 0.6):
            suppressed_edges = []
            for (lo_id, lo_bbox), (c_id, c_bbox) in product(lo_id_bboxes, c_id_bboxes):
                r = lo_bbox[-1] / c_bbox[-1]
                r = min(r, 1 / r)

                if r < threshold_ratio:
                    suppressed_edges.append((lo_id, c_id))

            return suppressed_edges

        def get_suppressed_edges_by_distance(lo_id_bboxes, c_id_bboxes, threshold_dis = 3.0):
            suppressed_edges = []

            def calc_centers(bboxes): return np.vstack([bboxes[:, 0] + bboxes[:, 2] / 2, bboxes[:, 1] + bboxes[:, 3] / 2]).T

            lo_ids = np.array([lo[0] for lo in lo_id_bboxes])
            c_ids = np.array([c[0] for c in c_id_bboxes])

            lo_bboxes = np.array([lo[1] for lo in lo_id_bboxes])
            c_bboxes = np.array([c[1] for c in c_id_bboxes])

            lo_centers = calc_centers(lo_bboxes)
            c_centers = calc_centers(c_bboxes)

            lo_ws = lo_bboxes[:, 2]

            for (lo_id, lo_center, lo_w), (c_id, c_center) in product(zip(lo_ids, lo_centers, lo_ws), zip(c_ids, c_centers)):
                d = np.linalg.norm(lo_center - c_center) / lo_w
                if d < threshold_dis:
                    suppressed_edges.append((lo_id, c_id))

            return suppressed_edges


        # supp_edges_d = get_suppressed_edges_by_distance(lo_id_bboxes, c_id_bboxes, threshold_dis)
        supp_edges = get_suppressed_edges_by_ratio(lo_id_bboxes, c_id_bboxes, threshold_ratio)
        # supp_edges = set(supp_edges_d + supp_edges_r)

        if len(supp_edges) > 0:
            unsupp_indices = [i for i, e in enumerate(edges) if tuple(e) not in supp_edges]
            # unsup_edges, unsup_ws = zip(*[(e, w) for e, w in zip(edges, weights) if tuple(e) not in supp_edges])

            weights = np.array([w if tuple(e) not in supp_edges else 0.0 for e, w in zip(edges, weights)])

        return edges, weights


    def remove_false_positive_detections(
        self,
        candidate_boxes,
        candidate_image_crops,
        candidate_occupancy_grids,
        candidate_locations,
    ):

        video_name = candidate_boxes[0].video_name
        frame_number = candidate_boxes[0].frame_number

        tp_dets = []

        for i, cand_box in enumerate(candidate_boxes):
            cand_bbox = dataset_utils.box_to_bbox(cand_box)
            detections = self.dataset.gt_videos_dict[video_name][frame_number]["detections"]
            gt_detections = [dataset_utils.box_to_bbox(b) for b in detections]

            max_o = np.max([dataset_utils.bb_intersection_over_union(det, cand_bbox) for det in gt_detections])
            if max_o > 0.5:
                tp_dets.append(i)

        candidate_boxes = [candidate_boxes[i] for i in tp_dets]
        candidate_image_crops = [candidate_image_crops[i] for i in tp_dets]
        if self.occupancy_grids:
            candidate_occupancy_grids = [candidate_occupancy_grids[i] for i in tp_dets]
        candidate_locations = [candidate_locations[i] for i in tp_dets]

        return candidate_boxes, candidate_image_crops, candidate_occupancy_grids, candidate_locations

    def tracked_check(self):
        return
        for id, obj in self.tracked_objects.items():
            if len(obj['frames_numbers']) == 1:

                frame_num = int(obj['frames_numbers'][-1])
                detections = self.dataset.gt_videos_dict[self.current_video_name][frame_num]["detections"]
                gt_detections = [dataset_utils.box_to_bbox(b) for b in detections]

                obj_bbox = obj['bboxes'][-1]
                max_o = np.max([0]+[dataset_utils.bb_intersection_over_union(det, obj_bbox) for det in gt_detections])

                assert max_o > 0.5

    def remove_left_objs(self, frame):
        left_ids = []
        for id, to in self.tracked_objects.items():
            if any(to['bboxes'][-1][:2] < [0., 0.]) or any(to['bboxes'][-1][:2] > frame.shape[:2]):
                left_ids.append(id)

        # rprint("left :", left_ids)
        for id in left_ids:
            del self.tracked_objects[id]

    def test(self, disable_a3=False):
        self.current_video_name = None
        self.current_frame_number = 0
        self.max_key = 1
        lost_objects = {}
        self.tracked_objects = {}
        self.t_lost = 10
        lost_objects_counter = {}
        history = defaultdict(lambda : defaultdict(lambda: defaultdict(np.array))) # history[video_name][frame_num][id]=bbox
        frames_history = []  # only current video

        for iter, sample in enumerate(self.test_loader):
            if iter % 50 == 0:
                print("\niter {}/{}".format(iter, len(self.test_loader)))

            candidate_image_crops = sample["image_crops"] if self.crops else []
            candidate_occupancy_grids = sample["occupancy_grids"] if self.occupancy_grids else []
            candidate_locations = [location[0] for location in sample.get("locations", [])]

            self.max_key = max(list(self.tracked_objects.keys())+list(lost_objects)+[self.max_key])

            candidate_boxes = [
                Box(
                    sample["video_name"][0],
                    np.array(box[0])[0],
                    self.max_key+i+1,
                    *np.array(box[0])[2:]
                ) for i, box in enumerate(sample.get("boxes", []))
            ]

            # if len(candidate_boxes) > 0:
            #     candidate_boxes, candidate_image_crops, candidate_occupancy_grids, candidate_locations \
            #         = self.remove_false_positive_detections(
            #         candidate_boxes,
            #         candidate_image_crops,
            #         candidate_occupancy_grids,
            #         candidate_locations,
            #     )

            frame = np.array(sample["frame"])[0]

            if self.current_video_name != sample["video_name"][0]:

                if self.current_video_name is not None:
                    mota = visualize_video(
                        history[self.current_video_name],
                        frames_history,
                        self.current_video_name,
                        self.dump_videos,
                        self.vis_save_dir,
                        verbose=False
                    )

                frames_history = []
                frames_history.append(frame)

                self.current_video_name = sample["video_name"][0]
                self.current_frame_number = int(sample["frame_number"])

                if len(candidate_boxes) > 0:
                    assert len(set([b.frame_number for b in candidate_boxes])) == 1
                    assert self.current_frame_number == int(candidate_boxes[0].frame_number)

                assert all([b.conf > 0.5 for b in candidate_boxes])

                self.tracked_objects = defaultdict(dict)
                self.max_key = 1
                lost_objects = {}
                lost_objects_counter = {}

                for i, box in enumerate(candidate_boxes):
                    id = i + 1  # smallest id is 1
                    bbox = dataset_utils.box_to_bbox(box)
                    self.tracked_objects[id]["id"] = id
                    self.tracked_objects[id]["bboxes"] = deque([bbox], maxlen=self.sequence_len)
                    self.tracked_objects[id]["frames_numbers"] = [self.current_frame_number]

                    if not disable_a3:
                        # tracker = cv2.TrackerKCF_create()
                        tracker = cv2.TrackerTLD_create()
                        # tracker = cv2.TrackerCSRT_create()
                        # tracker = cv2.TrackerMIL_create()
                        # tracker = cv2.TrackerGOTURN_create()
                        tracker.init(frame, tuple(bbox))
                        self.tracked_objects[id]["tracker"] = tracker


            else:
                assert self.current_frame_number > 0
                assert len(set([b.frame_number for b in candidate_boxes])) in [0, 1]
                if len(candidate_boxes) > 0: assert candidate_boxes[0].frame_number == sample["frame_number"]
                assert len(set([b.frame_number for b in candidate_boxes])) in [0, 1]

                prev_fn = self.current_frame_number
                self.current_frame_number = int(sample["frame_number"])
                assert self.current_frame_number == prev_fn + 1  # assert one by one
                frames_history.append(frame)

                # get new object locations and detect lost objects
                newly_lost_objects_ids = []
                # print("updating trackers")

                for id, obj in self.tracked_objects.items():

                    if disable_a3:
                        ok = False  # disable a3
                        enough_overlap = False

                    else:
                        ok, bbox = obj["tracker"].update(frame)
                        bbox = np.array(bbox)

                        enough_overlap, mean_over = self.check_predictions_detections_overlap(
                            obj, bbox, self.current_frame_number, 0.8, frame)

                        self.tracked_check()

                    if ok and enough_overlap:
                        # remember location, update tracker
                        obj["bboxes"].append(bbox)
                        obj["frames_numbers"].append(self.current_frame_number)

                    else:
                        # rprint("ok {} ov {}".format(ok, enough_overlap))
                        # mark object as lost
                        lost_objects_counter[id] = 0
                        newly_lost_objects_ids.append(id)
                        lost_objects[id] = obj

                # remove lost object from tracked objects
                for lost_object_id in newly_lost_objects_ids:
                    del self.tracked_objects[lost_object_id]

                if len(lost_objects) > 0:
                    lost_ids = list(lost_objects_counter.keys())
                    # print("forget:", end=" ")
                    for lost_id in lost_ids:
                        lost_objects_counter[lost_id] += 1

                        if lost_objects_counter[lost_id] >= self.t_lost:
                            # print(lost_id, end=" ")
                            del lost_objects[lost_id]
                            del lost_objects_counter[lost_id]
                    # print()

                # print("lost ids:", sorted(lost_objects.keys()))
                # assert same length
                # assert len(set(map(len,[
                #     candidate_boxes, candidate_image_crops, candidate_occupancy_grids, candidate_locations
                # ]))) == 1

                assert len(set(map(len,[
                    candidate_boxes, candidate_image_crops, candidate_locations
                ]))) == 1

                # print("all: {} lost: {}".format(len(candidate_boxes), len(lost_objects)))
                # from IPython import embed
                # print("provjeri kaj je s ovim suppresanjem dole")
                # embed()
                if not disable_a3:

                    candidate_boxes, candidate_image_crops, candidate_occupancy_grids, candidate_locations\
                        = self.get_unsuppressed_candidates(
                        candidate_boxes,
                        candidate_image_crops,
                        candidate_occupancy_grids,
                        candidate_locations,
                    )

                # print("unsupressed: {} lost: {}".format(len(candidate_boxes), len(lost_objects)))

                # if len(candidate_boxes) > 0 and False:  # disable a6
                if len(candidate_boxes) > 0:
                    if len(lost_objects) > 0:
                        # bprint("Relocalization")
                        lost_objects = self.prepare_lost_objects(lost_objects)

                        num_candidates = len(candidate_boxes)

                        # keys = list(self.tracked_objects.keys())+list(lost_objects)+[max_key]
                        # max_key = max(keys)

                        # candidate_ids = np.array([int(max_key+1+i) for i, c in enumerate(candidate_boxes)])
                        # for cand_id, cand_box in zip(candidate_ids, candidate_boxes): cand_box[1]=cand_id

                        candidate_ids = np.array([c.obj_id for c in candidate_boxes])

                        assert all([i > 0 for i in candidate_ids])

                        # assert same length
                        assert len(set(map(len, [
                            # candidate_image_crops, candidate_occupancy_grids, candidate_locations, candidate_ids
                            candidate_image_crops, candidate_locations, candidate_ids
                        ]))) == 1

                        parsed_sample = {
                            "objects": lost_objects,
                            "candidates_crops": candidate_image_crops,
                            "candidates_grids": candidate_occupancy_grids,
                            "candidates_locations": candidate_locations,
                            "candidates_ids": candidate_ids,
                            "candidates_boxes": candidate_boxes
                        }
                        assert set([c.obj_id for c in candidate_boxes]) == set(candidate_ids)

                        # get network results
                        if type(self.model) == DummyModel:
                            # Dummy
                            assert False
                            # edges, logits, _ = self.model(
                            #     parsed_sample, tracking=True, video_name=self.current_video_name, frame=self.current_frame_number)
                        else:
                            # Normal
                            with torch.no_grad():
                                edges, logits, _ = self.model(parsed_sample, tracking=True)


                                # test consistency
                                if False:
                                    batch_size = min(len(parsed_sample['candidates_crops']), len(parsed_sample['objects']))

                                    candidate_crop = torch.cat(
                                        parsed_sample['candidates_crops'][:batch_size], dim=0)

                                    # candidate_occupancy_grid = torch.cat(
                                    #     parsed_sample['candidates_grids'][:batch_size], dim=0)

                                    objs = list(parsed_sample['objects'].values())[:batch_size]

                                    candidate_location = parsed_sample['candidates_locations'][:batch_size]
                                    obj_locations = [obj["locations"][-1] for obj in objs]

                                    candidate_velocity = torch.cat(
                                        [c-o for c, o in zip(candidate_location, obj_locations)]
                                    ).view(-1, 2)

                                    candidate_location = torch.stack(candidate_location)

                                    candidate_frame_number = [b.frame_number for b in parsed_sample["candidates_boxes"]]
                                    candidate_frame_number = torch.tensor(candidate_frame_number).view(batch_size, 1).float()

                                    sequence_velocities = [
                                        torch.cat([obj["velocities"][time_step].view(-1, 2) for obj in objs])
                                        for time_step in range(self.sequence_len)
                                    ]

                                    sequence_locations = [
                                        torch.cat([obj["locations"][time_step].view(-1, 2) for obj in objs])
                                        for time_step in range(self.sequence_len)
                                    ]
                                    sequence_locations = [torch.zeros_like(sequence_locations[0])] + sequence_locations

                                    sequence_frames_numbers = torch.stack([
                                        torch.tensor([obj["frames_numbers"][time_step] for obj in objs])
                                        for time_step in range(self.sequence_len)
                                    ]).float()

                                    sequence_frames_numbers = torch.cat(
                                        ((sequence_frames_numbers[0] - 1).view(1, batch_size), sequence_frames_numbers),
                                        dim=0).permute(1, 0)

                                    # sequence_occupancy_grids = [
                                    #     torch.cat([obj["occupancy_grids"][time_step].view(-1, 49) for obj in objs])
                                    #     for time_step in range(self.sequence_len)
                                    # ]

                                    image_crops = [
                                        torch.cat([obj["crops"][time_step] for obj in objs])
                                        for time_step in range(self.sequence_len)
                                    ]

                                    label = torch.ones(batch_size)


                                    clf_sample = {
                                        "candidate_crop": candidate_crop,
                                        "candidate_velocity": candidate_velocity,
                                        "candidate_location": candidate_location,
                                        "candidate_frame_number": candidate_frame_number,

                                        "image_crops": image_crops,
                                        "sequence_velocities": sequence_velocities,
                                        "sequence_locations": sequence_locations,
                                        "sequence_frame_numbers": sequence_frames_numbers,

                                        "label": label
                                    }

                                    logs, _ = self.model(clf_sample)
                                    if not all(
                                        [min([abs(torch.mean(self.sigmoid(f_)) - torch.mean(self.sigmoid(f))) for f in logits]) < 1e-5 for f_ in logs]
                                    ):
                                        print("not consistent")
                                        from IPython import embed
                                        embed()



                        weights = self.sigmoid(logits).cpu().detach().numpy()

                        edges, weights = Tester.get_unsuppresed_edges_weights(
                            edges,
                            weights,
                            [(id, lo['bboxes'][-1]) for id, lo in lost_objects.items()],
                            [(b.obj_id, dataset_utils.box_to_bbox(b)) for b in candidate_boxes]
                        )

                        # bprint("unsuppressed edges:", len(edges))

                        if len(edges) > 0:

                            # print("assosiation results")
                            # np.set_printoptions(suppress=True)
                            # print(np.concatenate((edges, weights.reshape(-1, 1)), axis=1))

                            # Hungarian
                            valid_minimum_edges = assign_edges(weights, edges)

                        else:
                            valid_minimum_edges = []

                        # bprint("valid edges:", len(valid_minimum_edges))
                        for obj_id, cand_id in valid_minimum_edges:
                            assert cand_id not in lost_objects.keys()
                            assert cand_id >= 0

                            # duplicate or no matching ids in candidate boxes test
                            assert len([b for b in candidate_boxes if int(b.obj_id) == cand_id]) == 1

                            candidate_box = [b for b in candidate_boxes if int(b.obj_id) == cand_id][0]
                            candidate_bbox = dataset_utils.box_to_bbox(candidate_box)

                            lost_objects[obj_id]['bboxes'].append(candidate_bbox)
                            lost_objects[obj_id]["frames_numbers"].append(self.current_frame_number)

                            if not disable_a3:
                                # tracker = cv2.TrackerKCF_create() # radi dobro
                                tracker = cv2.TrackerTLD_create()
                                # tracker = cv2.TrackerCSRT_create()
                                # tracker = cv2.TrackerMIL_create()

                                # tracker = cv2.TrackerGOTURN_create()
                                tracker.init(frame, tuple(candidate_bbox))
                                lost_objects[obj_id]["tracker"]=tracker

                            # print("object {} relocated".format(obj_id))


                        # move found objects back to tracked
                        for obj_id, _ in valid_minimum_edges:
                            self.tracked_objects[obj_id]=lost_objects[obj_id]
                            del lost_objects[obj_id]
                            del lost_objects_counter[obj_id]

                        # print("lost_obj_counter:", lost_objects_counter)

                        # remove assigned candidates
                        if len(valid_minimum_edges) > 0:
                            assigned_cand_ids = valid_minimum_edges[:,1]
                            candidate_boxes = [ b for b in candidate_boxes if b.obj_id not in assigned_cand_ids ]


                # unsupressed_indices = self.get_unsuppresed_indices(self.tracked_objects, candidate_boxes)
                candidate_boxes, candidate_image_crops, candidate_occupancy_grids, candidate_locations \
                    = self.get_unsuppressed_candidates(
                    candidate_boxes,
                    candidate_image_crops,
                    candidate_occupancy_grids,
                    candidate_locations
                )


                # bprint("Adding {} new objects".format(len(candidate_boxes)))
                self.add_new_objects_to_tracked(candidate_boxes, frame, disable_a3)

                self.tracked_check()

            self.resolve_tracker_conflicts()

            self.remove_left_objs(frame)

            assert lost_objects.keys() == lost_objects_counter.keys()
            assert len(set(lost_objects) & set(self.tracked_objects)) == 0
            assert all([id == o['id'] for id, o in self.tracked_objects.items()])


            # print("currently_tracked_objects:", sorted(list(self.tracked_objects.keys())))
            for id, obj in self.tracked_objects.items():
                frame_num = obj["frames_numbers"][-1]
                bbox = obj["bboxes"][-1]
                history[self.current_video_name][frame_num][id]=bbox

            self.tracked_check()

        # process last video
        mota = visualize_video(
            history[self.current_video_name],
            frames_history,
            self.current_video_name,
            self.dump_videos,
            self.vis_save_dir,
            verbose=True,
        )

        return mota




# shrinks bbox if it is outside of frame
def fit_bbox_to_frame(bbox, frame):
    x, y, w, h = bbox
    # box mora poceti unutar frame-a
    # ovo bi trebalo biti rijeseno unutar dataseta
    # todo: prebaci ovo unutar dataseta
    fh, fw, _ = frame.shape
    assert x >= 0
    assert y >= 0
    assert x+w <= fw
    assert y+h <= fh
    w = min(w, fw-x)
    h = min(h, fh-y)
    bbox = np.array([x, y, w, h])
    return bbox


def vis_box(frame, x,y,w,h):
    import matplotlib.pyplot as plt
    import matplotlib.patches as patches

    fig, ax = plt.subplots(1)
    ax.imshow(frame)
    rect = patches.Rectangle((x,y),w,h,linewidth=1,edgecolor='r',facecolor='none')
    ax.add_patch(rect)
    plt.show(block=True)


def visualize_video(
        video_history,
        frames_history,
        video_name="output",
        dump_videos=False,
        save_dir="./res",
        verbose=True
):

    if dump_videos:
        h, w = frames_history[0].shape[:2]  # h, w, 3
        video = cv2.VideoWriter(
            "{}/{}.avi".format(save_dir, video_name),
            cv2.VideoWriter_fourcc('M','J','P','G'),
            10,
            (w, h)
        )

        for i, frame in enumerate(frames_history):

            for id, bbox in video_history[i+1].items():
                x, y, w, h = map(int, np.ceil(bbox))
                x1, y1, x2, y2 = x, y, x+w, y+h
                np.random.seed(id)
                r,g,b = map(int, np.random.randint(0, 256, 3))
                cv2.rectangle(frame, (x1, y1), (x2, y2), (r, g, b), 3)
                cv2.putText(frame, str(id), (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 1, (r,g,b), 3)

            frame = cv2.merge((
                frame[:,:,0],
                frame[:,:,1],
                frame[:,:,2]
            ))
            video.write(frame)
        video.release()

        if verbose:
            print("{} video visualized".format(video_name))


    # dump result
    # <frame>, <id>, <bb_left>, <bb_top>, <bb_width>, <bb_height>, <conf>, <x>, <y>, <z>
    # dir = os.path.join("./res", video_name)

    trajectory_save_dir = save_dir / "trajectories"
    if not os.path.isdir(trajectory_save_dir):
        os.makedirs(trajectory_save_dir)

    with open(os.path.join(trajectory_save_dir, "{}.txt".format(video_name)), 'w') as file:
       writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
       for i, frame in enumerate(frames_history):
           for id, bbox in video_history[i+1].items():
               x, y, w, h = map(int, np.ceil(bbox))
               writer.writerow([str(i+1), str(id), x, y, w, h, -1, -1, -1, -1])
    try:
        if verbose:
            RESULT = os.popen("python -m motmetrics.apps.eval_motchallenge ./data/MOT/train {} 2> /dev/null | tee {}/results.txt".format(trajectory_save_dir, save_dir)).read()
            print(RESULT)
            OVERALL_MOTA = float(RESULT.splitlines()[-1].split(" ")[-2][:-1])

        else:
            # os.system("python -m motmetrics.apps.eval_motchallenge ./data/MOT/train {} 1> {}/results.txt 2".format(save_dir, save_dir))
            stdout_str = os.popen("python -m motmetrics.apps.eval_motchallenge ./data/MOT/train {} 2> /dev/null | grep OVERALL".format(trajectory_save_dir)).read()
            OVERALL_MOTA = float(stdout_str.split(" ")[-2][:-1])

    except:
        print("EXCEPT")
        from IPython import embed
        embed()

    return OVERALL_MOTA


def assign_edges(weigths, edges):

    if len(edges) == 0:
        return []

    elif len(edges) == 1:
        return edges if weigths > 0.5 else []

    num_lost = len(set(edges[:, 0]))
    num_candidates = len(set(edges[:, 1]))
    assert num_lost * num_candidates != 1

    probs = weigths
    neg_probs = 1 - weigths

    probs = probs.reshape(num_lost, num_candidates)
    neg_probs = neg_probs.reshape(num_lost, num_candidates)

    edges = edges.reshape(num_lost, num_candidates, 2)

    obj_indices, cand_indices = linear_sum_assignment(neg_probs)
    minimum_edges = edges[obj_indices, cand_indices]

    # valid_minimum_edges = minimum_edges[np.where(probs[obj_indices, cand_indices] > self.threshold)]
    # print("cand_indices:", cand_indices)
    valid_minimum_edges = minimum_edges[np.where(probs[obj_indices, cand_indices] > 0.5)]

    return valid_minimum_edges


if __name__ == "__main__":
    # target
    target = True
    if target:
        print("TARGET")
        device="cuda"
        appearance_model = AppearanceModel(
            k=500, H=500,
            siamese=True, device=device, hidden=True,
            use_bn=True, train_backbone=True,
            backbone=models.resnet18(pretrained=False)
        )
        motion_model = MotionModel(k=100, H=128, hidden=True, normalized_velocities=True, device=device)

        model = TargetModel(
            k=100, H=128, device=device,
            hidden=True,
            mode=["A","M"],
            appearance_model=appearance_model,
            motion_model=motion_model,
            interaction_model=None
        )

        trainer = Tester(
            model,
            crops=True,
            velocities=True,
            occupancy_grids=False,
            boxes=True,
            # model_checkpoint_path="./experiments/TargetModelAM_15_black_NormalizedVelocities_FirstNotPadded/model_13.tar",
            # model_checkpoint_path="./experiments/TargetModelAM_1516_yolo_bl_fix/model_3.tar",  # appearance cropovi su -1
            # model_checkpoint_path="./experiments/TargetModelAM_1516_yolo_extended_v2/model_16.tar",
            model_checkpoint_path="./experiments/TargetModelAM_pretrained_extended/model_16.tar",

            sequence_len=11,
            device=device,
            dump_videos=True
        )
        trainer.test(disable_a3=True)

    else:
        # JOINT
        print("JOINT")
        device = "cuda"
        print("JointModel")
        model = JointModel(
           k=500, H=500,
           device=device,
           hidden=True,
           backbone=models.resnet18(pretrained=True),
           normalized_velocities=True,
           use_bn=True,
           num_layers=1
        )

        trainer = Tester(
            model,
            crops=True,
            velocities=True,
            occupancy_grids=False,
            boxes=True,
            # model_checkpoint_path="./experiments/JointModel_1516/model_2.tar",
            # model_checkpoint_path="./experiments/JointModel_1516_app_head_extended_trainset/model_2.tar",
            # model_checkpoint_path="./experiments/JointModel_1516_app_head_extended_trainset/mode

            # model_checkpoint_path="./experiments/JointModel_1516_11step_extended/model_4.tar",
            # model_checkpoint_path="./experiments/JointModel_1516_aux_different_heads_extended_v2/model_19.tar",
            sequence_len=6,
            device=device,
            dump_videos=True
        )
        trainer.test(disable_a3=True)

    # device = "cuda"
    # appearance_model = AppearanceModel(
    #     k=500, H=500,
    #     siamese=True, device=device, hidden=True,
    #     use_bn=True, train_backbone=True,
    #     backbone=models.resnet18(pretrained=False)
    # )
    # motion_model = MotionModel(k=100, H=128, hidden=True, normalized_velocities=True, device=device)
    #
    # model = TargetModel(
    #     k=100, H=128, device=device,
    #     hidden=True,
    #     mode=["A","M"],
    #     appearance_model=appearance_model,
    #     motion_model=motion_model,
    #     interaction_model=None
    # )
    # trainer = Tester(
    #     model,
    #     crops=True,
    #     velocities=True,
    #     occupancy_grids=False,
    #     boxes=True,
    #     sequence_len=6+5,
    #     # model_checkpoint_path="./experiments/TargetModelAMI_black/model_4.tar",
    #     # model_checkpoint_path=[
    #     # model_checkpoint_path="./experiments/TargetModelAMI_black_NormalizedVelocities/model_16.tar",  # appearance cropovi su -1
    #     model_checkpoint_path="./experiments/TargetModelAM_15_black_NormalizedVelocities_FirstNotPadded/model_4.tar",
    #     #     "./experiments/AppearanceModelRnnVgg11bnClipOrigLR/model_20.tar",
    #     #     "./experiments/MotionModelOrigLr/model_10.tar"
    #     #     # "./experiments/InteractionModelOrigLr/model_10.tar"
    #     # ],
    #     device=device,
    #     num_workers=10,
    # )
    # trainer.test(disable_a3=True)
