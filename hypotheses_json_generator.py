# [
#     {
#         "frames": [
#             {
#                 "timestamp": 0.054,
#                 "num": 0,
#                 "class": "frame",
#                 "hypotheses": [
#                     {
#                         "height": 31.0,
#                         "width": 31.0,
#                         "id": "sheldon",
#                         "y": 105.0,
#                         "x": 608.0
#                     }
#                 ]
#             },
#             {
#                 "timestamp": 3.854,
#                 "num": 95,
#                 "class": "frame",
#                 "hypotheses": [
#                     {
#                         "height": 31.0,
#                         "width": 31.0,
#                         "id": "sheldon",
#                         "y": 105.0,
#                         "x": 608.0
#                     },
#                     {
#                         "height": 38.0,
#                         "width": 29.0,
#                         "id": "leonard",
#                         "y": 145.0,
#                         "x": 622.0
#                     }
#                 ]
#             }
#         ],
#         "class": "video",
#         "filename": "/cvhci/data/multimedia/bigbangtheory/bbt_s01e01/bbt_s01e01.idx"
#     }
# ]

import json
import os
import pandas as pd
import datasets.dataset_utils as du

# pred dumping

def create_hypotheses_for_videos(videos, dir="./res"):
    for video in videos:
        data = []
        video_data = {
            "class": "video",
            "filename": os.path.join(dir, video)
        }

        frames=list()
        gt_path = os.path.join(dir, video, "hypo.txt")
        gt_boxes = pd.read_csv(gt_path, header=None).values[:, :7]
        gt_boxes = [du.Box(video, *box) for box in gt_boxes]
        gt_boxes_dict = du.create_sequence_gt_boxes_dict(gt_boxes)

        for frame_number in gt_boxes_dict:
            frame = {
                "timestamp": float(frame_number*0.01),
                "num": int(frame_number),
                "class": "frame"
            }

            annotations = []
            objects = list(gt_boxes_dict[frame_number].values())
            for obj in objects:
                obj = {
                    "height": float(obj.bb_height),
                    "width": float(obj.bb_width),
                    "id": str(obj.obj_id),
                    "y": float(obj.bb_top),
                    "x": float(obj.bb_left)
                }
                annotations.append(obj)

            frame["hypotheses"]=annotations

            frames.append(frame)

        video_data["frames"] = frames

        data.append(video_data)

    return data


if __name__ == "__main__":
    # print(json.dumps(data, indent=4))
    dir="./data/2DMOT2015/train"
    videos = os.listdir(dir)
    data = create_hypotheses_for_videos(videos)
    with open('hypotheses.json', 'w') as outfile:
        json.dump(data, outfile)
