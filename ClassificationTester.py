import torch

import torch.nn as nn
import numpy as np
import time
from torchvision import models

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from datasets.classification.MOT2015 import get_loaders
from models.Appearance import AppearanceModel
from models.Motion import MotionModel
from models.Interaction import InteractionModel
from models.Target import TargetModel


class Tester:

    def __init__(
            self,
            model,
            batch_size = 64,
            model_checkpoint_path = None,
            crops=False,
            velocities=True,
            occupancy_grids=True,
            num_workers = 0,
            sequence_length = 6,
            device = None
    ):

        # Device configuration
        self.device = device if device is not None else torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        print("Trainer device:", self.device)
        self.batch_size = batch_size

        _, self.test_loader = get_loaders(
            self.batch_size,
            shuffle=False,
            num_workers=num_workers,
            crops=crops,
            velocities=velocities,
            occupancy_grids=occupancy_grids,
            sequence_length=sequence_length,
            train=False,
            test=True
        )

        self.model = model.to(self.device).eval()
        self.criterion = nn.BCEWithLogitsLoss(size_average=True).to(self.device)

        self.start_epoch = self.model.load_state(
            model_checkpoint_path,
            resume_train=False, optimizer=None)

    def test(self):
        with torch.no_grad():
            self.model.eval()
            test_total_step = len(self.test_loader)
            num_of_ones = 0
            correct = 0
            total = 0
            test_loss = 0

            print("Evaluation started")

            for test_iter, test_sample in enumerate(self.test_loader):
                test_labels = test_sample['label'].to(self.device)
                test_logits, _ = self.model(test_sample)
                # test_loss += self.criterion(test_logits, test_labels).item()
                #
                # num_of_ones += torch.sum(test_predicted_classes).cpu().data.numpy()
                #
                # correct += (test_predicted_classes == test_labels).sum().cpu().data.numpy()
                # total += test_labels.cpu().data.numpy().shape[0]
                #
                # if (test_iter + 1) % 1 == 0:
                #     print('Testing step {}/{} acc: {}.'.format(test_iter+1, test_total_step, correct/total))
                #
                # # print("total_step_time:", time.time() - start)
                # # load_start = time.time()
                #
                # # visualization
                logs = np.array(test_logits > 0.)
                labs = np.array(test_labels)

                # display_objects_pos = [i for i, (lo, la) in enumerate(zip(logs, labs)) if lo == la][:10]
                # display_objects_pos = [i for i, la in enumerate(labs) if la][:10]
                display_objects_neg = [i for i, (lo, la) in enumerate(zip(logs, labs)) if lo != la][:5]
                display_objects = display_objects_neg

                # display_objects = display_objects_pos
                if len(display_objects) > 0:

                    print("step:", test_iter)
                    print('logs:', logs[display_objects])
                    print('labs:', labs[display_objects])

                    seq_len = len(test_sample['image_crops'])
                    f, axarr = plt.subplots(max(2, len(display_objects)), seq_len+1, figsize=(50, 50))

                    for y,o in enumerate(display_objects):
                        for i, crop in enumerate(test_sample["image_crops"]):
                            crop = np.array(crop[o]).swapaxes(0, -1).swapaxes(0, 1)[:,:,[2,1,0]]

                            axarr[y, i].imshow(crop/255)
                            vel = test_sample['sequence_velocities'][i][o].cpu().detach().numpy()

                            # o_grid = test_sample['sequence_occupancy_grids'][i][o].cpu().detach().numpy()

                            # axarr[y, i].imshow(o_grid.reshape((7,7)), cmap="Greys")
                            axarr[y, i].set_title('v({})'.format(vel), fontsize=10)

                        crop = np.array(test_sample["candidate_crop"][o]).swapaxes(0, -1).swapaxes(0, 1)[:,:,[2,1,0]]
                        vel_of_candidate = test_sample["candidate_velocity"][o].cpu().detach().numpy()
                        # o_grid_of_candidate = test_sample["candidate_occupancy_grid"][o].cpu().detach().numpy()

                        axarr[y, -1].imshow(crop/255)
                        # axarr[y, -1].imshow(o_grid_of_candidate.reshape((7,7)), cmap="Greys")
                        axarr[y, -1].set_title('v({})'.format(vel_of_candidate), fontsize=10)

                    # plt.show(block=True)
                    print("dumped iter : "+str(test_iter))
                    plt.savefig("./testing_figure/testing_figure_pos"+str(test_iter))

            print('Test set: Loss: {:.4f}, Accuracy: {:.4f} Pred_ones: {}'
                .format(
                test_loss/test_total_step,
                correct/total,
                num_of_ones/total)
            )


if __name__ == "__main__":

    # model = AppearanceModel(backbone=models.resnet18(), k=500, H=500, siamese=True, hidden=True, use_bn=True, device='cpu')
    # model = MotionModel(k=100, H=128, device='cpu')
    # model = InteractionModel(k=100, H=128, device='cuda')
    # model = TargetModel(k=100, H=128)
    model = AppearanceModel(
        k=500, H=500,
        backbone=models.resnet18(pretrained=True),
        siamese=True,
        hidden=True,
        train_backbone=True,
        use_bn=True,
        use_dropout=False,
        device='cpu',
    )
    trainer = Tester(
        model,
        batch_size=10,
        crops=True,
        velocities=True,
        occupancy_grids=False,
        model_checkpoint_path="./experiments/res18_yolo_1516_hidden_500_500_bl_fix_10_1000_gap_pos_neg/model_16.tar",
        device='cpu',
        num_workers=10
    )
    trainer.test()

    # # Target model
    # model = TargetModel(k=100, H=128, device='cuda')
    # trainer = Tester(
    #     model,
    #     crops=True,
    #     velocities=True,
    #     occupancy_grids=True,
    #     sequence_length=6+5,
    #     model_checkpoint_path="./experiments/TargetModelAMI/model_12.tar",
    #     # model_checkpoint_path="./experiments/TargetModelAMI/model_12.tar",
    #     # model_checkpoint_path=[
    #     #     "./experiments/AppearanceModelRnnVgg11bnClipOrigLR/model_20.tar",
    #     #     "./experiments/MotionModelOrigLr/model_10.tar"
    #     #     # "./experiments/InteractionModelOrigLr/model_10.tar"
    #     # ],
    #     device='cuda',
    #     num_workers=10,
    # )
    # trainer.test()

