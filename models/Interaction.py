import torch
import numpy as np
import torch.nn as nn
import itertools
from IPython import embed

from .Model import Model

class InteractionModel(Model):
    def __init__(self, k=100, H=128, occupancy_grid_size=7**2, device=None):
        super(InteractionModel, self).__init__()

        self.device = device if device else torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        # self.device = "cpu"
        print("InteractionModel device:"+str(self.device))

        self.k = k
        self.H = H
        self.occupancy_grid_size = occupancy_grid_size

        self.candiate_fc = nn.Linear(self.occupancy_grid_size, self.H)

        # RNN
        self.lstm = nn.LSTM(self.occupancy_grid_size, self.H, batch_first=False)

        # FC
        self.fc = nn.Linear(2*self.H, self.k)
        self.relu = nn.ReLU()
        self.bn = nn.BatchNorm1d(self.k)
        self.last_fc = nn.Linear(self.k, 1)


    def forward(self, sample, tracking=False):
        if not tracking:
            sequence_occupancy_grids, candidate_occupancy_grid, labels = (
                sample['sequence_occupancy_grids'],
                sample['candidate_occupancy_grid'],
                sample['label']
            )

            batch_size = labels.shape[0]
            seq_len = len(sequence_occupancy_grids)

            # (seq_len, batch, input_size)
            sequence_occupancy_grids = torch.stack(sequence_occupancy_grids).to(self.device)
            assert sequence_occupancy_grids.shape == (seq_len, batch_size, self.occupancy_grid_size)

            # Forward propagate LSTM
            object_trajectory_features_sequence, _ = self.lstm(sequence_occupancy_grids)

            # (seq_length, batch_size, hidden_size)
            object_trajectory_features = object_trajectory_features_sequence[-1, :, :] # last state


            candidate_occupancy_grid_features = self.candiate_fc(candidate_occupancy_grid.to(self.device))

            comparison_features = torch.cat((object_trajectory_features, candidate_occupancy_grid_features), dim=-1)

            final_features = self.bn(self.relu(self.fc(comparison_features)))
            logits = self.last_fc(final_features).view(-1)

            return logits, final_features

        else:
            objects, candidates_grids = sample['objects'], sample['candidates_grids']
            # objects = [{ 'id': <id>, 'crops': [<tensor at step 1>, ..., <tensor at step 6>] }]
            assert len(candidates_grids) > 0
            assert len(objects) > 0

            num_objects = len(objects)

            objects_ids, objects = zip(*objects.items())
            objects_ids = np.array(objects_ids)
            objects_grids = torch.stack([torch.stack(obj["occupancy_grids"]) for obj in objects])
            objects_grids = objects_grids.permute(1, 0, 2)

            sequence_len = len(objects[0]["occupancy_grids"])

            # (seq, batch_size, feature)
            objects_trajectory_features_sequence, _ = self.lstm(objects_grids.to(self.device))
            objects_trajectory_features = objects_trajectory_features_sequence[-1, :, :] # last state

            candidates_grids = torch.cat(candidates_grids, dim=0)
            candidates_grids_features = self.candiate_fc(candidates_grids.to(self.device))


            # object_trajectory_features x candidate_crops_features
            num_candidates = len(candidates_grids_features)
            comparison_features = torch.cat((
                objects_trajectory_features.repeat([1, num_candidates]).view(num_objects*num_candidates, self.H),
                candidates_grids_features.repeat([num_objects, 1])
            ), dim=-1)

            objects_ids = objects_ids.repeat([num_candidates]).reshape(num_objects*num_candidates, 1)  # 1,1,2,2,3,3
            # candidates_indices = torch.arange(num_candidates).repeat([num_objects, 1]).reshape(num_objects*num_candidates, 1) # 1,2,3,1,2,3
            candidates_ids_tiled = torch.tensor(sample["candidates_ids"]).repeat([num_objects, 1]).reshape(num_objects*num_candidates, 1) # 1,2,3,1,2,3
            edges = np.concatenate((objects_ids, candidates_ids_tiled), axis=1)

            final_features = self.bn(self.relu(self.fc(comparison_features)))
            logits = self.last_fc(final_features)

            return edges, logits, final_features
