import numpy as np
from termcolor import colored
from IPython import embed
import os
# import matplotlib
# matplotlib.use('Agg')
from collections import deque, defaultdict

import cv2
import numpy as np
import torch
import torch.nn as nn
from scipy.optimize import linear_sum_assignment
from sklearn.svm import LinearSVC
from sklearn.externals import joblib

from datasets.tracking.MOT2015 import get_loaders
from datasets.dataset_utils import Box
from models.Appearance import AppearanceModel

def check_fitted(clf):
    return hasattr(clf, "classes_")

rprint = lambda *args: print(colored("".join(map(str, args)), "red"))
bprint = lambda *args: print(colored("".join(map(str, args)), "blue"))

class Trainer:

    def __init__(
            self,
            model,
            model_checkpoint_path = None,
            crops=True,
            velocities=True,
            occupancy_grids=True,
            num_workers=20,
            device = None,
            threshold=0.5,
            sequence_len=6,
    ):

        # Device configuration
        self.device = device if device is not None else torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        print("Tester device:", self.device)

        self.train_loader, _ = get_loaders(
            batch_size=1,
            num_workers=num_workers,
            crops=crops,
            velocities=velocities,
            occupancy_grids=occupancy_grids,
            boxes=True,
            train=True,
            test=False
        )

        self.dataset = self.train_loader.dataset
        self.model = model.to(self.device).eval()
        self.model_checkpoint_path = model_checkpoint_path

        # Loss and optimizer
        self.criterion = nn.CrossEntropyLoss(size_average=True).to(self.device)

        self.start_epoch = self.model.load_state(
            self.model_checkpoint_path,
            resume_train=False
        )
        self.threshold = threshold
        self.sequence_len = sequence_len

        self.sigmoid = torch.nn.Sigmoid()

        self.svm = LinearSVC()
        self.svm_dataset=[]


    def pad_crops(self, crops):
        assert len(crops) <= 6
        return [torch.zeros_like(crops[0])] * (self.sequence_len - len(crops)) + crops

    def pad_locations(self, locations):
        assert len(locations) <= 6
        return [torch.zeros_like(locations[0])] * (self.sequence_len - len(locations)) + locations

    def pad_velocities(self, velocities):
        assert len(velocities) <= 6
        return [torch.zeros_like(velocities[0])] * (self.sequence_len - len(velocities)) + velocities

    def create_occupancy_grids(self, obj):
        frames_numbers = obj["frames_numbers"]
        video_dict = self.dataset.videos_dict[self.current_video_name]
        frames_object_bboxes = obj["bboxes"]
        frames_neighbours_boxes = [ video_dict[frame_number]['detections'] for frame_number in frames_numbers ]
        occupancy_grids = [
            torch.from_numpy(
                self.dataset.create_occupancy_grid(
                    Box(self.current_video_name, frame_num, obj["id"], *frame_object_box, 1.0),
                    frame_neighbours_boxes
                )
            ).float() for frame_num, frame_object_box, frame_neighbours_boxes
            in zip(
                frames_numbers, frames_object_bboxes, frames_neighbours_boxes)
        ]

        return occupancy_grids

    def pad_occupancy_grids(self, occupancy_grids):
        assert len(occupancy_grids) <= 6
        return [torch.zeros_like(occupancy_grids[0])] * (self.sequence_len - len(occupancy_grids)) + occupancy_grids

    def create_padded_crops(self, crop):
        return deque([torch.zeros_like(crop)] * (self.sequence_len - 1) + [crop], maxlen=self.sequence_len)

    def create_padded_grids(self, grid):
        return deque([torch.zeros_like(grid)] * (self.sequence_len - 1) + [grid], maxlen=self.sequence_len)

    def create_padded_locations_and_velocities(self, location):
        return deque([torch.tensor([0.0, 0.0])] * (self.sequence_len -1 ) + [location], maxlen=self.sequence_len),\
               deque([torch.tensor([0.0, 0.0])] * self.sequence_len, maxlen=6)

    def dump_bbox(self, box, id, frame, video_name):
        # < frame >, < id >, < bb_left >, < bb_top >, < bb_width >, < bb_height >, < conf >, < x >, < y >, < z >
        with open("./results/AppearanceModel/"+video_name+".txt", "a+") as file:
            file.write(
                ("{},"*9+"{}").format(
                    int(frame),
                    int(id),
                    float(box[0]),
                    float(box[1]),
                    float(box[2]),
                    float(box[3]),
                    float(0.99),
                    1, 1, 1)+"\n")

    def dump_box(self, box, id, video_name):
        # < frame >, < id >, < bb_left >, < bb_top >, < bb_width >, < bb_height >, < conf >, < x >, < y >, < z >
        with open("./results/AppearanceModel/"+video_name+".txt", "a+") as file:
            file.write(
                ("{},"*9+"{}").format(
                    int(box[0]),
                    int(id),
                    float(box[2]),
                    float(box[3]),
                    float(box[4]),
                    float(box[5]),
                    float(box[6]),
                    1, 1, 1)+"\n")

    # def parse_boxes(in_boxes, video_name):
    #     boxes = list()
    #     for box in in_boxes:
    #         left = box[2]
    #         top = box[3]
    #
    #         if left < 0:
    #             box[2] = 0
    #             box[4] = box[4] - abs(left)  # reduce with for the amount outside of border
    #
    #         if top < 0:
    #             box[3] = 0
    #             box[5] = box[5] - abs(top)  # reduce height for the amount outside of border
    #
    #         box = Box(video_name, *box)
    #
    #         boxes.append(box)
    #
    #     return boxes

    def get_crop(self, bbox, video_name, frame_number, as_torch=True):
        img_path = os.path.join(os.path.join(
            self.dataset.dataset_path,'2DMOT2015/train', video_name,'img1/',
            str(int(frame_number)).zfill(6) + ".jpg")
        )
        image = cv2.imread(img_path)
        assert image is not None

        image = image[
                int(np.floor(bbox[1])): int(np.ceil(bbox[1] + bbox[3])),
                int(np.floor(bbox[0])): int(np.ceil(bbox[0] + bbox[2]))
                ]

        crop = cv2.resize(image, dsize=(self.dataset.input_h, self.dataset.input_w), interpolation=cv2.INTER_CUBIC)

        if as_torch:
            crop = torch.from_numpy(crop).float().permute(2, 0, 1).unsqueeze(0)

        return crop


    def check_predictions_detections_overlap(self, obj, current_bbox, current_frame_number, threshold, frame=None):
        assert self.dataset.videos_dict is not None

        frames_dict = self.dataset.videos_dict[self. current_video_name]
        max_overlaps = []


        i = 0
        for bbox, frame_number in zip(list(obj["bboxes"])+[current_bbox], list(obj["frames_numbers"])+[current_frame_number]):
            i += 1
            detections = [
                (
                    box.bb_left,
                    box.bb_top,
                    box.bb_width,
                    box.bb_height,
                ) for box in frames_dict[frame_number]["detections"]
            ]

            max_overlaps.append(max([self.bb_intersection_over_union(det, bbox) for det in detections]))

        mean_overlap = sum(max_overlaps)/len(max_overlaps)

        return  mean_overlap > threshold, mean_overlap


    def bb_intersection_over_union(self, boxA, boxB):
        # determine the (x, y)-coordinates of the intersection rectangle
        xA = max(boxA[0], boxB[0])
        yA = max(boxA[1], boxB[1])
        xB = min(boxA[0] + boxA[2], boxB[0] + boxB[2])
        yB = min(boxA[1] + boxA[3], boxB[1] + boxB[3])

        # compute the area of intersection rectangle
        interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

        # compute the area of both the prediction and ground-truth
        # rectangles
        boxAArea = (boxA[0] + boxA[2] - boxA[0] + 1) * (boxA[1]+boxA[3] - boxA[1] + 1)
        boxBArea = (boxB[0] + boxB[2] - boxB[0] + 1) * (boxB[1]+boxB[3] - boxB[1] + 1)

        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        iou = interArea / float(boxAArea + boxBArea - interArea)

        # return the intersection over union value
        return iou

    def box_to_bbox(self, box):
        return np.array(box[2:6])

    def get_unsuppresed_indices(self, tracked_objects, candidate_boxes, suppres_above_iou_detection_thresh=0.5, confidence_thresh=0.5):
        # suppress detections
        tracked_bboxes = [to['bboxes'][-1] for to in tracked_objects.values()]
        candidate_bboxes = [self.box_to_bbox(box) for box in candidate_boxes]
        unsupressed_indices = [
            candidate_index for candidate_index, candidate_bbox in enumerate(candidate_bboxes)
            if max(
                [0.] + [self.bb_intersection_over_union(candidate_bbox, tr_bbox) for tr_bbox in tracked_bboxes]
            ) < suppres_above_iou_detection_thresh
        ]

        low_score_indices = [
            i for (i, box) in enumerate(candidate_boxes) if box[-1] < confidence_thresh
        ]

        unsupressed_indices = list(set(unsupressed_indices) - set(low_score_indices))
        return unsupressed_indices


    def prepare_lost_objects(self, lost_objects):
        for lost_id, lost_obj in lost_objects.items():
            bboxes = lost_obj["bboxes"]
            frames_numbers = lost_obj["frames_numbers"]

            assert len(bboxes) == len(frames_numbers)
            crops = [
                self.get_crop(
                    bbox, self.current_video_name, frame_number
                ) for bbox, frame_number in zip(bboxes, frames_numbers)
            ]

            lost_obj["crops"] = self.pad_crops(crops)

            locations = [
                torch.from_numpy(bbox[:2]).float()
                for bbox, frame_number in zip(bboxes, frames_numbers)
            ]
            occupancy_grids = self.create_occupancy_grids(lost_obj)

            velocities = [locations[i]-locations[i-1] for i in range(1, len(locations))]
            velocities = velocities if len(locations) > 1 else [0. * locations[0]]
            # todo: rijesi problem ako je dostupna samo jedna lokacija

            lost_obj["locations"] = self.pad_locations(locations)
            lost_obj["velocities"] = self.pad_velocities(velocities)
            lost_obj["occupancy_grids"] = self.pad_occupancy_grids(occupancy_grids)
            # todo: create occupancy grids, jel ovo rjeseno?

        return lost_objects


    def test(self):
        max_key = 0
        buffer_labels = []
        buffer_features = []

        svm_was_fitted = True
        epoch = 0
        while svm_was_fitted:
            epoch += 1
            if epoch >= 20: break

            # todo: odredi kaj od ovog je klasna varijabla kaj nije
            svm_was_fitted = False

            self.current_video_name = None
            self.current_frame_number = 0
            lost_objects = {}
            tracked_objects = {}
            lost_objects_counter = {}
            history = defaultdict(lambda : defaultdict(lambda: defaultdict(np.array))) # history[video_name][frame_num][id]=bbox
            frames_history = [] # only current video

            for iter, sample in enumerate(self.train_loader):
                print("\niter {}/{}".format(iter, len(self.train_loader)))

                candidate_image_crops = sample.get("image_crops", [])
                candidate_occupancy_grids = sample.get("occupancy_grids", [])
                candidate_locations = [location[0] for location in sample.get("locations", [])]
                candidate_boxes = [box[0] for box in sample.get("boxes", [])]
                frame = np.array(sample["frame"])[0]

                all_candidate_boxes = candidate_boxes

                unsupressed_indices = self.get_unsuppresed_indices(
                    tracked_objects, candidate_boxes, suppres_above_iou_detection_thresh=1.5, confidence_thresh=0.1
                )
                # we wont suppres during training for now?

                # remove suppresed candidates
                candidate_image_crops = [
                    candidate_image_crops[i] for i in unsupressed_indices] if candidate_image_crops else []
                candidate_occupancy_grids = [
                    candidate_occupancy_grids[i] for i in unsupressed_indices] if candidate_occupancy_grids else []
                candidate_locations = [
                    candidate_locations[i] for i in unsupressed_indices] if candidate_locations else []
                candidate_boxes = [
                    candidate_boxes[i] for i in unsupressed_indices] if candidate_boxes else []

                # num_candidates = max(
                #     len(candidate_image_crops),
                #     len(candidate_occupancy_grids),
                #     len(candidate_locations)
                # )

                if self.current_video_name != sample["video_name"][0]:
                    print("New video: ", self.current_video_name)
                    if self.current_video_name is not None:
                        visualize_video(history[self.current_video_name], frames_history)

                    self.current_frame_number = 1
                    frames_history = []
                    frames_history.append(frame)

                    self.current_video_name = sample["video_name"][0]

                    assert all([b[-1] > 0.5 for b in candidate_boxes])

                    tracked_objects = defaultdict(dict)
                    lost_objects = {}
                    lost_objects_counter = {}

                    for box in candidate_boxes:
                        id = int(box[1])
                        assert id not in tracked_objects
                        bbox = np.array([box[2], box[3], box[4], box[5]])
                        tracked_objects[id]["id"] = id
                        tracked_objects[id]["bboxes"] = deque([bbox], maxlen=self.sequence_len)
                        tracked_objects[id]["frames_numbers"] = deque([self.current_frame_number], maxlen=self.sequence_len)
                        # tracker = cv2.TrackerTLD_create()
                        tracker = cv2.TrackerMIL_create()
                        tracker.init(frame, tuple(bbox))
                        tracked_objects[id]["tracker"] = tracker

                else:
                    assert self.current_frame_number > 0
                    self.current_frame_number+=1
                    frames_history.append(frame)

                    # get new object locations and detect lost objects
                    newly_lost_objects_ids = []
                    for id, obj in tracked_objects.items():
                        ok, bbox = obj["tracker"].update(frame)
                        bbox = np.array(bbox)
                        enough_overlap, mean_over = self.check_predictions_detections_overlap(
                            obj, bbox, self.current_frame_number, 0.7, frame)

                        # print('enough overlap({}) {}: {} ok: {}'.format(mean_over, id, enough_overlap, ok))

                        if ok and enough_overlap:
                            # remember location
                            obj["bboxes"].append(bbox)
                            obj["frames_numbers"].append(self.current_frame_number)

                        else:
                            # mark object as lost
                            lost_objects_counter[id] = 0
                            newly_lost_objects_ids.append(id)
                            lost_objects[id] = obj


                    # remove lost object from tracked objects
                    for lost_object_id in newly_lost_objects_ids:
                        tracked_objects.pop(lost_object_id, None)

                    # forget objects lost for more than 6 steps
                    if len(lost_objects) > 0:
                        lost_ids = list(lost_objects_counter.keys())
                        for lost_id in lost_ids:
                            lost_objects_counter[lost_id] += 1
                            if lost_objects_counter[lost_id] >= 7:
                                print("forget: ", lost_id)
                                lost_objects.pop(lost_id)
                                lost_objects_counter.pop(lost_id)

                    print("lost ids:", lost_objects.keys())
                    if len(unsupressed_indices) > 0:
                        if len(lost_objects) > 0:

                            lost_objects = self.prepare_lost_objects(lost_objects)

                            num_candidates = len(unsupressed_indices)
                            candidate_ids = np.array([int(c[1]) for c in candidate_boxes])
                            # get network results
                            parsed_sample = {
                                "objects": lost_objects,
                                "candidates_crops": candidate_image_crops,
                                "candidates_grids": candidate_occupancy_grids,
                                "candidates_locations": candidate_locations,
                                "candidates_ids": candidate_ids
                            }
                            edges, logits, features = self.model(parsed_sample, tracking=True)
                            features = features.detach().numpy()

                            # if not check_fitted(self.svm):
                            if not svm_was_fitted:
                                weigths = self.sigmoid(logits).cpu().detach().numpy()
                                pred_classes = logits.detach().numpy() > 0.5

                            else:
                                weigths = self.svm.decision_function(features)
                                pred_classes = weigths > 0


                            objects_ids = edges[:, 0]
                            candidate_ids = edges[:, 1]
                            # candidate_ids = np.array([int(c[1]) for c in candidate_boxes])
                            # candidate_ids = candidate_ids[candidate_indices]

                            gt_classes = (objects_ids == candidate_ids)
                            # print("gt_classes:", gt_classes)
                            # print("pred_classes:", pred_classes)

                            false_indices = np.where(gt_classes != pred_classes)[0]
                            correct_indices = np.where(gt_classes == pred_classes)[0]

                            print("pr:", sum(pred_classes)/len(pred_classes), " gt:", sum(gt_classes)/len(gt_classes))

                            rprint("svm_accuracy:", len(correct_indices)/len(weigths))

                            unused_features = len(set.intersection(*[set(np.where(bf == 0)[0]) for bf in features]))
                            print("always_empty:", unused_features/features[1].shape[0])

                            if len(false_indices) > 0:
                                if len(set(buffer_labels)) > 1:
                                    # we already have both classes in train set
                                    buffer_labels.extend(np.where(gt_classes[false_indices], 1, -1))
                                    buffer_features.extend(features[false_indices])

                                    print("fitting svm iter: {} , dataset len {}: ".format(iter,len(buffer_labels)))
                                    self.svm.fit(buffer_features, buffer_labels)
                                    svm_was_fitted = True

                                else:
                                    buffer_labels.extend(np.where(gt_classes, 1, -1))
                                    buffer_features.extend(features)

                                positives = len([b for b in buffer_labels if b == 1])
                                negatives = len([b for b in buffer_labels if b == -1])
                                print("balance: p(", positives, ")-n(", negatives, ")")

                                if negatives > positives:
                                    pos_indices = np.where(gt_classes)[0]
                                    pos_gt = gt_classes[pos_indices]
                                    pos_fe = features[pos_indices]
                                    buffer_labels.extend(np.ones_like(pos_gt))
                                    buffer_features.extend(pos_fe)
                                else:
                                    neg_indices = 1-np.where(gt_classes)[0]
                                    neg_gt = gt_classes[neg_indices]
                                    neg_fe = features[neg_indices]
                                    buffer_labels.extend(-1*np.ones_like(neg_gt))
                                    buffer_features.extend(neg_fe)

                                assert len(buffer_labels) == len(buffer_features)



                            num_lost = len(lost_objects)

                            # Hungarian
                            if np.prod(weigths.shape) != num_candidates*num_lost:
                                from IPython import embed
                                embed()

                            valid_minimum_edges = assign_edges(weigths, edges, num_candidates, num_lost)
                            # valid_minimum_edges = edges[gt_classes]

                            for obj_id, cand_id in valid_minimum_edges:

                                # assert exactly one element with id in candidates
                                assert len([(i,b) for i,b in enumerate(candidate_boxes) if int(b[1]) == cand_id]) == 1

                                cand_ind, candidate_box = [(i,b) for i,b in enumerate(candidate_boxes) if int(b[1]) == cand_id][0]
                                candidate_bbox = self.box_to_bbox(candidate_box)
                                lost_objects[obj_id]['bboxes'].append(candidate_bbox)
                                lost_objects[obj_id]["frames_numbers"].append(self.current_frame_number)

                                tracker = cv2.TrackerMIL_create()
                                # tracker = cv2.TrackerTLD_create()
                                tracker.init(frame, tuple(candidate_bbox))
                                lost_objects[obj_id]["tracker"]=tracker

                                if candidate_image_crops:
                                    candidate_image = candidate_image_crops[cand_ind]
                                    lost_objects[obj_id]['crops'].append(candidate_image)

                                print("object {} relocated".format(obj_id))


                                # if candidate_occupancy_grids:
                                    #     grid = candidate_occupancy_grids[cand_ind]
                                    #     tracked_objects[obj_id]['occupancy_grids'].append(grid)
                                    #
                                    # if candidate_locations:
                                    #     location = candidate_locations[cand_ind]
                                    #     velocity = torch.tensor(location-tracked_objects[obj_id]['locations'][-1])
                                    #     tracked_objects[obj_id]['locations'].append(location)
                                    #     tracked_objects[obj_id]['velocities'].append(velocity)

                            # move found objects back to tracked
                            for obj_id, _ in valid_minimum_edges:
                                tracked_objects[obj_id]=lost_objects[obj_id]
                                lost_objects.pop(obj_id)
                                lost_objects_counter.pop(obj_id)
                            print("lost_obj_counter:", lost_objects_counter)

                            # remove assigned candidates
                            cand_inds = valid_minimum_edges[:,1]
                            candidate_image_crops = [ cr for (i, cr) in enumerate(candidate_image_crops) if i not in cand_inds ]
                            candidate_occupancy_grids = [ oc for (i, oc) in enumerate(candidate_occupancy_grids) if i not in cand_inds ]
                            candidate_locations = [ oc for (i, oc) in enumerate(candidate_locations) if i not in cand_inds ]
                            candidate_boxes = [ oc for (i, oc) in enumerate(candidate_boxes) if i not in cand_inds ]

                    # add new objects to tracked
                    # keys = list(tracked_objects.keys())+list(lost_objects)+[max_key]
                    # max_key = max(keys)
                    for candidate_box in candidate_boxes:
                        # new_id = max_key+1+i
                        candidate_id = int(candidate_box[1])
                        if candidate_id in tracked_objects:
                            continue

                        new_id = int(candidate_box[1]) # todo: propusti samo one koji imaju novi id
                        tracked_objects[new_id] = {"id": new_id}

                        bbox = self.box_to_bbox(candidate_box)
                        tracked_objects[new_id]["bboxes"] = deque([bbox], maxlen=self.sequence_len)
                        tracked_objects[new_id]["frames_numbers"] = deque([self.current_frame_number], maxlen=self.sequence_len)
                        # tracker = cv2.TrackerTLD_create()
                        tracker = cv2.TrackerMIL_create()
                        # if not all(bbox[[1,0]] + bbox[[3,2]] < frame.shape[:2]):
                        #     print(bbox)
                        #     print(frame.shape)
                        #     vis_box(frame, *bbox)

                        bbox = fit_bbox_to_frame(bbox, frame)
                        tracker.init(frame, tuple(bbox))
                        tracked_objects[new_id]["tracker"] = tracker

                print("currently_tracked_objects:", list(tracked_objects.keys()))
                for id, obj in tracked_objects.items():
                    frame_num = obj["frames_numbers"][-1]
                    bbox = obj["bboxes"][-1]
                    history[self.current_video_name][frame_num][id]=bbox

        filename = os.path.join(
            os.path.dirname(self.model_checkpoint_path),
            "svm.pkl"
        )

        joblib.dump(self.svm, filename)
        # visualize_video(history[self.current_video_name], frames_history)

# shrinks bbox if it is outside of frame
def fit_bbox_to_frame(bbox, frame):
    x, y, w, h = bbox
    # box mora poceti unutar frame-a
    # ovo bi trebalo biti rijeseno unutar dataseta
    # todo: prebaci ovo unutar dataseta
    fh, fw, _ = frame.shape
    assert x >= 0
    assert y >= 0
    assert x+w <= fw
    assert y+h <= fh
    w = min(w, fw-x)
    h = min(h, fh-y)
    bbox = np.array([x, y, w, h])
    return bbox

import matplotlib.pyplot as plt
import matplotlib.patches as patches
def vis_box(frame, x,y,w,h):
    fig,ax = plt.subplots(1)
    ax.imshow(frame)
    rect = patches.Rectangle((x,y),w,h,linewidth=1,edgecolor='r',facecolor='none')
    ax.add_patch(rect)
    plt.show(block=True)


def visualize_video(video_history, frames_history):
    h, w = frames_history[0].shape[:2] # h, w, 3
    video = cv2.VideoWriter(
        "./visualization/output.avi",
        cv2.VideoWriter_fourcc('M','J','P','G'),
        10,
        (w, h)
    )

    for i, frame in enumerate(frames_history):

        for id, bbox in video_history[i+1].items():
            x, y, w, h = map(int,np.ceil(bbox))
            x1, y1, x2, y2 = x, y, x+w, y+h
            np.random.seed(id)
            r,g,b = map(int,np.random.randint(0, 256, 3))
            cv2.rectangle(frame, (x1, y1), (x2, y2), (r, g, b), 3)
            cv2.putText(frame, str(id), (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 3, (r,g,b), 3)

        frame = cv2.merge((
            frame[:,:,0],
            frame[:,:,1],
            frame[:,:,2]
        ))
        video.write(frame)
    video.release()
    exit()

def assign_edges(weigths, edges, num_candidates, num_lost):
    # use Hungarian
    probs = weigths
    neg_probs = 1 - weigths

    probs = probs.reshape(num_lost, num_candidates)
    neg_probs = neg_probs.reshape(num_lost, num_candidates)

    edges = edges.reshape(num_lost, num_candidates, 2)
    obj_indices, cand_indices = linear_sum_assignment(neg_probs)

    minimum_edges = edges[obj_indices, cand_indices]

    # valid_minimum_edges = minimum_edges[np.where(probs[obj_indices, cand_indices] > self.threshold)]
    # print("cand_indices:", cand_indices)
    valid_minimum_edges = minimum_edges[np.where(probs[obj_indices, cand_indices] > 0.5 )]

    return valid_minimum_edges

if __name__ == "__main__":

    model = AppearanceModel(k=500, H=500, device='cpu')
    # model = MotionModel(k=500, H=500)
    # model = InteractionModel(k=500, H=500, occupancy_grid_size=7**2)
    trainer = Trainer(
        model,
        crops=True,
        velocities=True,
        occupancy_grids=True,
        sequence_len=6,
        model_checkpoint_path="experiments/AppearanceModelRnnVgg11bnClipOrigLR/model_60.tar",
        device='cpu'
    )
    trainer.test()
