import json
import sys
import os
import matplotlib.pyplot as plt

d = sys.argv[1]
test_path = os.path.join(d,"test_stats.json")
with open(test_path) as test:
    test_stats = json.load(test)

train_path = os.path.join(d,"train_stats.json")
with open(train_path) as train:
    train_stats = json.load(train)

# accuracy
test_steps = sorted(test_stats.keys(), key=int)
train_steps = sorted(train_stats.keys(), key=int)

test_accs = [test_stats[i][1] for i in test_steps]
train_accs = [train_stats[i][1] for i in train_steps]


plt.subplot(2,1,1)
plt.plot(test_steps, test_accs, label="test")
plt.plot(train_steps, train_accs, label="train")
plt.title("Accuracy")
plt.legend()

# losses
plt.subplot(2,1,2)
test_losses = [test_stats[i][0] for i in test_steps]
train_losses = [train_stats[i][0] for i in train_steps]

plt.plot(test_steps, test_losses, label="test")
plt.plot(train_steps, train_losses, label="train")
plt.title("Losses")
plt.legend()
plt.show()
