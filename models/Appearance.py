import torch
import numpy as np
import itertools
import random
from IPython import embed
import matplotlib
import matplotlib.pyplot as plt
import torch.nn as nn
from torchvision import transforms, utils, models
import subprocess

from .Model import Model


def get_gpu_memory_map():
    result = subprocess.check_output(
        [
            'nvidia-smi', '--query-gpu=memory.used',
            '--format=csv,nounits,noheader'
        ])

    return float(result)


def sparsity(tensor):
    return torch.sum(tensor == torch.zeros_like(tensor)).double() / tensor.numel()


def pos_perc(tensor):
    return torch.sum(tensor > 0.0).double() / tensor.numel()


class AppearanceModel(Model):
    def __init__(self, k=100, H=128, device=None, siamese=False, train_backbone=True, use_bn=True, use_dropout=False, backbone=None, hidden=False):
        super(AppearanceModel, self).__init__()

        self.device = device if device else torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        print("AppearanceModel device:"+str(self.device))

        self.k = k
        self.H = H

        self.siamese = siamese
        self.train_backbone = train_backbone
        self.use_bn = use_bn
        self.use_dropout = use_dropout
        self.hidden = hidden

        if backbone is None:
            print("default backbone")
            self.cnn = models.vgg11_bn(pretrained=True)

        else:
            self.cnn = backbone

        if type(self.cnn) == models.resnet.ResNet:
            self.feature_extractor = 'resnet'

        elif type(self.cnn) == models.squeezenet.SqueezeNet:
            self.feature_extractor = 'sqnet'

        elif type(self.cnn) == models.vgg.VGG:
            self.feature_extractor = 'vgg'

        elif type(self.cnn) == models.densenet.DenseNet:
            self.feature_extractor = 'densenet'

        if not self.train_backbone:
            # do not train cnn, except the last layer
            print("Not training feature extractor")
            for param in self.cnn.parameters():
                param.requires_grad = False

            if self.feature_extractor != "resnet":
                for param in self.cnn.classifier:
                    param.requires_grad = True

        if self.feature_extractor == "resnet":
            print("resnet")
            num_ftrs = self.cnn.fc.in_features
            self.cnn.fc = nn.Linear(num_ftrs, self.H)

        if self.feature_extractor == 'densenet':
            print("densenet")
            num_ftrs = self.cnn.classifier.in_features
            self.cnn.classifier = nn.Linear(num_ftrs, self.H)

        if self.feature_extractor == "sqnet":
            print("sq")
            self.cnn.classifier._modules["1"] = nn.Conv2d(512, self.H, kernel_size=(1, 1), stride=(1, 1))
            self.cnn.num_classes = self.H

        if self.feature_extractor == "vgg":
            print("vgg")
            self.cnn.classifier._modules['6'] = nn.Linear(4096, self.H)  # replace last layer

        self.relu_cnn = nn.ReLU(inplace=True)

        if self.use_bn:
            print("use bn")
            self.bn_cnn = nn.BatchNorm1d(self.H)
            if self.use_dropout:
                print("use dropout")
                self.dropout = nn.Dropout(0.2)
                self.backbone = lambda t: self.dropout(self.bn_cnn(self.relu_cnn(self.cnn(t))))
            else:
                if not self.training:
                    from IPython import embed
                    embed()
                self.backbone = lambda t: self.bn_cnn(self.relu_cnn(self.cnn(t)))

        else:
            self.backbone = lambda t: self.relu_cnn(self.cnn(t))
            # self.backbone = lambda t: self.cnn(t)

        # lstm
        if not self.siamese:
            print("lstm")
            self.lstm = nn.LSTM(self.H, self.H, batch_first=False)

        # FC
        self.fc = nn.Linear(2*self.H, self.k, bias=True)
        self.relu = nn.ReLU(inplace=True)

        if self.use_bn:
            self.bn = nn.BatchNorm1d(self.k)

            if self.use_dropout:
                self.dropout = nn.Dropout(0.2)
                self.fc_layer = lambda x: self.dropout(self.relu(self.bn(self.fc(x))))

            else:
                self.fc_layer = lambda x: self.bn(self.relu(self.fc(x)))

        else:
            self.fc_layer = lambda x: self.relu(self.fc(x))

        # hidden FC
        if self.hidden:
            self.hidden_fc = nn.Linear(self.k, self.k, bias=True)
            self.hidden_relu = nn.ReLU(inplace=True)
            self.hidden_fc_layer = lambda x: self.hidden_relu(self.hidden_fc(x))

        # last FC
        self.last_fc = nn.Linear(self.k, 1, bias=False)
        self.to(self.device)

    @staticmethod
    def first_valid_crops(image_crops):
        # time_step, batch, ...
        with torch.no_grad():
            indices = torch.sum(torch.stack([torch.sum(ic, dim=[1, 2, 3]) == 0 for ic in image_crops]), dim=0)
            # in case all the steps are black
            indices = torch.min(indices, torch.ones_like(indices) * (len(image_crops) - 1))
            batch_size = len(indices)
            crops = torch.stack(list(image_crops))[indices, range(batch_size)]
        return crops

    def forward(self, sample, tracking=False):

        if not tracking:
            # classification mode
            image_crops, image_candidates, labels = sample['image_crops'], sample['candidate_crop'], sample['label']
            batch_size = labels.shape[0]
            sequence_len = len(image_crops)

            if self.siamese:
                # Siamese version
                image_candidates_features = self.backbone(image_candidates.to(self.device))
                image_crops = AppearanceModel.first_valid_crops(image_crops)
                object_trajectory_features = self.backbone(image_crops.to(self.device))
                # object_trajectory_features = self.backbone(image_crops[-1].to(self.device))

            else:
                # lstm version
                # gpu, this way it takes up less memory

                # cnn_train_index = random.randint(0, len(image_crops))  # [0-6]
                # if ccn_train_index is last train the candidate cnn
                # if cnn_train_index != len(image_crops) and self.train_backbone:
                #     with torch.no_grad():
                #         image_candidates_features = self.backbone(image_candidates.to(self.device))
                # else:
                image_candidates_features = self.backbone(image_candidates.to(self.device))

                image_crops_features = []
                for i, image_crops_timestep in enumerate(image_crops):

                    # if i != cnn_train_index and self.train_backbone:
                        #     with torch.no_grad():
                        #         timestep_features = self.backbone(image_crops_timestep.to(self.device))
                        # else:
                    timestep_features = self.backbone(image_crops_timestep.to(self.device))

                    image_crops_features.append(timestep_features)

                image_crops_features = torch.stack(image_crops_features)

                # Forward propagate LSTM
                object_trajectory_features_sequence, _ = self.lstm(image_crops_features)
                object_trajectory_features = object_trajectory_features_sequence[-1, :, :]  # last state

            comparison_features = torch.cat((object_trajectory_features, image_candidates_features), dim=-1)
            final_features = self.fc_layer(comparison_features)

            if self.hidden:
                final_features = self.hidden_fc_layer(final_features)

            logits = self.last_fc(final_features).view(-1)

            # # VISUALIZE
            # logs = np.array(logits > 0.)
            # labs = np.array(labels)
            # display_objects_pos = [i for  i,(lo,la) in enumerate(zip(logs,labs)) if lo == la][:5]
            # display_objects_neg = [i for  i,(lo,la) in enumerate(zip(logs,labs)) if lo != la][:5]
            # display_objects = display_objects_pos + display_objects_neg
            #
            # if len(display_objects_neg) == 0:
            #     return logits
            #
            # print('logs:', logs[display_objects])
            # print('labs:', labs[display_objects])
            #
            # f, axarr = plt.subplots(len(display_objects), 7, figsize=(50, 50))
            #
            # for y,o in enumerate(display_objects):
            #     for i, crop in enumerate(image_crops):
            #         crop = np.array(crop[o]).swapaxes(0, -1).swapaxes(0, 1)
            #         axarr[y, i].imshow(crop/255)
            #         axarr[y, i].set_title('obj_t'+str(i))
            #
            #     crop = np.array(image_candidates[o]).swapaxes(0, -1).swapaxes(0, 1)
            #     axarr[y, -1].imshow(crop/255)
            #     axarr[y, -1].set_title('cand')
            #
            # # plt.show(block=True)
            # plt.savefig("./classification_vis")
            # input("dump")

            return logits, final_features

        else:
            # tracking mode
            objects, candidate_crops = sample['objects'], sample['candidates_crops']
            # objects = [{ 'id': <id>, 'crops': [<tensor at step 1>, ..., <tensor at step 6>] }]

            num_objects = len(objects)

            objects_ids, objects = zip(*objects.items())
            objects_ids = np.array(objects_ids)

            sequence_len = len(objects[0]["crops"])
            if not self.siamese:
                objects_crops = list(itertools.chain(*[obj["crops"] for obj in objects]))
                objects_features = self.backbone(torch.cat(objects_crops, dim=0).to(self.device))
                # (seq, batch_size, feature)
                objects_features = objects_features.view(num_objects, sequence_len, self.H).permute(1, 0, 2)
                objects_trajectory_features_sequence, _ = self.lstm(objects_features)
                objects_trajectory_features = objects_trajectory_features_sequence[-1, :, :]  # last state

            else:
                objects_crops = [obj["crops"] for obj in objects]
                oc = torch.stack([torch.cat(oc, dim=0) for oc in objects_crops])
                bl_nums = torch.sum(torch.sum(oc, dim=[-1, -2, -3]) == 0, dim=1) # num of blackedout per obj
                assert len(bl_nums) == num_objects

                objs, steps = list(range(0, num_objects)), bl_nums.tolist()
                maxsteps = len(objects_crops[0])

                steps = [min(s, maxsteps-1) for s in steps]

                objects_crops = oc[objs, steps]
                objects_features = self.backbone(objects_crops.to(self.device))
                objects_trajectory_features = objects_features

            candidate_crops_features = self.backbone(torch.cat(candidate_crops, dim=0).to(self.device))

            # objects_features = torch.split(objects_features, lens, dim=0)
            # objects_features = objects_features.permute(1, 0, 2) # (steps, num_obj, feature_size)
            # [ (1, steps, feature_size) for ... ]

            # object_trajectory_features x candidate_crops_features
            num_candidates = len(candidate_crops_features)
            comparison_features = torch.cat((
                objects_trajectory_features.repeat([1, num_candidates]).view(num_objects*num_candidates, self.H),
                candidate_crops_features.repeat([num_objects, 1])
            ), dim=-1)

            # assert len(set([c.__repr__() for c in comparison_features])) == comparison_features.shape[0]

            objects_ids_tiled = objects_ids.repeat([num_candidates]).reshape(num_objects*num_candidates, 1)
            assert np.var(np.mean(objects_ids_tiled[:num_candidates], axis=-1)) <= 1e-5  # 1,1,2,2,3,3

            # edges = np.concatenate((objects_ids_tiled, candidates_indices), axis=1)
            candidates_ids = sample["candidates_ids"]
            candidates_ids_tiled = torch.tensor(candidates_ids).repeat([num_objects, 1]).reshape(num_objects*num_candidates, 1)  # 1,2,3,1,2,3
            edges = np.concatenate((objects_ids_tiled, candidates_ids_tiled), axis=1)
            assert len(set([e.__repr__() for e in edges])) == edges.shape[0]

            final_features = self.fc_layer(comparison_features)

            if self.hidden:
                final_features = self.hidden_fc_layer(final_features)

            logits = self.last_fc(final_features).view(-1)

            # # VISUALIZtE
            # print('num_obj:', num_objects)
            # print('num_cand:', num_candidates)
            #
            # def sigmoid(x, derivative=False):
            #     return x * (1 - x) if derivative else 1 / (1 + np.exp(-x))
            #
            # np.set_printoptions(suppress=True)
            # res = np.concatenate((edges, sigmoid(logits.cpu().detach().numpy().reshape(-1, 1))), axis=1)
            # print("edges, probs:", res)
            #
            # f, axarr = plt.subplots(1+num_objects, max(6, num_candidates), figsize=(15, 15))
            #
            # assert len(candidates_ids) == len(candidate_crops)
            # for i, (c_id, crop) in enumerate(zip(candidates_ids, candidate_crops)):
            #     crop = np.array(crop[0]).swapaxes(0, -1).swapaxes(0, 1)
            #     axarr[0, i].imshow(crop/255)
            #     axarr[0, i].set_title('c_'+str(int(c_id)))
            #
            # assert len(objects_ids) == len(objects)
            # for i, (o_id, obj) in enumerate(zip(objects_ids, objects)):
            #     crops = obj['crops']
            #     for j, crop in enumerate(crops):
            #         crop = np.array(crop[0]).swapaxes(0, -1).swapaxes(0, 1)
            #         axarr[i+1, j].imshow(crop/255)
            #         axarr[i+1, j].set_title('o_'+str(int(o_id)))
            #
            # # plt.show()
            # plt.savefig('tracking_test')
            # input("saved figure")
            # # from IPython import embed
            # # embed()

            return edges, logits, final_features


