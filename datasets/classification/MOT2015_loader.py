import warnings
from collections import namedtuple

from torch.utils.data import DataLoader
from torchvision import transforms

from datasets.classification.MOT2015 import MOT2015 as ClassificationDataset
from datasets.classification.MOT2015 import ToTensor as train_ToTensor

warnings.filterwarnings("ignore")

Box = namedtuple('Box', ['video_name', 'frame_number', 'obj_id', 'bb_left', 'bb_top', 'bb_width', 'bb_height', 'conf'])


# def create_sequence_gt_boxes_dict(gt_boxes):
#     # todo: change this to defaultdict(dict)
#     gt_boxes_dict = defaultdict(lambda: defaultdict(list))
#
#     for gt_box in gt_boxes:
#         assert type(gt_box) == Box
#         gt_boxes_dict[gt_box.frame_number][gt_box.obj_id]=gt_box
#
#     return gt_boxes_dict
#
#
# def parse_gt_boxes(gt_boxes, video_name):
#     boxes = list()
#     for box in gt_boxes:
#         left = box[2]
#         top = box[3]
#
#         if left < 0:
#             box[2] = 0
#             box[4] = box[4] - abs(left)  # reduce with for the amount outside of border
#
#         if top < 0:
#             box[3] = 0
#             box[5] = box[5] - abs(top)  # reduce height for the amount outside of border
#
#         box = Box(video_name, *box)
#
#         boxes.append(box)
#
#     return boxes

def get_loader(
        batch_size,
        shuffle=False,
        crops=True,
        velocities=True,
        boxes=False,
        future_seq_len=6,
        occupancy_grids=True,
        num_workers=0,
        train=True,
        test=True,
):
    print("ne")
    exit()
    loaders = []
    if train:
        composed = transforms.Compose([train_ToTensor(crops=crops, velocities=velocities, occupancy_grids=occupancy_grids)])
        train_dataset = ClassificationDataset(composed, crops=crops, velocities=velocities, occupancy_grids=occupancy_grids, test_set=False, future_seq_len=future_seq_len)
        train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers)
        loaders.append(train_loader)

    if test:
        composed = transforms.Compose([train_ToTensor(crops=crops, velocities=velocities, occupancy_grids=occupancy_grids)])
        test_dataset = ClassificationDataset(composed, crops=crops, velocities=velocities, occupancy_grids=occupancy_grids, test_set=True)
        test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False, num_workers=1)
        loaders.append(test_loader)

    return loaders

