import torch
import torch.nn as nn
import abc
import os

class Model(nn.Module):

    @abc.abstractmethod
    def forward(self, sample):
        pass

    def save_state(self, path, optimizer=None, epoch=None, loss=None):
        print("Saving state to ", path)
        # create subdirectories if they don't exits
        dirname = os.path.dirname(os.path.abspath(path))
        if not os.path.exists(dirname):
            os.makedirs(dirname)

        torch.save({
            k: v for k,v in [
            ('epoch', epoch),
            ('model_state_dict', self.state_dict()),
            ('optimizer_state_dict', optimizer.state_dict()),
            ('loss', loss)
        ] if v is not None
        }, path)


    def load_state(self, path, resume_train=False, optimizer=None):
        print("Loading state from ", path)
        checkpoint = torch.load(path, self.device)

        # todo: pack this as abstractproperty
        self.load_state_dict(checkpoint['model_state_dict'])

        if resume_train:
            print("Loading optimizer state.")
            if optimizer is None:
                raise ValueError("Optimizer can't be None if resume_train is True.")

            optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
            epoch = checkpoint['epoch']+1

        else:
            epoch = 1

        return epoch

