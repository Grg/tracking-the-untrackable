from matplotlib import pyplot as plt
import csv
from IPython import embed
import os
import cv2
import numpy as np
from gluoncv import model_zoo, data, utils

net = model_zoo.get_model('faster_rcnn_resnet101_v1d_coco', pretrained=True)
# net = model_zoo.get_model('yolo3_darknet53_coco', pretrained=True)

video_names = os.listdir(os.path.join("./data/MOT/train"))

for video_name in video_names:

    print("video_name:", video_name)

    dir = "./data/MOT/train/{}/img1/".format(video_name)

    frames = sorted(os.listdir(dir))
    detections = []

    for i, frame in enumerate(frames):

        im_fname = os.path.join(dir, frame)
        print("im_fname {} frame {} ".format(im_fname, frame))

        x, orig_img = data.transforms.presets.rcnn.load_test(im_fname)

        box_ids, scores, bboxes = net(x)
        bboxes = bboxes.asnumpy()
        scores = scores.asnumpy()

        sq_scores = np.squeeze(scores)
        score_indices = np.where(sq_scores >= 0)[0]

        sq_ids = np.squeeze(box_ids.asnumpy())
        person_indices = np.where(sq_ids == 0)[0]

        indices = np.intersect1d(score_indices, person_indices)

        img = cv2.imread(im_fname)
        bboxes = bboxes[0][indices]
        scores = scores[0][indices]

        # scale bboxes back
        ratios = (np.array(img.shape[:2])/np.array(orig_img.shape[:2])).repeat(2)
        bboxes = bboxes*ratios

        assert len(bboxes) == len(scores)

        for bbox, score in zip(bboxes, scores):
            x1, y1, x2, y2 = map(int, np.ceil(bbox))

            w = x2-x1
            assert w > 0

            h = y2-y1
            assert h > 0

            # 1,-1,198,203,62.22,141.19,42.848,-1,-1,-1
            # < frame >, < id >, < bb_left >, < bb_top >, < bb_width >, < bb_height >, < conf >, < x >, < y >, < z >
            detection = [i+1, -1, x1, y1, w, h, float(score*100), -1, -1, -1]
            detections.append(detection)

            r, g, b = map(int, np.random.randint(0, 256, 3))
            if score > 0.5:
                cv2.rectangle(img, (x1, y1), (x2, y2), (r, g, b), 3)

    dump_path = "./data/MOT/train/{}/det/faster_det.txt".format(video_name)

    with open(dump_path, 'w+') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(detections)
    csvFile.close()

