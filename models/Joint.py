import torch
import numpy as np
import itertools
import random
from IPython import embed
import matplotlib
import  matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
from models.Motion import MotionModel
from torchvision import transforms, utils, models
import subprocess

from .Model import Model
from datasets import dataset_utils


def get_gpu_memory_map():
    result = subprocess.check_output(
        [
            'nvidia-smi', '--query-gpu=memory.used',
            '--format=csv,nounits,noheader'
        ])

    return float(result)


def sparsity(tensor):
    return torch.sum(tensor == torch.zeros_like(tensor)).double() / tensor.numel()


def pos_perc(tensor):
    return torch.sum(tensor > 0.0).double() / tensor.numel()


class JointModel(Model):
    def __init__(self, k=100, H=128,
                 occupancy_grid_size=7**2,
                 device=None,
                 train_backbone=True,
                 hidden=False,
                 use_bn=True,
                 normalized_velocities=False,
                 num_layers=1,
                 sequence_len=6,
                 bidir_pool=False,
                 backbone=None,
                 add_heads=False,
                 attention=False,
    ):
        super(JointModel, self).__init__()

        self.device = device if device else torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        print("AppearanceModel device:"+str(self.device))

        self.k = k
        self.H = H
        self.hidden = hidden
        self.normalized_velocities = normalized_velocities
        self.occupancy_grid_size = occupancy_grid_size
        self.attention = attention

        self.train_backbone = train_backbone
        self.use_bn = use_bn

        self.bidir_pool = bidir_pool

        # feature extractor
        self.prepare_feature_extractor(backbone)
        # self.candidate_velocity_fc = nn.Linear(2, 100)
        self.candiate_velocity_fc = nn.Linear(2, 100)
        # self.candiate_occupancy_fc = nn.Linear(self.occupancy_grid_size, 100)

        # lstm
        self.lstm = nn.LSTM(self.H+2, self.H, batch_first=False, num_layers=num_layers, bidirectional=bidir_pool)

        # FC
        if not self.bidir_pool:
            self.fc = nn.Linear(self.H+100+self.H, self.k, bias=True)
        else:
            assert self.bidir_pool or self.attention
            self.fc = nn.Linear(2*self.H+100+self.H, self.k, bias=True)

        self.relu = nn.ReLU(inplace=True)

        if self.use_bn:
            self.bn = nn.BatchNorm1d(self.k)
            self.fc_layer = lambda x: self.bn(self.relu(self.fc(x)))
        else:
            self.fc_layer = lambda x: self.relu(self.fc(x))

        # hidden FC
        if self.hidden:
            self.hidden_fc = nn.Linear(self.k, self.k, bias=True)
            self.hidden_relu = nn.ReLU(inplace=True)
            self.hidden_fc_layer = lambda x: self.hidden_relu(self.hidden_fc(x))

        # last FC
        self.last_fc = nn.Linear(self.k, 1, bias=False)

        if self.attention:
            self.q_fc = nn.Linear(self.H+100, self.H, bias=True)
            self.q_relu = nn.ReLU(inplace=True)
            self.query_fc = lambda x: self.q_relu(self.q_fc(x))

        self.add_heads = add_heads
        if self.add_heads:
            print("add heads")
            # self.app_fc = nn.Linear(500+self.H, 1, bias=False)
            # self.mo_fc = nn.Linear(100+self.H, 1, bias=False)
            self.app_fc1 = []
            self.app_fc2 = []
            self.app_fc = []

            for i in range(sequence_len):
                if self.bidir_pool:
                    self.app_fc1.append(nn.Linear(500+self.H*2, self.H, bias=True).to(self.device))
                else:
                    self.app_fc1.append(nn.Linear(500+self.H, self.H, bias=True).to(self.device))

                self.app_fc2.append(nn.Linear(self.H, 1, bias=False).to(self.device))

                l = lambda x: self.app_fc2[i](self.app_fc1[i](x))
                self.app_fc.append(l)

        if self.attention:
            self.key_fcs = []

            for i in range(sequence_len):
                self.key_fcs.append(nn.Linear(2*self.H, self.H, bias=True).to(self.device))

            num_att_heads = 5
            self.attention_fc = nn.Linear(2*self.H, num_att_heads, bias=True).to(self.device)



    def prepare_feature_extractor(self, backbone):
        if backbone is None:
            self.cnn = models.vgg11_bn(pretrained=True)

        else:
            self.cnn = backbone

        if type(self.cnn) == models.resnet.ResNet:
            self.feature_extractor = 'resnet'

        elif type(self.cnn) == models.squeezenet.SqueezeNet:
            self.feature_extractor = 'sqnet'

        elif type(self.cnn) == models.vgg.VGG:
            self.feature_extractor = 'vgg'

        elif type(self.cnn) == models.densenet.DenseNet:
            self.feature_extractor = 'densenet'

        if not self.train_backbone:
            # do not train cnn, except the last layer
            print("Not training feature extractor")
            for param in self.cnn.parameters():
                param.requires_grad = False

            if self.feature_extractor != "resnet":
                for param in self.cnn.classifier:
                    param.requires_grad = True

        if self.feature_extractor == "resnet":
            print("resnet")
            num_ftrs = self.cnn.fc.in_features
            self.cnn.fc = nn.Linear(num_ftrs, self.H)

        if self.feature_extractor == 'densenet':
            print("densenet")
            num_ftrs = self.cnn.classifier.in_features
            self.cnn.classifier = nn.Linear(num_ftrs, self.H)

        if self.feature_extractor == "sqnet":
            print("sq")
            self.cnn.classifier._modules["1"] = nn.Conv2d(512, self.H, kernel_size=(1, 1), stride=(1, 1))
            self.cnn.num_classes = self.H

        if self.feature_extractor == "vgg":
            print("vgg")
            self.cnn.classifier._modules['6'] = nn.Linear(4096, self.H)  # replace last layer

        self.relu_cnn = nn.ReLU(inplace=True)

        if self.use_bn:
            print("use bn")
            self.bn_cnn = nn.BatchNorm1d(self.H)
            self.backbone = lambda t: self.bn_cnn(self.relu_cnn(self.cnn(t)))

        else:
            self.backbone = lambda t: self.relu_cnn(self.cnn(t))
            # self.backbone = lambda t: self.cnn(t)

    def forward(self, sample, tracking=False):

        if not tracking:
            # classification mode
            image_crops, image_candidates, labels = sample['image_crops'], sample['candidate_crop'], sample['label']
            sequence_velocities, candidate_velocity = sample['sequence_velocities'], sample['candidate_velocity']

            batch_size = labels.shape[0]
            sequence_len = len(image_crops)

            if self.normalized_velocities:
                sequence_velocities, candidate_velocity = MotionModel.calcualte_normalized_velocities(sample, batch_size)
            else:
                sequence_velocities = torch.stack(sequence_velocities)

            assert sequence_velocities.shape == (sequence_len, batch_size, 2)

            image_candidates_features = self.backbone(image_candidates.to(self.device))
            candidate_velocity_features = self.candiate_velocity_fc(candidate_velocity.to(self.device))

            candidate_features = torch.cat((
                image_candidates_features,
                candidate_velocity_features,
            ), dim=-1)

            sequence_features = []
            assert len(image_crops) == len(sequence_velocities)

            for i, (image_crops_timestep, velocity_timestep) in enumerate(zip(image_crops, sequence_velocities)):
                sequence_crops_features = self.backbone(image_crops_timestep.to(self.device))
                timestep_features = torch.cat((sequence_crops_features, velocity_timestep.to(self.device)), dim=-1)
                sequence_features.append(timestep_features)

            sequence_features = torch.stack(sequence_features)
            # print(torch.mean(sequence_features[:,:4,-2:], -1))

            # Forward propagate LSTM
            object_trajectory_features_sequence, _ = self.lstm(sequence_features)


            if self.attention:
                print("attention")
                query_vector = self.query_fc(candidate_features)

                per_step_features = torch.split(object_trajectory_features_sequence, 1, dim=0)
                from IPython import embed
                embed()
                per_step_keys = [self.key_fcs[i](torch.squeeze(step_features)) for i, step_features in enumerate(per_step_features)]

                attention_weights = [self.attention_fc(torch.cat((step_key, query_vector), dim=-1)) for step_key in per_step_keys]
                attention_values = F.softmax(torch.stack(attention_weights, dim=0), dim=0)

                attended_features = []
                for attention_head in torch.split(attention_values, 1, dim=-1):
                    attended_features.append(attention_head * object_trajectory_features_sequence)
                    # TODO: assert with tile

                attended_features = torch.sum(torch.cat(attended_features, dim=-1), dim=0)


            elif self.bidir_pool:
                object_trajectory_features = torch.mean(object_trajectory_features_sequence, dim=0)

            else:
                object_trajectory_features = object_trajectory_features_sequence[-1, :, :]  # last state

            comparison_features = torch.cat((object_trajectory_features, candidate_features), dim=-1)
            final_features = self.fc_layer(comparison_features)

            if self.hidden:
                final_features = self.hidden_fc_layer(final_features)

            logits = self.last_fc(final_features).view(-1)

            if self.add_heads and self.training:
                aux_logits = []

                # appearance_comparison_features = torch.cat((object_trajectory_features, image_candidates_features), dim=-1)
                # app_logits = self.app_fc(appearance_comparison_features).view(-1)
                # aux_logits.append(app_logits)

                # every step head
                for i, obj_step_features in enumerate(object_trajectory_features_sequence):

                    step_comparison_features = torch.cat((obj_step_features, image_candidates_features), dim=-1)
                    step_logits = self.app_fc[i](step_comparison_features.to(self.device))

                    aux_logits.append(step_logits)

                logits = (logits, *aux_logits)

            # # VISUALIZE
            # logs = np.array(logits > 0.)
            # labs = np.array(labels)
            # display_objects_pos = [i for  i,(lo,la) in enumerate(zip(logs,labs)) if lo == la][:5]
            # display_objects_neg = [i for  i,(lo,la) in enumerate(zip(logs,labs)) if lo != la][:5]
            # display_objects = display_objects_pos + display_objects_neg
            #
            # if len(display_objects_neg) == 0:
            #     return logits
            #
            # print('logs:', logs[display_objects])
            # print('labs:', labs[display_objects])
            #
            # f, axarr = plt.subplots(len(display_objects), 7, figsize=(50, 50))
            #
            # for y,o in enumerate(display_objects):
            #     for i, crop in enumerate(image_crops):
            #         crop = np.array(crop[o]).swapaxes(0, -1).swapaxes(0, 1)
            #         axarr[y, i].imshow(crop/255)
            #         axarr[y, i].set_title('obj_t'+str(i))
            #
            #     crop = np.array(image_candidates[o]).swapaxes(0, -1).swapaxes(0, 1)
            #     axarr[y, -1].imshow(crop/255)
            #     axarr[y, -1].set_title('cand')
            #
            # # plt.show(block=True)
            # plt.savefig("./classification_vis")
            # from IPython import embed
            # embed()

            return logits, final_features

        else:
            # tracking mode
            objects, candidate_crops, candidates_locations = sample['objects'], sample['candidates_crops'], sample["candidates_locations"]
            # objects = [{ 'id': <id>, 'crops': [<tensor at step 1>, ..., <tensor at step 6>] }]

            num_objects = len(objects)
            num_candidates = len(candidates_locations)

            objects_ids, objects = zip(*objects.items())
            objects_ids = np.array(objects_ids)

            sequence_len = len(objects[0]["crops"])

            candidate_crops_features = self.backbone(torch.cat(candidate_crops, dim=0).to(self.device))
            candidate_crops_features_tiled = candidate_crops_features.repeat([num_objects, 1])  # c1, c2, c3

            candidates_velocities = calculate_candidates_velocities(candidates_locations, objects, sample["candidates_boxes"], normalized=True)
            candidates_velocities_features = self.candiate_velocity_fc(candidates_velocities.to(self.device))

            # c1o1, c2o1, c3o1, c1o2, c2o2, c3o2
            candidates_features_tiled = torch.cat([candidate_crops_features_tiled, candidates_velocities_features], dim=1)

            # (seq, batch_size, feature)
            # pass all through CNN
            objects_crops = list(itertools.chain(*[obj["crops"] for obj in objects]))  # o1----,o2----,o3----

            objects_image_features = self.backbone(torch.cat(objects_crops, dim=0).to(self.device))
            objects_image_features = objects_image_features.view([num_objects, sequence_len, self.H]).permute(1, 0, 2)

            # for i in range(num_objects):
            #     means = torch.mean(torch.cat(objects_crops, dim=0).view(num_objects, sequence_len, -1), dim=-1)[i]
            #     means_ = torch.tensor([torch.mean(s) for s in objects[i]["crops"]])
            #     # if not torch.sum(torch.abs(means_ - means)) < 1e-4:
            #     #     from IPython import embed
            #     #     embed()

            objects_velocities = calculate_objects_velocities(objects, normalized=True)
            # print('obj_v:', torch.prod(objects_velocities, dim=-1))
            # print("obj_c:", torch.mean(torch.cat(objects_crops).view([num_objects, sequence_len, -1]), dim=-1))

            # print(torch.mean(objects_velocities, dim=-1))

            objects_features = torch.cat([objects_image_features, objects_velocities.to(self.device)], dim=2)

            objects_trajectory_features_sequence, _ = self.lstm(objects_features)

            if self.bidir_pool:
                objects_trajectory_features = torch.mean(objects_trajectory_features_sequence, dim=0)

            else:
                objects_trajectory_features = objects_trajectory_features_sequence[-1, :, :]  # last state

            assert objects_velocities.shape == (len(objects[0]['velocities']), num_objects, 2)

            candidates_ids_tiled = torch.tensor(sample["candidates_ids"]).repeat(
                [num_objects, 1]).reshape(num_objects*num_candidates, 1)

            objects_trajectory_features_tiled = objects_trajectory_features.repeat(
                [1, num_candidates]).view(num_objects * num_candidates, 2*self.H)

            # 1,1,2,2
            if num_candidates != 1: assert torch.var(torch.mean(objects_trajectory_features_tiled[:num_candidates], dim=-1)) <= 1e-5

            comparison_features = torch.cat((
                objects_trajectory_features_tiled,
                candidates_features_tiled
            ), dim=1)

            objects_ids = objects_ids.repeat([num_candidates]).reshape(num_objects*num_candidates, 1)

            # 1,1,2,2,3,3
            if num_candidates != 1: assert np.var(np.mean(objects_ids[:num_candidates], axis=-1)) <= 1e-5

            edges = np.concatenate((objects_ids, candidates_ids_tiled), axis=1)

            assert len(set([e.__repr__() for e in edges])) == edges.shape[0]

            final_features = self.fc_layer(comparison_features)

            if self.hidden:
                final_features = self.hidden_fc_layer(final_features)

            logits = self.last_fc(final_features).view(-1)

            # # VISUALIZE
            # print('num_obj:', num_objects)
            # print('num_cand:', num_candidates)
            #
            # def sigmoid(x, derivative=False):
            #     return x * (1 - x) if derivative else 1 / (1 + np.exp(-x))
            #
            # np.set_printoptions(suppress=True)
            # res = np.concatenate((edges, sigmoid(logits.cpu().detach().numpy().reshape(-1, 1))), axis=1)
            # print("edges, probs:", res)
            #
            # f, axarr = plt.subplots(1+num_objects, max(6, num_candidates), figsize=(15, 15))
            #
            # assert len(candidates_ids) == len(candidate_crops)
            # for i, (c_id, crop) in enumerate(zip(candidates_ids, candidate_crops)):
            #     crop = np.array(crop[0]).swapaxes(0, -1).swapaxes(0, 1)
            #     axarr[0, i].imshow(crop/255)
            #     axarr[0, i].set_title('c_'+str(int(c_id)))
            #
            # assert len(objects_ids) == len(objects)
            # for i, (o_id, obj) in enumerate(zip(objects_ids, objects)):
            #     crops = obj['crops']
            #     for j, crop in enumerate(crops):
            #         crop = np.array(crop[0]).swapaxes(0, -1).swapaxes(0, 1)
            #         axarr[i+1, j].imshow(crop/255)
            #         axarr[i+1, j].set_title('o_'+str(int(o_id)))
            #
            # # plt.show(block=True)
            # plt.savefig('tracking_test')
            # print("saved figure")
            # from IPython import embed
            # embed()

            return edges, logits, final_features


def calculate_candidates_velocities(candidates_locations, objects, boxes, normalized=False):
    num_objects = len(objects)
    num_candidates = len(candidates_locations)

    candidates_locations_tiled = torch.stack(candidates_locations).repeat([num_objects, 1])
    if num_objects != 1: assert torch.var(torch.mean(candidates_locations_tiled[::num_candidates], dim=-1)) <= 1e-5  # 1,2,3,1,2,3,1,2,3

    objects_last_locations = torch.stack([obj["locations"][-1] for obj in objects], dim=0)  # (obj, 2)
    objects_last_locations_tiled = objects_last_locations.repeat([1, num_candidates]).view(num_objects*num_candidates, 2)

    if num_candidates != 1: assert torch.var(torch.mean(objects_last_locations_tiled[:num_candidates], dim=-1)) <= 1e-5  # 1,1,1,2,2,2,3,3,3

    candidates_velocities = candidates_locations_tiled - objects_last_locations_tiled

    if normalized:
        assert torch.all(torch.eq(
            torch.stack([torch.tensor(dataset_utils.box_center(b)) for b in boxes]).float(),
            torch.stack(candidates_locations).float()
        ))

        candidates_frame_numbers = [b.frame_number for b in boxes]
        candidates_frame_numbers_tiled = torch.tensor(candidates_frame_numbers).view(-1, 1).repeat([num_objects, 1])
        if num_objects != 1: assert torch.var(torch.mean(candidates_frame_numbers_tiled[::num_candidates], dim=-1)) <= 1e-5  # 1,2,3,1,2,3,1,2,3

        objects_last_frame_numbers = torch.tensor([obj["frames_numbers"][-1] for obj in objects]).view(-1, 1)  # (obj, 1)
        objects_last_frame_numbers_tiled = objects_last_frame_numbers.repeat([1, num_candidates]).view(num_objects*num_candidates, 1)
        if num_candidates != 1: assert torch.var(torch.mean(objects_last_locations_tiled[:num_candidates], dim=-1)) <= 1e-5  # 1,1,1,2,2,2,3,3,3

        candidates_diffs = candidates_frame_numbers_tiled.float() - objects_last_frame_numbers_tiled.float()
        candidates_diffs = candidates_diffs.repeat([1, 2])
        assert candidates_diffs.shape == candidates_velocities.shape

        candidates_velocities /= candidates_diffs.float()

    return candidates_velocities


def calculate_objects_velocities(objects, normalized=False):
    # prvi je 0,0
    objects_velocities = torch.stack([torch.stack(list(obj["velocities"])) for obj in objects])
    objects_velocities = objects_velocities.permute(1, 0, 2)

    if normalized:
        # [n_fr, obj]
        num_steps = objects_velocities.shape[0]

        # mask first enterance of object (velocity would be location)
        num_objects = len(objects)
        objects_locations = torch.stack([torch.stack(list(obj["locations"])) for obj in objects]).permute(1, 0, 2)
        mask = (torch.sum(objects_locations[:-1, :, :], dim=-1) > 0) * (
                torch.sum(objects_locations[1:, :, :], dim=-1) > 0)

        # first step velocities are always zeros
        mask = torch.cat((torch.zeros(1, num_objects), mask.float()))
        objects_velocities = objects_velocities * mask[:, :, None].repeat(1, 1, 2)

        objects_frames_numbers = [torch.Tensor(obj["frames_numbers"]) for obj in objects]

        diffs = []
        for object_frames_numbers in objects_frames_numbers:
            diff = object_frames_numbers[1:]-object_frames_numbers[:-1]

            assert len(diff) <= num_steps
            assert len(diff) != num_steps  # uvijek paddamo jedan mozda je to problem

            diff = torch.cat([torch.ones(num_steps-len(diff)), diff]) # padd
            diffs.append(diff)

        diffs = torch.stack(diffs).permute(1, 0)
        assert diffs.shape == (num_steps, len(objects))

        # repeat for x and y axis
        diffs = diffs[:, :, None].repeat([1, 1, 2])

        assert objects_velocities.shape == diffs.shape
        objects_velocities = objects_velocities/diffs.float()


    return objects_velocities
