## Tracking the untrackable reimplementation and further experiments

The original paper. [Tracking the untrackable](https://arxiv.org/abs/1701.01909)

Comparison of top performing models with the private and public detectors. The trackers using the private detectors are expectedly better.

The reimplementation uses the yolo detector hence achieves improvement.

Other results are taken from the 2DMOT2015 [leaderboard](https://motchallenge.net/results/2D_MOT_2015/).


model       | MOTA | decetor
:-----------: | :----: | :----:
youtu\_face (top priv) | 56.8 | priv
this_project (reimp with yolo)   | 41.8 | priv
ReID\_CNN (top pub)  | 38.9 | pub
AMIR15  (original paper)     | 37.6 | pub

## Tracking demo:
Tracking demo.
Note that the video resolution is reduced only for the demo.

![Resized trakcing demo](demo_resized.gif)
