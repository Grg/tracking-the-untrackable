import torch
import copy
import random
import torch.nn as nn
import torch.optim.lr_scheduler as lr_scheduler
from TrackingTester import Tester
import time
import numpy as np
from IPython import embed
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from pathlib import Path

import os
import json
from datasets.classification.MOT2015 import get_loaders
from models.Appearance import AppearanceModel
from models.Target import TargetModel
from models.Motion import MotionModel
from models.Joint import JointModel
from models.Interaction import InteractionModel
from torchvision import transforms, utils, models

from termcolor import colored
rprint = lambda *args: print(colored("".join(map(str, args)), "red"))
bprint = lambda *args: print(colored("".join(map(str, args)), "blue"))
gprint = lambda *args: print(colored("".join(map(str, args)), "green"))


class Trainer:

    def __init__(
            self,
            model,
            # start_learning_rate = 0.002 / 2, # divide by 64/batch_size
            # start_learning_rate = 0.002,
            start_learning_rate=0.002,
            diminish_lr_every_epoch_number=10,
            diminish_lr_factor=0.1,
            diminish_lr_last_step=50,
            batch_size=32,
            num_epochs=100,
            model_checkpoint_path=None,
            resume_train=False,
            crops=False,
            velocities=True,
            occupancy_grids=True,
            blackouts=True,
            train_sequence_length=6,
            test_sequence_length=6,
            future_seq_len=6,
            shuffle=True,
            num_workers= 0,
            save_model=True,
            save_step=1,
            save_path="./experiments/experiment",
            device=None
    ):

        self.start_learning_rate = start_learning_rate
        self.diminsh_lr_every_epoch_number = diminish_lr_every_epoch_number
        self.diminish_lr_factor = diminish_lr_factor
        self.diminish_lr_last_step = diminish_lr_last_step
        self.batch_size = batch_size
        self.num_epochs = num_epochs
        self.model_checkpoint_path = model_checkpoint_path
        self.crops = crops
        self.velocities = velocities
        self.occupancy_grids = occupancy_grids
        self.blackouts = blackouts
        self.train_sequence_length = train_sequence_length
        self.test_sequence_length = test_sequence_length
        self.future_seq_len = future_seq_len
        self.shuffle = shuffle
        self.num_workers = num_workers
        self.save_model = save_model
        self.save_step = save_step
        self.save_path = save_path
        self.resume_train = resume_train
        self.model = model

        # Device configuration
        self.device = device if device is not None else torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        print("Trainer device:", self.device)

        self.train_loader, _ = get_loaders(
            self.batch_size,
            shuffle=shuffle,
            num_workers=num_workers,
            crops=crops,
            velocities=velocities,
            future_seq_len=future_seq_len,
            occupancy_grids=occupancy_grids,
            train_sequence_length=self.train_sequence_length,
            test_sequence_length=self.test_sequence_length,
            train=True,
            test=False
        )

        self.model.to(self.device).train()

        # Loss and optimizer
        self.criterion = nn.BCEWithLogitsLoss(size_average=True).to(self.device)
        self.params = list(self.model.parameters())
        self.optimizer = torch.optim.Adam(self.params, lr=start_learning_rate)

        # load model
        if model_checkpoint_path:
            self.start_epoch = self.model.load_state(
                model_checkpoint_path,
                resume_train=resume_train, optimizer=self.optimizer if resume_train else None)

        else:
            self.start_epoch = 1

        self.scheduler = lr_scheduler.StepLR(
            self.optimizer,
            step_size=diminish_lr_every_epoch_number,
            gamma=diminish_lr_factor,
        )

        self.diminish_lr_last_step = diminish_lr_last_step

        self.save_model = save_model
        self.save_step = save_step
        self.save_path = save_path

        if self.save_model:
            dirname = os.path.dirname(os.path.abspath(self.save_path))
            if not os.path.exists(dirname):
                os.makedirs(dirname)

        self.model.to(self.device)

    @staticmethod
    def create_masks(sequence_length, elements_in_batch):
        blackout_nums = np.random.randint(0, sequence_length, elements_in_batch)
        step_masks = []
        for i in range(sequence_length):
            step_masks.append(
                np.where(i < blackout_nums, 0, 1).astype(np.float32)
            )

        assert np.sum(1-np.hstack(step_masks)) == np.sum(blackout_nums)

        return step_masks, blackout_nums

    def add_blackouts_to_sample(self, sample):
        elements_in_batch = len(sample['label'])

        step_masks, blackout_nums = Trainer.create_masks(sequence_length=self.train_sequence_length, elements_in_batch=elements_in_batch)

        def mask(sequence, step_masks, check=False):
            step_masks = copy.deepcopy(step_masks)
            masked_sequence = []

            if len(step_masks) < len(sequence):
                # locations have one additional step
                diff = len(sequence) - len(step_masks)
                assert diff == 1
                step_masks = [np.zeros_like(step_masks[0])] + step_masks

            assert len(step_masks)==len(sequence)
            for mask, s in zip(step_masks, sequence):
                for _ in range(len(s.shape)-1):
                    mask = np.expand_dims(mask, -1)

                masked_sequence.append(mask * s)

            if check:
                for e in range(elements_in_batch):
                    zeros = sum([np.sum(np.array(mc[e])) == 0 for mc in masked_sequence])
                    assert zeros == blackout_nums[e]

            return masked_sequence

        if self.crops:
            masked_crops = mask(sample["image_crops"], step_masks, check=True)
            sample["image_crops"] = masked_crops

        if self.velocities:
            # if there is no obj at first step then 2 vels are missing
            masked_velocities = mask(
                sample["sequence_velocities"],
                [np.zeros_like(step_masks[0])] + step_masks[:-1]
            )
            sample["sequence_velocities"] = masked_velocities

            masked_locations = mask(sample["sequence_locations"], step_masks)
            sample["sequence_locations"] = masked_locations

        if self.occupancy_grids:
            masked_crops = mask(sample["sequence_occupancy_grids"], step_masks, check=True)
            sample["sequence_occupancy_grids"] = masked_crops

        return sample

    def train(self, display_steps=250, test_every_epoch=1):
        print("Training")
        train_statistics = {}
        test_statistics = {}

        for epoch in range(self.start_epoch, 1+self.num_epochs):
            if epoch <= self.diminish_lr_last_step:
                self.scheduler.step()

            print("\nEpoch {} lr {} ".format(epoch, self.scheduler.get_lr()))

            total_loss, total_acc = 0, 0
            total_step = len(self.train_loader)

            start_epoch = time.time()
            for iter, sample in enumerate(self.train_loader):

                self.model.train()  # train mode

                if self.blackouts:
                    sample = self.add_blackouts_to_sample(sample)

                logits, final_features = self.model(sample)

                if type(logits) == tuple:
                    logits, *aux_logits = logits

                predicted_classes = (logits > 0.5).float()

                labels = sample['label'].to(self.device)

                acc = ((predicted_classes == labels).sum().float()/labels.shape[0]).cpu().data.numpy()
                total_acc += acc

                # Forward
                loss = self.criterion(logits, labels)

                if type(logits) == tuple:
                    for aux_ls in aux_logits:
                        aux_loss = self.criterion(aux_ls, labels)
                        loss += aux_loss

                # Backward and optimize
                self.optimizer.zero_grad()
                loss.backward()
                torch.nn.utils.clip_grad_norm_(self.model.parameters(), 1.0)
                self.optimizer.step()

                total_loss += loss.cpu().data.numpy()

                if (iter + 1) % display_steps == 0:
                    with torch.no_grad():
                        num_of_ones = (torch.sum(predicted_classes)/predicted_classes.shape[0]).cpu().data.numpy()

                        lab_num_of_ones = (torch.sum(labels)/predicted_classes.shape[0]).cpu().data.numpy()
                        print('Train Epoch [{}/{}], Step [{}/{}], Learning rate: {:.9f}, Loss: {:.4f}, Accuracy: {:.4f} pred_ones_perc: {} lab_ones_perc: {}'
                              .format(
                            epoch,
                            self.num_epochs,
                            iter + 1,
                            total_step,
                            self.scheduler.get_lr()[0],
                            total_loss/(iter+1),
                            total_acc/(iter+1),
                            num_of_ones,
                            lab_num_of_ones)
                        )

            epoch_time = time.time() - start_epoch
            epoch_loss = total_loss / total_step
            epoch_acc = total_acc / total_step

            rprint('Train set: Loss: {:.4f}, Accuracy: {:.4f}, time: {:.4f}'.format(epoch_loss, epoch_acc, epoch_time))

            if self.save_model and epoch % self.save_step == 0:
                print("Saving state for epoch:", epoch)
                self.model.save_state(
                    "{}_{}.tar".format(self.save_path, epoch),
                    epoch=epoch,
                    optimizer=self.optimizer
                )

            # dump train stats
            if self.save_model:
                train_statistics[epoch] = (epoch_loss, epoch_acc)
                self.dump(train_statistics, "train_stats.json")

            if epoch % test_every_epoch == 0:
                print("Test epoch {}".format(epoch))
                l, a = self.evaluate_model(epoch=epoch)

                # dump test stats
                if self.save_model:
                    test_statistics[epoch] = (l, a)
                    self.dump(test_statistics, "test_stats.json")

                    log_dict = {
                        "start_learning_rate": self.start_learning_rate,
                        "diminish_lr_every_epoch": self.diminsh_lr_every_epoch_number,
                        "diminish_lr_last_step": self.diminish_lr_last_step,
                        "batch_size": self.batch_size,
                        "num_epochs": self.num_epochs,
                        "model_checkpoint_path": self.model_checkpoint_path,
                        "resume_train": self.resume_train,
                        "crops": self.crops,
                        "velocities": self.velocities,
                        "occupancy_grids": self.occupancy_grids,
                        "blackouts": self.blackouts,
                        "train_sequence_length": self.train_sequence_length,
                        "test_sequence_length": self.test_sequence_length,
                        "shuffle": self.shuffle,
                        "num_workers": self.num_workers,
                        "save_model": self.save_model,
                        "save_step": self.save_step,
                        "save_path": self.save_path,
                    }
                    self.dump(log_dict, "log.json")

    def dump(self, statistics, filename):
        json_path = os.path.join(os.path.dirname(self.save_path), filename)
        with open(json_path, 'w') as test_json:
            json.dump(statistics, test_json)

    def evaluate_model(self, display_steps=10000, epoch=None):
        self.model.eval()

        trainer = Tester(
            self.model,
            crops=True,
            velocities=True,
            occupancy_grids=False,
            boxes=True,
            sequence_len=self.test_sequence_length,
            device=self.device,
            dump_videos=False,
            vis_save_dir=Path(self.save_path).parent / "vis" / str(epoch),
        )
        mota = trainer.test(disable_a3=True)
        return 0.5, mota

        # with torch.no_grad():
        #     test_total_step = len(self.test_loader)
        #     num_of_ones = 0
        #     total_examples = 0
        #     test_loss = 0
        #     total_acc = 0
        #     epoch_time_start = time.time()
        #
        #     for test_iter, test_sample in enumerate(self.test_loader):
        #         test_logits, test_final_features = self.model(test_sample)
        #         test_predicted_classes = (test_logits > 0.5).float()
        #
        #         test_labels = test_sample['label'].to(self.device)
        #
        #         acc = ((test_predicted_classes == test_labels).sum().float()/test_labels.shape[0]).cpu().data.numpy()
        #         total_acc += acc
        #
        #         current_loss = self.criterion(test_logits, test_labels).cpu().data.numpy()
        #         test_loss += current_loss
        #
        #         num_of_ones += torch.sum(test_logits > 0.5).cpu().data.numpy()
        #         current_total_examples = test_labels.cpu().data.numpy().shape[0]
        #         total_examples += current_total_examples
        #
        #         if (test_iter + 1) % display_steps == 0:
        #             print('Testing step {}/{} loss: {} acc: {}.'.format(
        #                 test_iter+1, test_total_step, test_loss/(test_iter+1), total_acc/(test_iter+1)))
        #             print("current acc:", acc)
        #
        #     if self.blackouts:
        #         for test_iter, test_sample in enumerate(self.test_loader):
        #             if self.velocities:
        #                 test_sample['sequence_velocities'] = [v*0 if i < self.test_sequence_length else v for i, v in enumerate(test_sample['sequence_velocities'])]
        #                 test_sample['sequence_locations'] = [v*0 if i < self.test_sequence_length-1 else v for i, v in enumerate(test_sample['sequence_locations'])]
        #
        #             if self.crops:
        #                 test_sample['image_crops'] = [c*0 if i < self.test_sequence_length-1 else c for i, c in enumerate(test_sample['image_crops'])]
        #
        #             test_logits, test_final_features = self.model(test_sample)
        #             test_predicted_classes = (test_logits > 0.5).float()
        #
        #             test_labels = test_sample['label'].to(self.device)
        #
        #             acc = ((test_predicted_classes == test_labels).sum().float()/test_labels.shape[0]).cpu().data.numpy()
        #             total_acc += acc
        #
        #             current_loss = self.criterion(test_logits, test_labels).cpu().data.numpy()
        #             test_loss += current_loss
        #
        #             num_of_ones += torch.sum(test_logits > 0.5).cpu().data.numpy()
        #             current_total_examples = test_labels.cpu().data.numpy().shape[0]
        #             total_examples += current_total_examples
        #
        #             if (test_iter + 1) % display_steps == 0:
        #                 print('Testing step {}/{} loss: {} acc: {}.'.format(
        #                     test_iter+1, test_total_step, test_loss/(test_iter+1), total_acc/(len(self.test_loader)+test_iter+1)))
        #
        #         # half
        #         for test_iter, test_sample in enumerate(self.test_loader):
        #             if self.velocities:
        #                 test_sample['sequence_velocities'] = [v*0 if i < self.test_sequence_length//2 else v for i, v in enumerate(test_sample['sequence_velocities'])]
        #                 test_sample['sequence_locations'] = [v*0 if i < (self.test_sequence_length//2)-1 else v for i, v in enumerate(test_sample['sequence_locations'])]
        #
        #             if self.crops:
        #                 test_sample['image_crops'] = [c*0 if i < self.test_sequence_length-1 else c for i, c in enumerate(test_sample['image_crops'])]
        #
        #             test_logits, test_final_features = self.model(test_sample)
        #             test_predicted_classes = (test_logits > 0.5).float()
        #
        #             test_labels = test_sample['label'].to(self.device)
        #
        #             acc = ((test_predicted_classes == test_labels).sum().float()/test_labels.shape[0]).cpu().data.numpy()
        #             total_acc += acc
        #
        #             current_loss = self.criterion(test_logits, test_labels).cpu().data.numpy()
        #             test_loss += current_loss
        #
        #             num_of_ones += torch.sum(test_logits > 0.5).cpu().data.numpy()
        #             current_total_examples = test_labels.cpu().data.numpy().shape[0]
        #             total_examples += current_total_examples
        #
        #             if (test_iter + 1) % display_steps == 0:
        #                 print('Testing step {}/{} loss: {} acc: {}.'.format(
        #                     test_iter+1, test_total_step, test_loss/(test_iter+1), total_acc/(2*len(self.test_loader)+test_iter+1)))
        #
        #     epoch_time = time.time() - epoch_time_start
        #
        #     total = 3*test_total_step if self.blackouts else test_total_step
        #
        #     epoch_loss = test_loss/total
        #     epoch_acc = total_acc/total
        #
        #     gprint('Test set: Loss: {:.4f}, Accuracy: {:.4f}, time: {:.4f}, Pred_ones_perc: {}'.format(
        #         epoch_loss,
        #         epoch_acc,
        #         epoch_time,
        #         num_of_ones/total_examples)
        #     )
        #
        #     return epoch_loss, mota


def experiment(
        name="experiment",
        k=100, H=128,
        siamese=False,
        blackouts=True,
        hidden=False,
        train_sequence_length=6,
        test_sequence_length=6,
        backbone=None, use_bn=False, use_dropout=False,
        start_learning_rate=0.002, diminish_lr_every_epoch_number=5, diminish_lr_last_step=10,
        num_epochs=20, train_backbone=True, batch_size=64, device='cuda'
):

    print()
    gprint("experiment:", name)
    model = AppearanceModel(
        k=k, H=H,
        siamese=siamese,
        hidden=hidden,
        train_backbone=train_backbone,
        use_bn=use_bn,
        use_dropout=use_dropout,
        device=device,
        backbone=backbone
    )

    trainer = Trainer(
        model,
        crops=True,
        velocities=False,
        occupancy_grids=False,

        blackouts=blackouts,

        save_model=True,
        start_learning_rate=start_learning_rate,
        diminish_lr_every_epoch_number=diminish_lr_every_epoch_number,
        diminish_lr_last_step=diminish_lr_last_step,

        num_epochs=num_epochs,
        save_step=1,
        shuffle=True,

        save_path="./experiments/{}/model".format(name),
        num_workers=20,
        batch_size=batch_size,
        device=device,
        train_sequence_length=train_sequence_length,
        test_sequence_length=test_sequence_length
    )
    trainer.train()


if __name__ == "__main__":
    import sys
    m = sys.argv[1]
    assert m in ["m", "i", "a", "t", "j"]

    if m == "t":
        device = "cuda"

        appearance_model = AppearanceModel(
            k=500, H=500,
            siamese=True, device=device, hidden=True,
            use_bn=True, train_backbone=True,
            backbone=models.resnet18(pretrained=False)
        )
        motion_model = MotionModel(k=100, H=128, hidden=True, normalized_velocities=True, device=device)

        gprint("Target model")
        model = TargetModel(
            k=100, H=128, device=device,
            hidden=True,
            mode=["A", "M"],
            appearance_model=appearance_model,
            motion_model=motion_model,
            interaction_model=None,
            aux_heads=True,
        )
        trainer = Trainer(
            model,
            shuffle=True,
            crops=True,
            velocities=True,
            occupancy_grids=False,
            start_learning_rate=0.001,
            diminish_lr_every_epoch_number=5,
            diminish_lr_last_step=10,
            num_epochs=20,
            future_seq_len=10,
            blackouts=True,
            train_sequence_length=6+5,
            test_sequence_length=6+5,
            batch_size=32,
            save_model=True,
            model_checkpoint_path=[
                # "./experiments/res18_yolo_det_1516_hidden_v2/model_6.tar",
                # "./experiments/MotionModel_black_1516/model_7.tar",

                # "./experiments/res18_noisy_det_15_hidden_500_500_bn/model_4.tar",
                # "./experiments/res18_both_1516_hidden_500_500_bn/model_5.tar",
                # "./experiments/MotionModel_both_1516_normalized/model_4.tar"
                
                # "./experiments/res18_yolo_1516_hidden_500_500_pairs_only_cuhk_v2/model_1.tar",
                # "./experiments/res18_yolo_1516_hidden_500_500_pairs_only_cuhk_v2_extended/model_4.tar",
                "./experiments/AppearanceModel_res18_yolo_500_500_pairs_only_cuhk_pairs_cuhk_extended/model_3.tar",
                "./experiments/MotionModel_extended/model_10.tar"

            ],
            device=device,
            # save_path="./experiments/TargetModelAM_1516_yolo_bl_fix/model",  # appearance cropovi su -1
            save_path="./experiments/TargetModelAM_pretrained_extended_aux_heads/model",
            save_step=1,
            num_workers=10,
        )
        trainer.train()

    elif m == "a":
        gprint("Appearance")
        experiment(
            name="AppearanceModel_res18_yolo_500_500_pairs_only_cuhk_pairs_cuhk_extended",
            backbone=models.resnet18(pretrained=True),
            k=500,
            H=500,
            start_learning_rate=0.001,
            diminish_lr_every_epoch_number=3,
            diminish_lr_last_step=10,
            num_epochs=20,
            blackouts=False,
            hidden=True,
            use_bn=True,
            use_dropout=False,
            siamese=True,
            train_sequence_length=2,
            test_sequence_length=6,
            train_backbone=True,
            batch_size=64,
            device='cuda'
        )

    elif m == "m":
        gprint("Motion")
        device = "cpu"
        trainer = Trainer(
            MotionModel(k=100, H=128, hidden=True, normalized_velocities=True, device=device),
            batch_size=64,
            start_learning_rate=0.002,
            diminish_lr_every_epoch_number=5,
            diminish_lr_last_step=10,
            num_epochs=20,
            save_step=1,
            shuffle=True,
            crops=True,
            velocities=True,
            occupancy_grids=False,
            blackouts=True,
            save_model=True,
            # save_path="./experiments/MotionModel_no_occ_new/model",
            # save_path="./experiments/MotionModel_black_1516_avg/model",
            # save_path="./experiments/MotionModel_both_1516_normalized/model",
            # save_path="./experiments/MotionModel_1516_pairs_only/model",
            save_path="./experiments/MotionModel_extended/model",
            # save_path="./experiments/NormalizedMotionModel_black_15_noisy_det/model",
            num_workers=10,
            future_seq_len=10,
            device=device
        )
        trainer.train()

    elif m == "i":
        gprint("InteractionModel")
        trainer = Trainer(
            InteractionModel(k=100, H=128, device='cuda'),
            batch_size=64,
            start_learning_rate=0.002,
            diminish_lr_every_epoch_number=20,
            diminish_lr_last_step=10,
            num_epochs=20,
            save_step=1,
            shuffle=True,
            crops=False,
            velocities=False,
            occupancy_grids=True,
            blackouts=True,
            save_model=True,
            save_path="./experiments/InteractionModel_black/model",
            num_workers=10,
            device='cuda'
        )
        trainer.train()

    elif m == "j":
        device = "cpu"
        gprint("JointModel")
        trainer = Trainer(
            JointModel(
                k=500, H=500,
                device=device,
                hidden=True,
                backbone=models.resnet18(pretrained=True),
                normalized_velocities=True,
                use_bn=True,
                num_layers=1,
                bidir_pool=True,
                add_heads=True,
                attention=True,
            ),
            batch_size=3,
            start_learning_rate=0.001,
            diminish_lr_every_epoch_number=3,
            diminish_lr_last_step=10,
            num_epochs=20,
            save_step=1,
            shuffle=True,
            crops=True,
            velocities=True,
            occupancy_grids=False,
            blackouts=True,
            save_model=True,
            train_sequence_length=6,
            test_sequence_length=6,
            # save_path="./experiments/JointModel_1516_aux_different_heads_extended_v2/model",
            # save_path="./experiments/JointModel_bidir_aux_heads/model",
            save_path="./experiments/JointAttentionTest/model",
            num_workers=10,
            device=device
        )
        trainer.train()

    else:
        assert False
