import warnings
from collections import namedtuple

from torch.utils.data import DataLoader
from torchvision import transforms

from datasets.tracking.MOT2015 import MOT2015 as TrackingDatset
from datasets.tracking.MOT2015 import ToTensor as eval_ToTensor

warnings.filterwarnings("ignore")

Box = namedtuple('Box', ['video_name', 'frame_number', 'obj_id', 'bb_left', 'bb_top', 'bb_width', 'bb_height', 'conf'])


# def create_sequence_gt_boxes_dict(gt_boxes):
#     # todo: change this to defaultdict(dict)
#     gt_boxes_dict = defaultdict(lambda: defaultdict(list))
#
#     for gt_box in gt_boxes:
#         assert type(gt_box) == Box
#         gt_boxes_dict[gt_box.frame_number][gt_box.obj_id]=gt_box
#
#     return gt_boxes_dict
#
#
# def parse_gt_boxes(gt_boxes, video_name):
#     boxes = list()
#     for box in gt_boxes:
#         left = box[2]
#         top = box[3]
#
#         if left < 0:
#             box[2] = 0
#             box[4] = box[4] - abs(left)  # reduce with for the amount outside of border
#
#         if top < 0:
#             box[3] = 0
#             box[5] = box[5] - abs(top)  # reduce height for the amount outside of border
#
#         box = Box(video_name, *box)
#
#         boxes.append(box)
#
#     return boxes

def get_loader(
        batch_size,
        shuffle=False,
        crops=True,
        velocities=True,
        boxes=False,
        occupancy_grids=True,
        num_workers=0,
        classification=True,
        train=True,
        test=True,
):
    loaders = []
    if test:
        composed = transforms.Compose([eval_ToTensor(crops=crops, velocities=velocities, occupancy_grids=occupancy_grids)])
        test_dataset = TrackingDatset(composed, crops=crops, velocities=velocities, occupancy_grids=occupancy_grids, boxes=boxes)
        loaders.append(DataLoader(test_dataset, batch_size=batch_size, shuffle=False, num_workers=1))

    return loaders


# if __name__ == "__main__":
#
#     composed = transforms.Compose([ToTensor()])
#     mot_dataset = MOT2015Dataset(composed)
#
#
#     dataloader = DataLoader(mot_dataset, batch_size=2,
#                             shuffle=False, num_workers=1)
#
#     for i_batch, sample in enumerate(dataloader):
#         images = sample['image_crops']
#         continue
#
#         for step, image in enumerate(images):
#             plt.imshow(image[0])
#             plt.show(block=True)
#
#         plt.imshow(sample['candidate_crop'][0])
#         plt.show(block=True)
#
#         continue
#         im, detections = sample['image'][0], sample['detections'][0]
#
#
#         # Create figure and axes
#         fig, ax = plt.subplots(1)
#
#         # Display the image
#         ax.imshow(im)
#
#         # Create a Rectangle patch
#         for detection in detections:
#             top_left = detection[1:3]
#             w, h  = detection[3], detection[4]
#             bot_left = np.stack((top_left[0], top_left[1]-h))
#             rect = patches.Rectangle(top_left, w, h, linewidth=1, edgecolor='r', facecolor='none')
#
#             # Add the patch to the Axes
#             ax.add_patch(rect)
#
#         plt.show(block=True)

